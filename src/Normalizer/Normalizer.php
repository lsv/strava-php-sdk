<?php

namespace Lsv\Strava\Normalizer;

use Symfony\Component\OptionsResolver\Options;

class Normalizer
{
    public static function dateTime(): \Closure
    {
        return function (Options $options, \DateTime $value) {
            return $value->format('c');
        };
    }

    public static function dateTimeToTimestamp(): \Closure
    {
        return function (Options $options, \DateTimeInterface $value) {
            return $value->format('U');
        };
    }

    public static function arrayToCommaList(): \Closure
    {
        return function (Options $options, array $values) {
            return implode(',', $values);
        };
    }

    public static function boolToInteger(): \Closure
    {
        return function (Options $options, bool $value) {
            return $value ? 1 : 0;
        };
    }

    public static function classToId(): \Closure
    {
        return function (Options $options, $value) {
            if (is_object($value) && property_exists($value, 'id')) {
                return $value->id;
            }

            return $value;
        };
    }
}
