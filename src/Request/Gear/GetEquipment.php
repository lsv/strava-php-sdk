<?php

namespace Lsv\Strava\Request\Gear;

use Lsv\Strava\Model\DetailedGear;
use Lsv\Strava\Request\Traits\GearUrlTrait;

/**
 * Returns an equipment using its identifier.
 */
class GetEquipment extends AbstractGearRequest
{
    use GearUrlTrait;

    /**
     * Execute the method.
     */
    public function execute(): DetailedGear
    {
        return $this->single(DetailedGear::class);
    }

    protected function url(array $urlOptions): string
    {
        return sprintf('gear/%s', $urlOptions['id']);
    }
}
