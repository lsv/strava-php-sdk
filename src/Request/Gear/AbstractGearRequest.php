<?php

namespace Lsv\Strava\Request\Gear;

use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedGear;
use Lsv\Strava\Model\SummaryGear;
use Lsv\Strava\Request\AbstractRequest;

abstract class AbstractGearRequest extends AbstractRequest
{
    /**
     * @param string|SummaryGear|DetailedGear $gear Gear ID
     */
    public function __construct(Client $client, $gear)
    {
        parent::__construct($client);
        $this->setByGear($gear);
    }

    abstract protected function setByGear($gear);
}
