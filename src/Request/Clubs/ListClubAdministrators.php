<?php

namespace Lsv\Strava\Request\Clubs;

use Lsv\Strava\Model\SummaryAthlete;
use Lsv\Strava\Request\Traits\ClubUrlTrait;
use Lsv\Strava\Request\Traits\PaginationTrait;

/**
 * Returns a list of the administrators of a given club.
 */
class ListClubAdministrators extends AbstractClubRequest
{
    use ClubUrlTrait;
    use PaginationTrait;

    /**
     * @return SummaryAthlete[]
     */
    public function execute(): array
    {
        return $this->multiple(SummaryAthlete::class);
    }

    protected function url(array $urlOptions): string
    {
        return sprintf('clubs/%s/admins', $urlOptions['id']);
    }
}
