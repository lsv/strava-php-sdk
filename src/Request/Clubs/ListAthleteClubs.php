<?php

namespace Lsv\Strava\Request\Clubs;

use Lsv\Strava\Model\SummaryClub;
use Lsv\Strava\Request\AbstractRequest;
use Lsv\Strava\Request\Traits\PaginationTrait;

/**
 * Returns a list of the clubs whose membership includes the authenticated athlete.
 */
class ListAthleteClubs extends AbstractRequest
{
    use PaginationTrait;

    /**
     * @return SummaryClub[]
     */
    public function execute(): array
    {
        return $this->multiple(SummaryClub::class);
    }

    protected function url(array $urlOptions): string
    {
        return 'athlete/clubs';
    }
}
