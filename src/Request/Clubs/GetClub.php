<?php

namespace Lsv\Strava\Request\Clubs;

use Lsv\Strava\Model\DetailedClub;
use Lsv\Strava\Request\Traits\ClubUrlTrait;

/**
 * Returns a given club using its identifier.
 */
class GetClub extends AbstractClubRequest
{
    use ClubUrlTrait;

    public function execute(): DetailedClub
    {
        return $this->single(DetailedClub::class);
    }

    protected function url(array $urlOptions): string
    {
        return sprintf('clubs/%s', $urlOptions['id']);
    }
}
