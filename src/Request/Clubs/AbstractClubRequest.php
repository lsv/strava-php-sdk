<?php

namespace Lsv\Strava\Request\Clubs;

use Lsv\Strava\Client;
use Lsv\Strava\Request\AbstractRequest;

abstract class AbstractClubRequest extends AbstractRequest
{
    public function __construct(Client $client, $club)
    {
        parent::__construct($client);
        $this->setByClub($club);
    }

    abstract protected function setByClub($club);
}
