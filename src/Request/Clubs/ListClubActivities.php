<?php

namespace Lsv\Strava\Request\Clubs;

use Lsv\Strava\Model\SummaryActivity;
use Lsv\Strava\Request\Traits\ClubUrlTrait;
use Lsv\Strava\Request\Traits\PaginationTrait;

/**
 * Retrieve recent activities from members of a specific club.
 *
 * The authenticated athlete must belong to the requested club in order to hit this endpoint.
 *
 * Athlete profile visibility is respected for all activities.
 */
class ListClubActivities extends AbstractClubRequest
{
    use ClubUrlTrait;
    use PaginationTrait;

    /**
     * @return SummaryActivity[]
     */
    public function execute(): array
    {
        return $this->multiple(SummaryActivity::class);
    }

    protected function url(array $urlOptions): string
    {
        return sprintf('clubs/%s/activities', $urlOptions['id']);
    }
}
