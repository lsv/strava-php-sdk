<?php

namespace Lsv\Strava\Request\Athletes;

use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedAthlete;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Update the currently authenticated athlete.
 *
 * Requires profile:write scope.
 */
class UpdateAthlete extends AbstractAthleteRequest
{
    /**
     * @param float $weight The weight in kilogram
     */
    public function __construct(Client $client, float $weight)
    {
        parent::__construct($client);
        $this->setWeight($weight);
    }

    /**
     * Set weight in kilogram.
     *
     * @return UpdateAthlete
     */
    public function setWeight(float $weight): self
    {
        $this->queryOptions['weight'] = $weight;

        return $this;
    }

    public function execute(): DetailedAthlete
    {
        return $this->single(DetailedAthlete::class);
    }

    protected function validateQueryOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired(['weight']);
        $resolver->setAllowedTypes('weight', 'float');
    }

    protected function url(array $urlOptions): string
    {
        return 'athlete';
    }

    protected function method(): string
    {
        return 'PUT';
    }
}
