<?php

namespace Lsv\Strava\Request\Athletes;

use Lsv\Strava\Client;
use Lsv\Strava\Model\ActivityStats;
use Lsv\Strava\Model\DetailedAthlete;
use Lsv\Strava\Model\MetaAthlete;
use Lsv\Strava\Model\SummaryAthlete;
use Lsv\Strava\Request\Traits\AthleteUrlTrait;
use Lsv\Strava\Request\Traits\PaginationTrait;

/**
 * Returns the activity stats of an athlete.
 */
class GetAthletesStats extends AbstractAthleteRequest
{
    use AthleteUrlTrait;
    use PaginationTrait;

    /**
     * @param string|MetaAthlete|SummaryAthlete|DetailedAthlete $athlete set the ID from an athlete
     */
    public function __construct(Client $client, $athlete)
    {
        parent::__construct($client);
        $this->setByAthlete($athlete);
    }

    public function execute(): ActivityStats
    {
        return $this->single(ActivityStats::class);
    }

    protected function url(array $urlOptions): string
    {
        return sprintf('athletes/%s/stats', $urlOptions['id']);
    }
}
