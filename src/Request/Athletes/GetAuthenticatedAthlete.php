<?php

namespace Lsv\Strava\Request\Athletes;

use Lsv\Strava\Model\DetailedAthlete;

/**
 * Returns the currently authenticated athlete.
 *
 * Tokens with profile:read_all scope will receive a detailed athlete representation;
 * all others will receive a summary representation.
 */
class GetAuthenticatedAthlete extends AbstractAthleteRequest
{
    public function execute(): DetailedAthlete
    {
        return $this->single(DetailedAthlete::class);
    }

    protected function url(array $urlOptions): string
    {
        return 'athlete';
    }
}
