<?php

namespace Lsv\Strava\Request\Athletes;

use Lsv\Strava\Model\SummaryActivity;
use Lsv\Strava\Normalizer\Normalizer;
use Lsv\Strava\Request\Traits\PaginationTrait;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Returns the activities of an athlete for a specific identifier.
 *
 * Requires activity:read.
 * Only Me activities will be filtered out unless requested by a token with activity:read_all.
 */
class GetActivities extends AbstractAthleteRequest
{
    use PaginationTrait {
        validateQueryOptions as paginationResolver;
    }

    /**
     * A timestamp to use for filtering activities that have taken place before a certain time.
     *
     * @return GetActivities
     */
    public function setBefore(\DateTimeInterface $beforeTimstamp): self
    {
        $this->queryOptions['before'] = $beforeTimstamp;

        return $this;
    }

    /**
     * A timestamp to use for filtering activities that have taken place after a certain time.
     *
     * @return GetActivities
     */
    public function setAfter(\DateTimeInterface $afterTimestamp): self
    {
        $this->queryOptions['after'] = $afterTimestamp;

        return $this;
    }

    /**
     * @return SummaryActivity[]
     */
    public function execute(): array
    {
        return $this->multiple(SummaryActivity::class);
    }

    protected function validateQueryOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefined(['before', 'after']);
        $resolver->setAllowedTypes('before', [\DateTimeInterface::class, 'string']);
        $resolver->setAllowedTypes('after', [\DateTimeInterface::class, 'string']);

        $resolver->setNormalizer('before', Normalizer::dateTimeToTimestamp());
        $resolver->setNormalizer('after', Normalizer::dateTimeToTimestamp());

        $this->paginationResolver($resolver);
    }

    protected function url(array $urlOptions): string
    {
        return sprintf('athletes/activities');
    }
}
