<?php

namespace Lsv\Strava\Request\Athletes;

use Lsv\Strava\Model\Zones;

/**
 * Returns the the authenticated athlete's heart rate and power zones.
 *
 * Requires profile:read_all.
 */
class GetZones extends AbstractAthleteRequest
{
    /**
     * @return Zones[]
     */
    public function execute(): array
    {
        return $this->multiple(Zones::class);
    }

    protected function url(array $urlOptions): string
    {
        return 'athlete/zones';
    }
}
