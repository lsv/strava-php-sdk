<?php

namespace Lsv\Strava\Request\Uploads;

use GuzzleHttp\Psr7\MultipartStream;
use Lsv\Strava\Client;
use Lsv\Strava\Model\Upload;
use Lsv\Strava\Request\AbstractRequest;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Uploads a new data file to create an activity from.
 *
 * Requires activity:write scope.
 */
class UploadActivity extends AbstractRequest
{
    public const ALLOWED_FILE_EXTENSIONS = ['fit', 'fit.gz', 'tcx', 'tcx.gz', 'gpx', 'gpx.gz'];

    public function __construct(Client $client, File $file)
    {
        parent::__construct($client);
        $this->setFile($file);
    }

    public function setFile(File $file)
    {
        $this->bodyOptions['file'] = $file;
        $this->bodyOptions['data_type'] = $file->getExtension();

        if (!isset($this->bodyOptions['name'])) {
            $this->setName($file->getFilename());
        }

        return $this;
    }

    public function setName(string $name): self
    {
        /** @var File $file */
        $file = $this->bodyOptions['file'];
        $extension = $file->getExtension();
        if (substr($name, strlen($extension)) !== $extension) {
            $name .= '.'.$extension;
        }

        $this->bodyOptions['name'] = $name;

        return $this;
    }

    public function setDescription(string $description): self
    {
        $this->bodyOptions['description'] = $description;

        return $this;
    }

    public function setTrainer(string $trainer): self
    {
        $this->bodyOptions['trainer'] = $trainer;

        return $this;
    }

    public function setCommute(string $commute): self
    {
        $this->bodyOptions['commute'] = $commute;

        return $this;
    }

    public function setExternalId(string $externalId): self
    {
        $this->bodyOptions['external_id'] = $externalId;

        return $this;
    }

    protected function validateFormOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired(['file', 'name', 'data_type']);
        $resolver->setDefined(['description', 'trainer', 'commute', 'external_id']);
        $resolver->setAllowedTypes('file', File::class);
        $resolver->setAllowedTypes('name', 'string');
        $resolver->setAllowedTypes('data_type', 'string');
        $resolver->setAllowedValues('data_type', self::ALLOWED_FILE_EXTENSIONS);
        $resolver->setAllowedTypes('description', 'string');
        $resolver->setAllowedTypes('trainer', 'string');
        $resolver->setAllowedTypes('commute', 'string');
        $resolver->setAllowedTypes('external_id', 'string');
    }

    public function execute(): Upload
    {
        return $this->single(Upload::class);
    }

    protected function url(array $urlOptions): string
    {
        return 'uploads';
    }

    protected function method(): string
    {
        return 'POST';
    }

    protected function buildHeaders(?array $resolvedOptions): array
    {
        return ['Content-Type' => 'multipart/form-data'];
    }

    protected function buildBody(?array $resolvedOptions): string
    {
        /** @var File $file */
        $file = $this->bodyOptions['file'];
        $multiparts = [];
        $multiparts[] = [
            'name' => 'file',
            'contents' => fopen($file->getPathname(), 'rb'),
            'filename' => $this->bodyOptions['name'],
        ];

        foreach (['data_type', 'description', 'trainer', 'commute', 'external_id'] as $item) {
            if (isset($this->bodyOptions[$item]) && $content = $this->bodyOptions[$item]) {
                $multiparts[] = [
                    'name' => $item,
                    'contents' => $content,
                ];
            }
        }

        return (new MultipartStream($multiparts))->__toString();
    }
}
