<?php

namespace Lsv\Strava\Request\Uploads;

use Lsv\Strava\Client;
use Lsv\Strava\Model\Upload;
use Lsv\Strava\Normalizer\Normalizer;
use Lsv\Strava\Request\AbstractRequest;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Returns an upload for a given identifier.
 *
 * Requires activity:write scope.
 */
class GetUpload extends AbstractRequest
{
    /**
     * @param string|Upload $upload
     */
    public function __construct(Client $client, $upload)
    {
        parent::__construct($client);
        $this->setByUpload($upload);
    }

    public function setUploadId($uploadId): self
    {
        $this->urlOptions['uploadId'] = $uploadId;

        return $this;
    }

    public function setUpload(Upload $upload): self
    {
        $this->urlOptions['uploadId'] = $upload;

        return $this;
    }

    protected function setByUpload($upload): self
    {
        if (!is_object($upload)) {
            return $this->setUploadId($upload);
        }

        if ($upload instanceof Upload) {
            return $this->setUpload($upload);
        }

        throw new \InvalidArgumentException('Can not set upload with the requested object');
    }

    protected function validateUrlOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired(['uploadId']);
        $resolver->setAllowedTypes('uploadId', ['int', 'string', Upload::class]);
        $resolver->setNormalizer('uploadId', Normalizer::classToId());
    }

    public function execute(): Upload
    {
        return $this->single(Upload::class);
    }

    protected function url(array $urlOptions): string
    {
        return "uploads/{$urlOptions['uploadId']}";
    }
}
