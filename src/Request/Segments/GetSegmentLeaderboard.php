<?php

namespace Lsv\Strava\Request\Segments;

use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedClub;
use Lsv\Strava\Model\DetailedSegment;
use Lsv\Strava\Model\MetaClub;
use Lsv\Strava\Model\SegmentLeaderboard;
use Lsv\Strava\Model\SummaryClub;
use Lsv\Strava\Model\SummarySegment;
use Lsv\Strava\Request\AbstractRequest;
use Lsv\Strava\Request\Traits\PaginationTrait;
use Lsv\Strava\Request\Traits\SegmentUrlTrait;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * List of the authenticated athlete's starred segments.
 *
 * Private segments are filtered out unless requested by a token with read_all scope.
 */
class GetSegmentLeaderboard extends AbstractRequest
{
    use SegmentUrlTrait;
    use PaginationTrait {
        validateQueryOptions as protected paginationQueryOptions;
    }

    public const AGE_GROUPS = [
        '0_19' => ['min' => 0, 'max' => 19],
        '20_24' => ['min' => 20, 'max' => 24],
        '25_34' => ['min' => 25, 'max' => 34],
        '35_44' => ['min' => 35, 'max' => 44],
        '45_54' => ['min' => 45, 'max' => 54],
        '55_64' => ['min' => 55, 'max' => 64],
        '65_69' => ['min' => 65, 'max' => 69],
        '70_74' => ['min' => 70, 'max' => 74],
        '75_plus' => ['min' => 75, 'max' => 9999],
    ];

    public const WEIGHT_CLASS = [
        '0_54' => ['min' => 0, 'max' => 54],
        '55_64' => ['min' => 55, 'max' => 64],
        '65_74' => ['min' => 65, 'max' => 74],
        '75_84' => ['min' => 75, 'max' => 84],
        '85_94' => ['min' => 85, 'max' => 94],
        '95_104' => ['min' => 95, 'max' => 104],
        '105_114' => ['min' => 105, 'max' => 114],
        '115_plus' => ['min' => 115, 'max' => 124],
        '125_149' => ['min' => 125, 'max' => 149],
        '150_164' => ['min' => 150, 'max' => 164],
        '165_179' => ['min' => 165, 'max' => 179],
        '180_199' => ['min' => 180, 'max' => 199],
        '200_224' => ['min' => 200, 'max' => 224],
        '225_249' => ['min' => 225, 'max' => 249],
        '250_plus' => ['min' => 250, 'max' => 99999],
    ];

    public const DATE_RANGE = [
        'this_year',
        'this_month',
        'this_week',
        'today',
    ];

    /**
     * @param string|SummarySegment|DetailedSegment $segment
     */
    public function __construct(Client $client, $segment)
    {
        parent::__construct($client);
        $this->setBySegment($segment);
    }

    public function setGender(string $gender): self
    {
        $this->queryOptions['gender'] = $gender;

        return $this;
    }

    public function setAgeGroup(string $ageGroup): self
    {
        $this->queryOptions['age_group'] = $ageGroup;

        return $this;
    }

    public function setAge(int $age): self
    {
        $this->queryOptions['age_group'] = $this->calculateAgeGroupFromAge($age);

        return $this;
    }

    public function setWeightClass(string $weightClass): self
    {
        $this->queryOptions['weight_class'] = $weightClass;

        return $this;
    }

    public function setWeight(float $weight): self
    {
        $this->queryOptions['weight_class'] = $this->calculateWeightClassFromWeight($weight);

        return $this;
    }

    public function setFollowing(): self
    {
        $this->queryOptions['following'] = 1;

        return $this;
    }

    /**
     * @param MetaClub|SummaryClub|DetailedClub $club
     */
    public function setClub(MetaClub $club): self
    {
        return $this->setClubId($club->id);
    }

    public function setClubId($clubId): self
    {
        $this->queryOptions['club_id'] = $clubId;

        return $this;
    }

    public function setDateRange(string $dateRange): self
    {
        $this->queryOptions['date_range'] = $dateRange;

        return $this;
    }

    public function execute(): SegmentLeaderboard
    {
        return $this->single(SegmentLeaderboard::class);
    }

    protected function validateQueryOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefined(
            ['gender', 'age_group', 'weight_class', 'following', 'club_id', 'date_range']
        );
        $resolver->setAllowedValues('gender', ['M', 'F']);
        $resolver->setAllowedValues('age_group', array_keys(self::AGE_GROUPS));
        $resolver->setAllowedValues('weight_class', array_keys(self::WEIGHT_CLASS));
        $resolver->setAllowedValues('following', 1);
        $resolver->setAllowedTypes('club_id', ['int', 'string']);
        $resolver->setAllowedValues('date_range', self::DATE_RANGE);
        $this->paginationQueryOptions($resolver);
    }

    protected function url(array $urlOptions): string
    {
        return "segments/{$urlOptions['id']}/leaderboard";
    }

    private function calculateWeightClassFromWeight(float $weight)
    {
        foreach (self::WEIGHT_CLASS as $key => $class) {
            if ($weight <= $class['max'] && $weight >= $class['min']) {
                return $key;
            }
        }

        throw new \InvalidArgumentException("Can not find a weight class for the weight '{$weight}'");
    }

    private function calculateAgeGroupFromAge(int $age): string
    {
        foreach (self::AGE_GROUPS as $key => $ageGroup) {
            if ($age <= $ageGroup['max'] && $age >= $ageGroup['min']) {
                return $key;
            }
        }

        throw new \InvalidArgumentException("Can not find a age group for the age '{$age}'");
    }
}
