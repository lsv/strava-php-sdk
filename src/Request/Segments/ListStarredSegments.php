<?php

namespace Lsv\Strava\Request\Segments;

use Lsv\Strava\Model\DetailedSegment;
use Lsv\Strava\Request\AbstractRequest;
use Lsv\Strava\Request\Traits\PaginationTrait;

/**
 * List of the authenticated athlete's starred segments.
 *
 * Private segments are filtered out unless requested by a token with read_all scope.
 */
class ListStarredSegments extends AbstractRequest
{
    use PaginationTrait;

    /**
     * @return DetailedSegment[]
     */
    public function execute(): array
    {
        return $this->multiple(DetailedSegment::class);
    }

    protected function url(array $urlOptions): string
    {
        return 'segments/starred';
    }
}
