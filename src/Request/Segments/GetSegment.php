<?php

namespace Lsv\Strava\Request\Segments;

use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedSegment;
use Lsv\Strava\Model\SummarySegment;
use Lsv\Strava\Request\AbstractRequest;
use Lsv\Strava\Request\Traits\SegmentUrlTrait;

/**
 * Returns the specified segment.
 *
 * read_all scope required in order to retrieve athlete-specific segment information,
 * or to retrieve private segments.
 */
class GetSegment extends AbstractRequest
{
    use SegmentUrlTrait;

    /**
     * @param string|SummarySegment|DetailedSegment $segment
     */
    public function __construct(Client $client, $segment)
    {
        parent::__construct($client);
        $this->setBySegment($segment);
    }

    public function execute(): DetailedSegment
    {
        return $this->single(DetailedSegment::class);
    }

    protected function url(array $urlOptions): string
    {
        return "segments/{$urlOptions['id']}";
    }
}
