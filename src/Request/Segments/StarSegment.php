<?php

namespace Lsv\Strava\Request\Segments;

/**
 * Stars/Unstars the given segment for the authenticated athlete.
 *
 * Requires profile:write scope.
 */
class StarSegment extends GetSegment
{
    protected function method(): string
    {
        return 'PUT';
    }
}
