<?php

namespace Lsv\Strava\Request\Segments;

use Lsv\Strava\Client;
use Lsv\Strava\Model\ExplorerResponse;
use Lsv\Strava\Request\AbstractRequest;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Returns the top 10 segments matching a specified query.
 */
class ExploreSegments extends AbstractRequest
{
    public function __construct(
        Client $client,
        float $southwestCornerLatitude,
        float $southwestCornerLongitude,
        float $northEastCornerLatitude,
        float $northEastCornerLongitude
    ) {
        parent::__construct($client);
        $this->setBounds(
            $southwestCornerLatitude,
            $southwestCornerLongitude,
            $northEastCornerLatitude,
            $northEastCornerLongitude
        );
    }

    public function setBounds(
        float $southwestCornerLatitude,
        float $southwestCornerLongitude,
        float $northEastCornerLatitude,
        float $northEastCornerLongitude
    ): self {
        $this->queryOptions['bounds'] = [
            $southwestCornerLatitude,
            $southwestCornerLongitude,
            $northEastCornerLatitude,
            $northEastCornerLongitude,
        ];

        return $this;
    }

    public function setActivityType(string $type): self
    {
        $this->queryOptions['activity_type'] = $type;

        return $this;
    }

    public function setMinimumClimbingCategory(float $minimum): self
    {
        $this->queryOptions['min_cat'] = $minimum;

        return $this;
    }

    public function setMaximumClimbingCategory(float $maximum): self
    {
        $this->queryOptions['max_cat'] = $maximum;

        return $this;
    }

    public function execute(): ExplorerResponse
    {
        return $this->single(ExplorerResponse::class);
    }

    protected function validateQueryOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired(['bounds']);
        $resolver->setDefined(['activity_type', 'min_cat', 'max_cat']);
        $resolver->setAllowedTypes('bounds', ['array']);
        $resolver->setAllowedValues('activity_type', ['running', 'riding']);
        $resolver->setAllowedTypes('min_cat', ['int', 'float']);
        $resolver->setAllowedTypes('max_cat', ['int', 'float']);
    }

    protected function url(array $urlOptions): string
    {
        return 'segments/explore';
    }
}
