<?php

namespace Lsv\Strava\Request\Activities;

use Lsv\Strava\Model\DetailedActivity;
use Lsv\Strava\Normalizer\Normalizer;
use Lsv\Strava\Request\Traits\ActivityUrlTrait;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Returns the given activity that is owned by the authenticated athlete.
 *
 * Requires activity:read for Everyone and Followers activities.
 * Requires activity:read_all for Only Me activities.
 */
class GetActivity extends AbstractActivities
{
    use ActivityUrlTrait;

    /**
     * To include all segments efforts.
     */
    public function setIncludeAllEfforts(): self
    {
        $this->queryOptions['include_all_efforts'] = true;

        return $this;
    }

    public function execute(): DetailedActivity
    {
        return $this->single(DetailedActivity::class);
    }

    protected function validateQueryOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefined(['include_all_efforts']);
        $resolver->setAllowedTypes('include_all_efforts', ['bool', 'int']);
        $resolver->setNormalizer('include_all_efforts', Normalizer::boolToInteger());
    }

    protected function url(array $urlOptions): string
    {
        return "activities/{$urlOptions['id']}";
    }
}
