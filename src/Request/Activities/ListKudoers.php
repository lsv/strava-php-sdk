<?php

namespace Lsv\Strava\Request\Activities;

use Lsv\Strava\Model\SummaryAthlete;
use Lsv\Strava\Request\Traits\ActivityUrlTrait;
use Lsv\Strava\Request\Traits\PaginationTrait;

/**
 * Returns the athletes who kudoed an activity identified by an identifier.
 *
 * Requires activity:read for Everyone and Followers activities.
 * Requires activity:read_all for Only Me activities.
 */
class ListKudoers extends AbstractActivities
{
    use ActivityUrlTrait;
    use PaginationTrait;

    /**
     * @return SummaryAthlete[]
     */
    public function execute(): array
    {
        return $this->multiple(SummaryAthlete::class);
    }

    protected function url(array $urlOptions): string
    {
        return sprintf('activities/%s/kudos', $urlOptions['id']);
    }
}
