<?php

namespace Lsv\Strava\Request\Activities;

use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedActivity;
use Lsv\Strava\Model\MetaActivity;
use Lsv\Strava\Model\SummaryActivity;
use Lsv\Strava\Request\AbstractRequest;

abstract class AbstractActivities extends AbstractRequest
{
    /**
     * @param string|MetaActivity|SummaryActivity|DetailedActivity $activity The ID of an activity
     */
    public function __construct(Client $client, $activity)
    {
        parent::__construct($client);
        $this->setByActivity($activity);
    }

    abstract protected function setByActivity($activity);
}
