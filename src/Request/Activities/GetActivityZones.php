<?php

namespace Lsv\Strava\Request\Activities;

use Lsv\Strava\Model\ActivityZone;
use Lsv\Strava\Request\Traits\ActivityUrlTrait;

/**
 * Returns the zones of a given activity.
 *
 * Requires activity:read for Everyone and Followers activities.
 * Requires activity:read_all for Only Me activities.
 */
class GetActivityZones extends AbstractActivities
{
    use ActivityUrlTrait;

    /**
     * @return ActivityZone[]
     */
    public function execute(): array
    {
        return $this->multiple(ActivityZone::class);
    }

    protected function url(array $urlOptions): string
    {
        return sprintf('activities/%s/zones', $urlOptions['id']);
    }
}
