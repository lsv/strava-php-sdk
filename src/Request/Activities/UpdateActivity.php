<?php

namespace Lsv\Strava\Request\Activities;

use Lsv\Strava\Model\DetailedActivity;
use Lsv\Strava\Model\SummaryGear;
use Lsv\Strava\Normalizer\Normalizer;
use Lsv\Strava\Request\Traits\ActivityUrlTrait;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Updates the given activity that is owned by the authenticated athlete.
 *
 * Requires activity:write.
 * Also requires activity:read_all in order to update Only Me activities
 */
class UpdateActivity extends AbstractActivities
{
    use ActivityUrlTrait;

    /**
     * The name of the activity.
     */
    public function setName(string $name): self
    {
        $this->bodyOptions['name'] = $name;

        return $this;
    }

    /**
     * Type of activity. For example - Run, Ride etc.
     */
    public function setType(string $type): self
    {
        $this->bodyOptions['type'] = $type;

        return $this;
    }

    /**
     * Description of the activity.
     */
    public function setDescription(string $description): self
    {
        $this->bodyOptions['description'] = $description;

        return $this;
    }

    /**
     * Set to true to mark as a trainer activity.
     */
    public function setTrainer(bool $trainer): self
    {
        $this->bodyOptions['trainer'] = $trainer;

        return $this;
    }

    /**
     * Set to 1 to mark as commute.
     */
    public function setCommute(bool $commute): self
    {
        $this->bodyOptions['commute'] = $commute;

        return $this;
    }

    /**
     * Identifier for the gear associated with the activity.
     * ‘none’ clears gear from activity.
     *
     * @param SummaryGear|string $gearId
     */
    public function setGearId($gearId): self
    {
        if ($gearId instanceof SummaryGear) {
            $gearId = $gearId->id;
        }

        $this->bodyOptions['gear_id'] = $gearId;

        return $this;
    }

    public function execute(): DetailedActivity
    {
        return $this->single(DetailedActivity::class);
    }

    protected function validateFormOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefined(['name', 'type', 'description', 'trainer', 'commute', 'gear_id']);

        $resolver->setAllowedTypes('name', 'string');
        $resolver->setAllowedTypes('type', 'string');
        $resolver->setAllowedTypes('description', 'string');
        $resolver->setAllowedTypes('trainer', 'bool');
        $resolver->setAllowedTypes('commute', ['bool']);
        $resolver->setAllowedTypes('gear_id', 'string');

        $resolver->setNormalizer('trainer', Normalizer::boolToInteger());
        $resolver->setNormalizer('commute', Normalizer::boolToInteger());
    }

    protected function url(array $urlOptions): string
    {
        return sprintf('activities/%s', $urlOptions['id']);
    }

    protected function method(): string
    {
        return 'PUT';
    }
}
