<?php

namespace Lsv\Strava\Request\Activities;

use Lsv\Strava\Model\Comment;
use Lsv\Strava\Request\Traits\ActivityUrlTrait;
use Lsv\Strava\Request\Traits\PaginationTrait;

/**
 * Returns the comments on the given activity.
 *
 * Requires activity:read for Everyone and Followers activities.
 * Requires activity:read_all for Only Me activities.
 */
class ListComments extends AbstractActivities
{
    use ActivityUrlTrait;
    use PaginationTrait;

    /**
     * @return Comment[]
     */
    public function execute(): array
    {
        return $this->multiple(Comment::class);
    }

    protected function url(array $urlOptions): string
    {
        return sprintf('activities/%s/comments', $urlOptions['id']);
    }
}
