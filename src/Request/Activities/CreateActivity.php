<?php

namespace Lsv\Strava\Request\Activities;

use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedActivity;
use Lsv\Strava\Normalizer\Normalizer;
use Lsv\Strava\Request\AbstractRequest;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Creates a manual activity for an athlete.
 *
 * Requires activity:write scope.
 */
class CreateActivity extends AbstractRequest
{
    /**
     * @param string $name        the name of the activity
     * @param string $type        Type of activity. For example - Run, Ride etc.
     * @param int    $elapsedTime activity elapsed time in seconds
     */
    public function __construct(Client $client, string $name, string $type, int $elapsedTime)
    {
        parent::__construct($client);
        $this->setName($name);
        $this->setType($type);
        $this->setElapsedTime($elapsedTime);
    }

    /**
     * The name of the activity.
     */
    public function setName(string $name): self
    {
        $this->bodyOptions['name'] = $name;

        return $this;
    }

    /**
     * Type of activity. For example - Run, Ride etc.
     */
    public function setType(string $type): self
    {
        $this->bodyOptions['type'] = $type;

        return $this;
    }

    /**
     * The date of the activity.
     */
    public function setStartDate(\DateTime $startDate): self
    {
        $this->bodyOptions['start_date_local'] = $startDate;

        return $this;
    }

    /**
     * Activity elapsed time in seconds.
     */
    public function setElapsedTime(int $elapsedTime): self
    {
        $this->bodyOptions['elapsed_time'] = $elapsedTime;

        return $this;
    }

    /**
     * Description of the activity.
     */
    public function setDescription(string $description): self
    {
        $this->bodyOptions['description'] = $description;

        return $this;
    }

    /**
     * Activity distance in meters.
     */
    public function setDistance(float $distance): self
    {
        $this->bodyOptions['distance'] = $distance;

        return $this;
    }

    /**
     * Set to true to mark as a trainer activity.
     */
    public function setTrainer(bool $trainer): self
    {
        $this->bodyOptions['trainer'] = $trainer;

        return $this;
    }

    /**
     * List of native photo ids to attach to the activity.
     */
    public function setPhotoIds(array $ids): self
    {
        $this->bodyOptions['photo_ids'] = $ids;

        return $this;
    }

    /**
     * Set to 1 to mark as commute.
     */
    public function setCommute(bool $commute): self
    {
        $this->bodyOptions['commute'] = $commute;

        return $this;
    }

    public function execute(): DetailedActivity
    {
        return $this->single(DetailedActivity::class);
    }

    protected function validateFormOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired(['name', 'type', 'start_date_local', 'elapsed_time']);
        $resolver->setDefined(['description', 'distance', 'trainer', 'photo_ids', 'commute']);

        $resolver->setDefault('start_date_local', new \DateTime());

        $resolver->setAllowedTypes('name', 'string');
        $resolver->setAllowedTypes('type', 'string');
        $resolver->setAllowedTypes('start_date_local', \DateTime::class);
        $resolver->setAllowedTypes('elapsed_time', 'int');
        $resolver->setAllowedTypes('description', 'string');
        $resolver->setAllowedTypes('distance', 'float');
        $resolver->setAllowedTypes('trainer', 'bool');
        $resolver->setAllowedTypes('photo_ids', 'array');
        $resolver->setAllowedTypes('commute', ['bool']);

        $resolver->setNormalizer('start_date_local', Normalizer::dateTime());
        $resolver->setNormalizer('trainer', Normalizer::boolToInteger());
        $resolver->setNormalizer('photo_ids', Normalizer::arrayToCommaList());
        $resolver->setNormalizer('commute', Normalizer::boolToInteger());
    }

    /**
     * The URL to the method.
     */
    protected function url(array $urlOptions): string
    {
        return 'activities';
    }

    protected function method(): string
    {
        return 'POST';
    }
}
