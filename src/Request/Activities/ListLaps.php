<?php

namespace Lsv\Strava\Request\Activities;

use Lsv\Strava\Model\Lap;
use Lsv\Strava\Request\Traits\ActivityUrlTrait;

/**
 * Returns the laps of an activity identified by an identifier.
 *
 * Requires activity:read for Everyone and Followers activities.
 * Requires activity:read_all for Only Me activities.
 */
class ListLaps extends AbstractActivities
{
    use ActivityUrlTrait;

    /**
     * @return Lap[]
     */
    public function execute(): array
    {
        return $this->multiple(Lap::class);
    }

    protected function url(array $urlOptions): string
    {
        return sprintf('activities/%s/laps', $urlOptions['id']);
    }
}
