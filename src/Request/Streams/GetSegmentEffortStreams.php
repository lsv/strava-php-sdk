<?php

namespace Lsv\Strava\Request\Streams;

use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedSegmentEffort;
use Lsv\Strava\Model\SummarySegmentEffort;
use Lsv\Strava\Request\Traits\SegmentEffortUrlTrait;

/**
 * Returns a set of streams for a segment effort completed by the authenticated athlete.
 *
 * Requires read_all scope.
 */
class GetSegmentEffortStreams extends AbstractStreams
{
    use SegmentEffortUrlTrait;

    /**
     * @param string|SummarySegmentEffort|DetailedSegmentEffort $segmentEffort
     */
    public function __construct(Client $client, $segmentEffort, array $keys)
    {
        parent::__construct($client, $keys);
        $this->setBySegmentEffort($segmentEffort);
    }

    protected function url(array $urlOptions): string
    {
        return "segment_efforts/{$urlOptions['id']}/streams";
    }
}
