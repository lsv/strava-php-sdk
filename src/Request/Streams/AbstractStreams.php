<?php

namespace Lsv\Strava\Request\Streams;

use Lsv\Strava\Client;
use Lsv\Strava\Model\StreamSet;
use Lsv\Strava\Request\AbstractRequest;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractStreams extends AbstractRequest
{
    public function __construct(Client $client, array $keys)
    {
        parent::__construct($client);
        $this->setKeys($keys);
    }

    public function setKeys(array $keys): self
    {
        $this->queryOptions['keys'] = $keys;

        return $this;
    }

    protected function validateQueryOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired(['keys', 'key_by_type']);
        $resolver->setDefault('key_by_type', true);
        $resolver->setAllowedTypes('keys', 'array');
    }

    /**
     * @return StreamSet[]
     */
    public function execute(): array
    {
        return $this->multiple(StreamSet::class);
    }
}
