<?php

namespace Lsv\Strava\Request\Streams;

use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedSegment;
use Lsv\Strava\Model\SummarySegment;
use Lsv\Strava\Request\Traits\SegmentUrlTrait;

/**
 * Returns a set of streams for a segment effort completed by the authenticated athlete.
 *
 * Requires read_all scope.
 */
class GetSegmentStreams extends AbstractStreams
{
    use SegmentUrlTrait;

    /**
     * @param string|SummarySegment|DetailedSegment $segment
     */
    public function __construct(Client $client, $segment, array $keys)
    {
        parent::__construct($client, $keys);
        $this->setBySegment($segment);
    }

    protected function url(array $urlOptions): string
    {
        return "segments/{$urlOptions['id']}/streams";
    }
}
