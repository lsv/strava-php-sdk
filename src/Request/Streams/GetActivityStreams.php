<?php

namespace Lsv\Strava\Request\Streams;

use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedActivity;
use Lsv\Strava\Model\MetaActivity;
use Lsv\Strava\Model\SummaryActivity;
use Lsv\Strava\Request\Traits\ActivityUrlTrait;

/**
 * Returns the given activity's streams.
 *
 * Requires activity:read scope.
 * Requires activity:read_all scope for Only Me activities.
 */
class GetActivityStreams extends AbstractStreams
{
    use ActivityUrlTrait;

    /**
     * @param string|MetaActivity|SummaryActivity|DetailedActivity $activity
     */
    public function __construct(Client $client, $activity, array $keys)
    {
        parent::__construct($client, $keys);
        $this->setByActivity($activity);
    }

    protected function url(array $urlOptions): string
    {
        return "activities/{$urlOptions['id']}/streams";
    }
}
