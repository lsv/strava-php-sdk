<?php

namespace Lsv\Strava\Request\RunningRaces;

use Lsv\Strava\Client;
use Lsv\Strava\Model\RunningRace;
use Lsv\Strava\Request\AbstractRequest;
use Lsv\Strava\Request\Traits\RaceUrlTrait;

/**
 * Returns a running race for a given identifier.
 */
class GetRunningRace extends AbstractRequest
{
    use RaceUrlTrait;

    /**
     * @param string|RunningRace $race
     */
    public function __construct(Client $client, $race)
    {
        parent::__construct($client);
        $this->setByRace($race);
    }

    public function execute(): RunningRace
    {
        return $this->single(RunningRace::class);
    }

    protected function url(array $urlOptions): string
    {
        return "running_races/{$urlOptions['id']}";
    }
}
