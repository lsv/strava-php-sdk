<?php

namespace Lsv\Strava\Request\RunningRaces;

use Lsv\Strava\Model\RunningRace;
use Lsv\Strava\Request\AbstractRequest;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Returns a list running races based on a set of search criteria.
 */
class ListRunningRaces extends AbstractRequest
{
    public function setYear(int $year): self
    {
        $this->queryOptions['year'] = $year;

        return $this;
    }

    protected function validateQueryOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefined(['year']);
        $resolver->setAllowedTypes('year', 'int');
    }

    /**
     * @return RunningRace[]
     */
    public function execute(): array
    {
        return $this->multiple(RunningRace::class);
    }

    protected function url(array $urlOptions): string
    {
        return 'running_races';
    }
}
