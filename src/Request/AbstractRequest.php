<?php

namespace Lsv\Strava\Request;

use Http\Client\Exception\HttpException;
use Http\Message\MessageFactory\GuzzleMessageFactory;
use Lsv\Strava\Build;
use Lsv\Strava\Client;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractRequest
{
    private const BASE_URL = 'https://www.strava.com/api/v3';

    /**
     * @var array
     */
    protected $bodyOptions = [];

    /**
     * @var array
     */
    protected $urlOptions = [];

    /**
     * @var array
     */
    protected $queryOptions = [];

    /**
     * @var array
     */
    private $resolvedBodyOptions = [];

    /**
     * @var Client
     */
    private $client;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var ResponseInterface
     */
    private $response;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Execute the method.
     */
    abstract public function execute();

    public function getRequest(): RequestInterface
    {
        if ($this->request instanceof RequestInterface) {
            return $this->request;
        }

        if (!$this->resolvedBodyOptions) {
            $resolver = new OptionsResolver();
            $this->validateFormOptions($resolver);
            $this->resolvedBodyOptions = $resolver->resolve($this->bodyOptions);
        }

        $headers = $this->buildHeaders($this->resolvedBodyOptions);
        $body = $this->buildBody($this->resolvedBodyOptions);

        $this->request = (new GuzzleMessageFactory())
            ->createRequest(
                $this->method(),
                $this->getUrl(),
                $headers,
                $body
            );

        $this->request = $this->client->authenticate($this->request);

        return $this->request;
    }

    public function getResponse(): ResponseInterface
    {
        if ($this->response instanceof ResponseInterface) {
            return $this->response;
        }

        $this->getRequest();

        return $this->response = $this->buildResponse($this->request);
    }

    abstract protected function url(array $urlOptions): string;

    protected function single($className)
    {
        return Build::single(
            json_decode($this->doExecute(), true, 512, JSON_BIGINT_AS_STRING),
            $className
        );
    }

    protected function multiple($className): array
    {
        return Build::multiple(
            json_decode($this->doExecute(), true, 512, JSON_BIGINT_AS_STRING),
            $className
        );
    }

    protected function buildHeaders(?array $resolvedOptions): array
    {
        $headers = [];
        if ($resolvedOptions) {
            $headers['Content-Type'] = 'application/x-www-form-urlencoded';
        }

        return $headers;
    }

    protected function buildBody(?array $resolvedOptions): string
    {
        $body = '';

        if ($resolvedOptions) {
            $body = http_build_query($resolvedOptions, '', '&');
        }

        return $body;
    }

    /**
     * HTTP method.
     */
    protected function method(): string
    {
        return 'GET';
    }

    protected function validateFormOptions(OptionsResolver $resolver): void
    {
    }

    protected function validateUrlOptions(OptionsResolver $resolver): void
    {
    }

    protected function validateQueryOptions(OptionsResolver $resolver): void
    {
    }

    protected function buildResponse(RequestInterface $request): ResponseInterface
    {
        return $this->client->send($request);
    }

    private function getUrl(): string
    {
        $urlResolver = new OptionsResolver();
        $this->validateUrlOptions($urlResolver);

        $queryResolver = new OptionsResolver();
        $this->validateQueryOptions($queryResolver);

        $url = self::BASE_URL.'/'.$this->url($urlResolver->resolve($this->urlOptions));
        if ($queryOptions = $queryResolver->resolve($this->queryOptions)) {
            $url .= '?'.http_build_query($queryOptions);
        }

        return $url;
    }

    /**
     * @return mixed
     */
    protected function doExecute()
    {
        try {
            $this->getResponse();
        } catch (HttpException $exception) {
            if (401 === $exception->getResponse()->getStatusCode()) {
                // Throttling
                if (
                    $exception->getResponse()->hasHeader('X-RateLimit-Limit')
                    && $exception->getResponse()->hasHeader('X-RateLimit-Usage')
                ) {
                    [$minLimit, $dailyLimit] = explode(
                        ',',
                        $exception->getResponse()->getHeader('X-RateLimit-Limit')[0]
                    );
                    [$minUsage, $dailyUsage] = explode(
                        ',',
                        $exception->getResponse()->getHeader('X-RateLimit-Usage')[0]
                    );

                    if ($minUsage > $minLimit) {
                        throw new HttpException('15-minute request limit reached', $exception->getRequest(), $exception->getResponse());
                    }

                    if ($dailyUsage > $dailyLimit) {
                        throw new HttpException('Daily request limit reached', $exception->getRequest(), $exception->getResponse());
                    }
                }

                // Or access denied
                throw $exception;
            }
        }

        return $this->response->getBody();
    }
}
