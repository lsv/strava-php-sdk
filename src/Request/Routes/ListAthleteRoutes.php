<?php

namespace Lsv\Strava\Request\Routes;

use Lsv\Strava\Client;
use Lsv\Strava\Model\Route;
use Lsv\Strava\Request\AbstractRequest;
use Lsv\Strava\Request\Traits\AthleteUrlTrait;
use Lsv\Strava\Request\Traits\PaginationTrait;

/**
 * Returns a list of the routes created by the authenticated athlete using their athlete ID.
 * Private routes are filtered out unless requested by a token with read_all scope.
 */
class ListAthleteRoutes extends AbstractRequest
{
    use AthleteUrlTrait;
    use PaginationTrait;

    public function __construct(Client $client, $athlete)
    {
        parent::__construct($client);
        $this->setByAthlete($athlete);
    }

    /**
     * @return Route[]
     */
    public function execute(): array
    {
        return $this->multiple(Route::class);
    }

    protected function url(array $urlOptions): string
    {
        return "athletes/{$urlOptions['id']}/routes";
    }
}
