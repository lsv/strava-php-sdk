<?php

namespace Lsv\Strava\Request\Routes;

use Lsv\Strava\Model\Route;
use Lsv\Strava\Request\Traits\RouteUrlTrait;

/**
 * Returns a route using its identifier.
 *
 * Requires read_all scope for private routes.
 */
class GetRoute extends AbstractRoutesRequest
{
    use RouteUrlTrait;

    public function execute(): Route
    {
        return $this->single(Route::class);
    }

    protected function url(array $urlOptions): string
    {
        return "routes/{$urlOptions['id']}";
    }
}
