<?php

namespace Lsv\Strava\Request\Routes;

use Lsv\Strava\Client;
use Lsv\Strava\Request\AbstractRequest;

abstract class AbstractRoutesRequest extends AbstractRequest
{
    public function __construct(Client $client, $route)
    {
        parent::__construct($client);
        $this->setByRoute($route);
    }

    abstract protected function setByRoute($route);
}
