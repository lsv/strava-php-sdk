<?php

namespace Lsv\Strava\Request\Routes;

use Lsv\Strava\Request\Traits\RouteUrlTrait;

/**
 * Returns a GPX file of the route.
 * Requires read_all scope for private routes.
 */
class ExportRouteGpx extends AbstractRoutesRequest
{
    use RouteUrlTrait;

    /**
     * Execute the method.
     */
    public function execute(): string
    {
        return $this->doExecute();
    }

    protected function url(array $urlOptions): string
    {
        return "routes/{$urlOptions['id']}/export_gpx";
    }
}
