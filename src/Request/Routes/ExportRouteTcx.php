<?php

namespace Lsv\Strava\Request\Routes;

/**
 * Returns a TCX file of the route.
 * Requires read_all scope for private routes.
 */
class ExportRouteTcx extends ExportRouteGpx
{
    protected function url(array $urlOptions): string
    {
        return "routes/{$urlOptions['id']}/export_tcx";
    }
}
