<?php

namespace Lsv\Strava\Request\Traits;

trait IdTrait
{
    /**
     * The unique identifier.
     *
     * @var string
     */
    public $id;

    protected function setId($id): void
    {
        $this->id = (string) $id;
    }
}
