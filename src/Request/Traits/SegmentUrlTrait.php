<?php

namespace Lsv\Strava\Request\Traits;

use Lsv\Strava\Model\DetailedSegment;
use Lsv\Strava\Model\SummarySegment;
use Lsv\Strava\Normalizer\Normalizer;
use Symfony\Component\OptionsResolver\OptionsResolver;

trait SegmentUrlTrait
{
    public function setSegmentId($id): self
    {
        $this->urlOptions['id'] = $id;

        return $this;
    }

    public function setSegment(SummarySegment $segment): self
    {
        $this->urlOptions['id'] = $segment;

        return $this;
    }

    protected function setBySegment($segment): self
    {
        if (!is_object($segment)) {
            return $this->setSegmentId($segment);
        }

        if ($segment instanceof SummarySegment) {
            return $this->setSegment($segment);
        }

        throw new \InvalidArgumentException('Can not set segment with the requested object');
    }

    protected function validateUrlOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired(['id']);
        $resolver->setAllowedTypes('id', ['int', 'string', SummarySegment::class, DetailedSegment::class]);
        $resolver->setNormalizer('id', Normalizer::classToId());
    }
}
