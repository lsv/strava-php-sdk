<?php

namespace Lsv\Strava\Request\Traits;

use Symfony\Component\OptionsResolver\OptionsResolver;

trait PaginationTrait
{
    public function setPageNumber(int $pageNumber): self
    {
        $this->queryOptions['page'] = $pageNumber;

        return $this;
    }

    public function setPerPage(int $number = 30): self
    {
        $this->queryOptions['per_page'] = $number;

        return $this;
    }

    protected function validateQueryOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefined(['page', 'per_page']);
        $resolver->setAllowedTypes('page', 'int');
        $resolver->setAllowedTypes('per_page', 'int');
    }
}
