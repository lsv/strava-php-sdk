<?php

namespace Lsv\Strava\Request\Traits;

use Lsv\Strava\Model\SummaryGear;
use Symfony\Component\OptionsResolver\OptionsResolver;

trait GearUrlTrait
{
    /**
     * Set the ID from an summary gear.
     *
     * @var SummaryGear
     */
    public function setGear(SummaryGear $gear): self
    {
        return $this->setGearId($gear->id);
    }

    public function setGearId($id): self
    {
        $this->urlOptions['id'] = $id;

        return $this;
    }

    protected function setByGear($gear): self
    {
        if (!is_object($gear)) {
            return $this->setGearId($gear);
        }

        if ($gear instanceof SummaryGear) {
            return $this->setGear($gear);
        }

        throw new \InvalidArgumentException('Can not set gear with the requested object');
    }

    protected function validateUrlOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired(['id']);
        $resolver->setAllowedTypes('id', ['int', 'string']);
    }
}
