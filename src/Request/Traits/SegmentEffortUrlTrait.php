<?php

namespace Lsv\Strava\Request\Traits;

use Lsv\Strava\Model\DetailedSegmentEffort;
use Lsv\Strava\Model\SummarySegmentEffort;
use Lsv\Strava\Normalizer\Normalizer;
use Symfony\Component\OptionsResolver\OptionsResolver;

trait SegmentEffortUrlTrait
{
    public function setSegmentEffortId($id): self
    {
        $this->urlOptions['id'] = $id;

        return $this;
    }

    /**
     * @param DetailedSegmentEffort|SummarySegmentEffort $effort
     */
    public function setSegmentEffort(SummarySegmentEffort $effort): self
    {
        $this->urlOptions['id'] = $effort;

        return $this;
    }

    protected function setBySegmentEffort($segmentEffort): self
    {
        if (!is_object($segmentEffort)) {
            return $this->setSegmentEffortId($segmentEffort);
        }

        if ($segmentEffort instanceof SummarySegmentEffort) {
            return $this->setSegmentEffort($segmentEffort);
        }

        throw new \InvalidArgumentException('Can not set segmentEffort with the requested object');
    }

    protected function validateUrlOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired(['id']);
        $resolver->setAllowedTypes('id', ['int', 'string', SummarySegmentEffort::class, DetailedSegmentEffort::class]);
        $resolver->setNormalizer('id', Normalizer::classToId());
    }
}
