<?php

namespace Lsv\Strava\Request\Traits;

use Lsv\Strava\Model\RunningRace;
use Lsv\Strava\Normalizer\Normalizer;
use Symfony\Component\OptionsResolver\OptionsResolver;

trait RaceUrlTrait
{
    public function setRaceId($raceId): self
    {
        $this->urlOptions['id'] = $raceId;

        return $this;
    }

    public function setRace(RunningRace $race): self
    {
        $this->urlOptions['id'] = $race;

        return $this;
    }

    protected function setByRace($race): self
    {
        if (!is_object($race)) {
            return $this->setRaceId($race);
        }

        if ($race instanceof RunningRace) {
            return $this->setRace($race);
        }

        throw new \InvalidArgumentException('Can not set race with the requested object');
    }

    protected function validateUrlOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired(['id']);
        $resolver->setAllowedTypes('id', ['int', 'string', RunningRace::class]);
        $resolver->setNormalizer('id', Normalizer::classToId());
    }
}
