<?php

namespace Lsv\Strava\Request\Traits;

use Lsv\Strava\Model\DetailedActivity;
use Lsv\Strava\Model\MetaActivity;
use Lsv\Strava\Model\SummaryActivity;
use Lsv\Strava\Normalizer\Normalizer;
use Symfony\Component\OptionsResolver\OptionsResolver;

trait ActivityUrlTrait
{
    /**
     * Set the ID from an activity.
     *
     * @var MetaActivity|SummaryActivity|DetailedActivity
     */
    public function setActivity(MetaActivity $activity): self
    {
        $this->urlOptions['id'] = $activity;

        return $this;
    }

    public function setActivityId($id): self
    {
        $this->urlOptions['id'] = $id;

        return $this;
    }

    protected function setByActivity($activity): self
    {
        if (!is_object($activity)) {
            return $this->setActivityId($activity);
        }

        if ($activity instanceof MetaActivity) {
            return $this->setActivity($activity);
        }

        throw new \InvalidArgumentException('Can not set activity with the requested object');
    }

    protected function validateUrlOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired(['id']);
        $resolver->setAllowedTypes(
            'id',
            ['int', 'string', MetaActivity::class, SummaryActivity::class, DetailedActivity::class]
        );
        $resolver->setNormalizer('id', Normalizer::classToId());
    }
}
