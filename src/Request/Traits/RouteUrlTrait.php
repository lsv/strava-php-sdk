<?php

namespace Lsv\Strava\Request\Traits;

use Lsv\Strava\Model\Route;
use Lsv\Strava\Normalizer\Normalizer;
use Symfony\Component\OptionsResolver\OptionsResolver;

trait RouteUrlTrait
{
    public function setRouteId($id): self
    {
        $this->urlOptions['id'] = $id;

        return $this;
    }

    public function setRoute(Route $route): self
    {
        $this->urlOptions['id'] = $route;

        return $this;
    }

    protected function setByRoute($route): self
    {
        if (!is_object($route)) {
            return $this->setRouteId($route);
        }

        if ($route instanceof Route) {
            return $this->setRoute($route);
        }

        throw new \InvalidArgumentException('Can not set route with the requested object');
    }

    protected function validateUrlOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired(['id']);
        $resolver->setAllowedTypes('id', ['int', 'string', Route::class]);
        $resolver->setNormalizer('id', Normalizer::classToId());
    }
}
