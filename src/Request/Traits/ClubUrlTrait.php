<?php

namespace Lsv\Strava\Request\Traits;

use Lsv\Strava\Model\DetailedClub;
use Lsv\Strava\Model\MetaClub;
use Lsv\Strava\Model\SummaryClub;
use Symfony\Component\OptionsResolver\OptionsResolver;

trait ClubUrlTrait
{
    /**
     * The identifier of the club.
     *
     * @param string|int $id
     */
    public function setClubId($id): self
    {
        $this->urlOptions['id'] = $id;

        return $this;
    }

    /**
     * Set the ID from an club.
     *
     * @var MetaClub|SummaryClub|DetailedClub
     */
    public function setClub(MetaClub $club): self
    {
        return $this->setClubId($club->id);
    }

    protected function setByClub($club): self
    {
        if (!is_object($club)) {
            return $this->setClubId($club);
        }

        if ($club instanceof MetaClub) {
            return $this->setClub($club);
        }

        throw new \InvalidArgumentException('Can not set club with the requested object');
    }

    protected function validateUrlOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired(['id']);
        $resolver->setAllowedTypes('id', ['int', 'string']);
    }
}
