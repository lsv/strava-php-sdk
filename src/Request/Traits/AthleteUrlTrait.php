<?php

namespace Lsv\Strava\Request\Traits;

use Lsv\Strava\Model\DetailedAthlete;
use Lsv\Strava\Model\MetaAthlete;
use Lsv\Strava\Model\SummaryAthlete;
use Symfony\Component\OptionsResolver\OptionsResolver;

trait AthleteUrlTrait
{
    public function setAthleteId($id): self
    {
        $this->urlOptions['id'] = $id;

        return $this;
    }

    /**
     * Set the ID from an athlete.
     *
     * @var MetaAthlete|SummaryAthlete|DetailedAthlete
     */
    public function setAthlete($athlete): self
    {
        return $this->setAthleteId($athlete->id);
    }

    protected function setByAthlete($athlete): self
    {
        if (!is_object($athlete)) {
            return $this->setAthleteId($athlete);
        }

        if ($athlete instanceof MetaAthlete) {
            return $this->setAthlete($athlete);
        }

        throw new \InvalidArgumentException('Can not set athlete with the requested object');
    }

    protected function validateUrlOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired(['id']);
        $resolver->setAllowedTypes(
            'id',
            ['int', 'string', MetaAthlete::class, SummaryAthlete::class, DetailedAthlete::class]
        );
    }
}
