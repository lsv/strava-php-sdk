<?php

namespace Lsv\Strava\Request\SegmentEffort;

use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedSegmentEffort;
use Lsv\Strava\Model\SummarySegmentEffort;
use Lsv\Strava\Request\AbstractRequest;
use Lsv\Strava\Request\Traits\SegmentEffortUrlTrait;

/**
 * Returns a segment effort from an activity that is owned by the authenticated athlete.
 */
class GetSegmentEffort extends AbstractRequest
{
    use SegmentEffortUrlTrait;

    /**
     * @param string|SummarySegmentEffort|DetailedSegmentEffort $segmentEffort
     */
    public function __construct(Client $client, $segmentEffort)
    {
        parent::__construct($client);
        $this->setBySegmentEffort($segmentEffort);
    }

    public function execute(): DetailedSegmentEffort
    {
        return $this->single(DetailedSegmentEffort::class);
    }

    protected function url(array $urlOptions): string
    {
        return "segment_efforts/{$urlOptions['id']}";
    }
}
