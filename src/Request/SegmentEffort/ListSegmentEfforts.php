<?php

namespace Lsv\Strava\Request\SegmentEffort;

use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedSegment;
use Lsv\Strava\Model\SummarySegment;
use Lsv\Strava\Model\SummarySegmentEffort;
use Lsv\Strava\Request\AbstractRequest;
use Lsv\Strava\Request\Traits\PaginationTrait;
use Lsv\Strava\Request\Traits\SegmentUrlTrait;

/**
 * Returns a set of the authenticated athlete's segment efforts for a given segment.
 */
class ListSegmentEfforts extends AbstractRequest
{
    use PaginationTrait;
    use SegmentUrlTrait;

    /**
     * @param string|SummarySegment|DetailedSegment $segment
     */
    public function __construct(Client $client, $segment)
    {
        parent::__construct($client);
        $this->setBySegment($segment);
    }

    /**
     * @return SummarySegmentEffort[]
     */
    public function execute(): array
    {
        return $this->multiple(SummarySegmentEffort::class);
    }

    protected function url(array $urlOptions): string
    {
        return "segments/{$urlOptions['id']}/all_efforts";
    }
}
