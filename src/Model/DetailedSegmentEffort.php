<?php

namespace Lsv\Strava\Model;

class DetailedSegmentEffort extends SummarySegmentEffort
{
    /**
     * The effort's average cadence.
     *
     * @var float
     */
    public $averageCadence;

    /**
     * The heart heart rate of the athlete during this effort.
     *
     * @var float
     */
    public $averageHeartrate;

    /**
     * The maximum heart rate of the athlete during this effort.
     *
     * @var float
     */
    public $maxHeartrate;

    /**
     * Whether this effort should be hidden when viewed within an activity.
     *
     * @var bool
     */
    public $hidden;

    /**
     * @var AthleteSegmentStats
     */
    public $athleteSegmentStats;

    protected function setAthleteSegmentStats(AthleteSegmentStats $stats): void
    {
        $this->athleteSegmentStats = $stats;
    }
}
