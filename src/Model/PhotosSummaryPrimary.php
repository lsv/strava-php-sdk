<?php

namespace Lsv\Strava\Model;

use Lsv\Strava\Request\Traits\IdTrait;

class PhotosSummaryPrimary
{
    use IdTrait;

    /**
     * @var int
     */
    public $source;

    /**
     * @var string
     */
    public $uniqueId;

    /**
     * @var array<string>
     */
    public $urls;
}
