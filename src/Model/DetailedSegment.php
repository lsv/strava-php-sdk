<?php

namespace Lsv\Strava\Model;

class DetailedSegment extends SummarySegment
{
    /**
     * @var \DateTime
     */
    public $createdAt;

    /**
     * @var \DateTime
     */
    public $updatedAt;

    /**
     * @var float
     */
    public $totalElevationGain;

    /**
     * @var PolylineMap
     */
    public $map;

    /**
     * @var int
     */
    public $effortCount;

    /**
     * @var int
     */
    public $athleteCount;

    /**
     * @var int
     */
    public $starCount;

    /**
     * @var AthleteSegmentStats
     */
    public $athleteSegmentStats;

    protected function setCreatedAt($time): void
    {
        $this->createdAt = new \DateTime($time);
    }

    protected function setUpdatedAt($time): void
    {
        $this->updatedAt = new \DateTime($time);
    }

    protected function setMap(PolylineMap $map): void
    {
        $this->map = $map;
    }

    protected function setAthleteSegmentStats(AthleteSegmentStats $stats): void
    {
        $this->athleteSegmentStats = $stats;
    }
}
