<?php

namespace Lsv\Strava\Model;

use Lsv\Strava\Build;

class DetailedAthlete extends SummaryAthlete
{
    /**
     * The athlete's follower count.
     *
     * @var int
     */
    public $followerCount;

    /**
     * The athlete's friend count.
     *
     * @var int
     */
    public $friendCount;

    /**
     * The number or athletes mutually followed by this athlete and the currently logged-in athlete.
     *
     * @var int
     */
    public $mutualFriendCount;

    /**
     * @var int
     */
    public $athleteType;

    /**
     * @var string
     */
    public $datePreference;

    /**
     * The athlete's preferred unit system. May take one of the following values: feet, meters.
     *
     * @var string
     */
    public $measurementPreference;

    /**
     * The athlete's clubs.
     *
     * @var Club[]
     */
    public $clubs;

    /**
     * The athlete's FTP (Functional Threshold Power).
     *
     * @var int
     */
    public $ftp;

    /**
     * The athlete's weight.
     *
     * @var float
     */
    public $weight;

    /**
     * The athlete's bikes.
     *
     * @var SummaryGear[]
     */
    public $bikes;

    /**
     * The athlete's shoes.
     *
     * @var SummaryGear[]
     */
    public $shoes;

    protected function setClubs(array $data): void
    {
        $this->clubs = Build::multiple($data, Club::class);
    }

    protected function setBikes(array $data): void
    {
        $this->bikes = Build::multiple($data, SummaryGear::class);
    }

    protected function setShoes(array $data): void
    {
        $this->shoes = Build::multiple($data, SummaryGear::class);
    }
}
