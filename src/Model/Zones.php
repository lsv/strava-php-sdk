<?php

namespace Lsv\Strava\Model;

use Lsv\Strava\Build;
use Lsv\Strava\Model\Traits\ResourceStateTrait;

class Zones
{
    use ResourceStateTrait;

    /**
     * @var string
     */
    public $type;

    /**
     * @var bool
     */
    public $sensorBased;

    /**
     * @var DistrubutionBucket[]
     */
    public $distributionBuckets;

    protected function setDistributionBuckets(array $data): void
    {
        $this->distributionBuckets = Build::multiple($data, DistrubutionBucket::class);
    }
}
