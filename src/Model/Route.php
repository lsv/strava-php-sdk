<?php

namespace Lsv\Strava\Model;

use Lsv\Strava\Build;
use Lsv\Strava\Request\Traits\IdTrait;

class Route
{
    use IdTrait;

    /**
     * @var MetaAthlete
     */
    public $athlete;

    /**
     * The description of the route.
     *
     * @var string
     */
    public $description;

    /**
     * The route's distance, in meters.
     *
     * @var float
     */
    public $distance;

    /**
     * The route's elevation gain.
     *
     * @var float
     */
    public $elevationGain;

    /**
     * @var PolylineMap
     */
    public $map;

    /**
     * The name of this route.
     *
     * @var string
     */
    public $name;

    /**
     * Whether this route is private.
     *
     * @var bool
     */
    public $private;

    /**
     * Whether this route is starred by the logged-in athlete.
     *
     * @var bool
     */
    public $starred;

    /**
     * @var int
     */
    public $timestamp;

    /**
     * This route's type (1 for ride, 2 for runs).
     *
     * @var int
     */
    public $type;

    /**
     * This route's sub-type (1 for road, 2 for mountain bike, 3 for cross, 4 for trail, 5 for mixed).
     *
     * @var int
     */
    public $subType;

    /**
     * The segments traversed by this route.
     *
     * @var SummarySegment[]
     */
    public $segments;

    /**
     * The directions of this route.
     *
     * @var RouteDirection[]
     */
    public $directions;

    protected function setAthlete(MetaAthlete $athlete): void
    {
        $this->athlete = $athlete;
    }

    protected function setMap(PolylineMap $map): void
    {
        $this->map = $map;
    }

    protected function setSegments(array $data): void
    {
        $this->segments = Build::multiple($data, SummarySegment::class);
    }

    protected function setDirections(array $data): void
    {
        $this->directions = Build::multiple($data, RouteDirection::class);
    }
}
