<?php

namespace Lsv\Strava\Model;

use Lsv\Strava\Build;

class SegmentLeaderboard
{
    /**
     * The total number of entries for this leaderboard.
     *
     * @var int
     */
    public $entryCount;

    /**
     * May take one of the following values: kom, cr.
     *
     * @var string
     */
    public $komType;

    /**
     * @var SegmentLeaderboardEntry[]
     */
    public $entries;

    protected function setEntries(array $data): void
    {
        $this->entries = Build::multiple($data, SegmentLeaderboardEntry::class);
    }

    protected function setEffortCount(int $data): void
    {
        $this->entryCount = $data;
    }
}
