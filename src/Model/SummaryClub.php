<?php

namespace Lsv\Strava\Model;

use Lsv\Strava\Model\Traits\AddressTrait;

class SummaryClub extends MetaClub
{
    use AddressTrait;

    /**
     * URL to a 124x124 pixel profile picture.
     *
     * @var string
     */
    public $profile;

    /**
     * URL to a 60x60 pixel profile picture.
     *
     * @var string
     */
    public $profileMedium;

    /**
     * URL to a ~1185x580 pixel cover photo.
     *
     * @var string
     */
    public $coverPhoto;

    /**
     * URL to a ~360x176 pixel cover photo.
     *
     * @var string
     */
    public $coverPhotoSmall;

    /**
     * @var string
     */
    public $sportType;

    /**
     * Whether the club is private.
     *
     * @var bool
     */
    public $private;

    /**
     * The club's member count.
     *
     * @var int
     */
    public $memberCount;

    /**
     * Whether the club is featured or not.
     *
     * @var bool
     */
    public $featured;

    /**
     * Whether the club is verified or not.
     *
     * @var bool
     */
    public $verified;

    /**
     * The club's vanity URL.
     *
     * @var string
     */
    public $url;
}
