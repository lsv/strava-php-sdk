<?php

namespace Lsv\Strava\Model;

use Lsv\Strava\Request\Traits\IdTrait;

class Upload
{
    use IdTrait;

    /**
     * The identifier of the activity this upload resulted into.
     *
     * @var string
     */
    public $activityId;

    /**
     * The external identifier of the upload.
     *
     * @var string
     */
    public $externalId;

    /**
     * The error associated with this upload.
     *
     * @var string
     */
    public $error;

    /**
     * The status of this upload.
     *
     * @var string
     */
    public $status;

    protected function setActivityId($id): void
    {
        $this->activityId = (string) $id;
    }
}
