<?php

namespace Lsv\Strava\Model;

class LatLng
{
    /**
     * Latitude.
     *
     * @var float
     */
    public $latitude;

    /**
     * Longitude.
     *
     * @var float
     */
    public $longitude;

    protected function set0(float $latitude): void
    {
        $this->latitude = $latitude;
    }

    protected function set1(float $longitude): void
    {
        $this->longitude = $longitude;
    }
}
