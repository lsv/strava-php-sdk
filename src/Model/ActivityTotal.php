<?php

namespace Lsv\Strava\Model;

class ActivityTotal
{
    /**
     * The number of activities considered in this total.
     *
     * @var int
     */
    public $count;

    /**
     * The total distance covered by the considered activities.
     *
     * @var float
     */
    public $distance;

    /**
     * The total moving time of the considered activities.
     *
     * @var int
     */
    public $movingTime;

    /**
     * The total elapsed time of the considered activities.
     *
     * @var int
     */
    public $elapsedTime;

    /**
     * The total elevation gain of the considered activities.
     *
     * @var float
     */
    public $elevationGain;

    /**
     * The total number of achievements of the considered activities.
     *
     * @var int
     */
    public $achievementCount;
}
