<?php

namespace Lsv\Strava\Model;

use Lsv\Strava\Model\Traits\AddressTrait;

class SummaryAthlete extends MetaAthlete
{
    use AddressTrait;

    /**
     * @var string
     */
    public $username;

    /**
     * The athlete's first name.
     *
     * @var string
     */
    public $firstname;

    /**
     * The athlete's last name.
     *
     * @var string
     */
    public $lastname;

    /**
     * @var bool
     */
    public $showName;

    /**
     * @var string
     */
    public $displayName;

    /**
     * @var string
     */
    public $avatarUrl;

    /**
     * URL to a 62x62 pixel profile picture.
     *
     * @var string
     */
    public $profileMedium;

    /**
     * URL to a 124x124 pixel profile picture.
     *
     * @var string
     */
    public $profile;

    /**
     * The athlete's sex. May take one of the following values: M, F.
     *
     * @var string
     */
    public $sex;

    /**
     * Whether the currently logged-in athlete follows this athlete.
     * May take one of the following values: pending, accepted, blocked.
     *
     * @var string
     */
    public $friend;

    /**
     * Whether this athlete follows the currently logged-in athlete.
     * May take one of the following values: pending, accepted, blocked.
     *
     * @var string
     */
    public $follower;

    /**
     * @var bool
     */
    public $premium;

    /**
     * Whether the athlete has any Summit subscription.
     *
     * @var bool
     */
    public $summit;

    /**
     * @var int
     */
    public $badgeTypeId;

    /**
     * @var string
     */
    public $destinationUrl;

    /**
     * The time at which the athlete was created.
     *
     * @var \DateTime
     */
    public $createdAt;

    /**
     * The time at which the athlete was last updated.
     *
     * @var \DateTime
     */
    public $updatedAt;

    protected function setCreatedAt($date): void
    {
        $this->createdAt = new \DateTime($date);
    }

    protected function setUpdatedAt($date): void
    {
        $this->updatedAt = new \DateTime($date);
    }
}
