<?php

namespace Lsv\Strava\Model;

use Lsv\Strava\Build;
use Lsv\Strava\Model\Traits\LatitudeLongitudeTrait;
use Lsv\Strava\Model\Traits\StartDateTrait;
use Lsv\Strava\Model\Traits\WattsTrait;

class SummaryActivity extends MetaActivity
{
    use StartDateTrait;
    use LatitudeLongitudeTrait;
    use WattsTrait;

    /**
     * The identifier provided at upload time.
     *
     * @var string|null
     */
    public $externalId;

    /**
     * The identifier of the upload that resulted in this activity.
     *
     * @var string|null
     */
    public $uploadId;

    /**
     * Athlete data.
     *
     * @var MetaAthlete
     */
    public $athlete;

    /**
     * The name of the activity.
     *
     * @var string
     */
    public $name;

    /**
     * The activity's distance, in meters.
     *
     * @var float
     */
    public $distance;

    /**
     * The activity's average cadance.
     *
     * @var float
     */
    public $averageCadence;

    /**
     * @var bool
     */
    public $hasHeartrate;

    /**
     * @var float
     */
    public $averageHeartrate;

    /**
     * @var float
     */
    public $maxHeartrate;

    /**
     * The activity's moving time, in seconds.
     *
     * @var int
     */
    public $movingTime;

    /**
     * The activity's elapsed time, in seconds.
     *
     * @var int
     */
    public $elapsedTime;

    /**
     * The activity's total elevation gain.
     *
     * @var float
     */
    public $totalElevationGain;

    /**
     * The activity's highest elevation, in meters.
     *
     * @var float
     */
    public $elevHigh;

    /**
     * The activity's lowest elevation, in meters.
     *
     * @var float
     */
    public $elevLow;

    /**
     * Activity type.
     *
     * @var string
     */
    public $type;

    /**
     * The number of achievements gained during this activity.
     *
     * @var int
     */
    public $achievementCount;

    /**
     * The number of kudos given for this activity.
     *
     * @var int
     */
    public $kudosCount;

    /**
     * The number of comments for this activity.
     *
     * @var int
     */
    public $commentCount;

    /**
     * The number of athletes for taking part in a group activity.
     *
     * @var int
     */
    public $athleteCount;

    /**
     * The number of Instagram photos for this activity.
     *
     * @var int
     */
    public $photoCount;

    /**
     * The number of Instagram and Strava photos for this activity.
     *
     * @var int
     */
    public $totalPhotoCount;

    /**
     * @var int
     */
    public $prCount;

    /**
     * Polyline map.
     *
     * @var PolylineMap
     */
    public $map;

    /**
     * Whether this activity was recorded on a training machine.
     *
     * @var bool
     */
    public $trainer;

    /**
     * Whether this activity is a commute.
     *
     * @var bool
     */
    public $commute;

    /**
     * Whether this activity was created manually.
     *
     * @var bool
     */
    public $manual;

    /**
     * Whether this activity is private.
     *
     * @var bool
     */
    public $private;

    /**
     * Whether this activity is flagged.
     *
     * @var bool
     */
    public $flagged;

    /**
     * The activity's workout type.
     *
     * @var int
     */
    public $workoutType;

    /**
     * The activity's average speed, in meters per second.
     *
     * @var float
     */
    public $averageSpeed;

    /**
     * The activity's max speed, in meters per second.
     *
     * @var float
     */
    public $maxSpeed;

    /**
     * Whether the logged-in athlete has kudoed this activity.
     *
     * @var bool
     */
    public $hasKudoed;

    /**
     * The id of the gear for the activity.
     *
     * @var string
     */
    public $gearId;

    /**
     * The total work done in kilojoules during this activity. Rides only.
     *
     * @var float
     */
    public $kilojoules;

    /**
     * The description of the activity.
     *
     * @var string
     */
    public $description;

    /**
     * Photos.
     *
     * @var PhotosSummary
     */
    public $photos;

    /**
     * Gear.
     *
     * @var SummaryGear
     */
    public $gear;

    /**
     * The number of kilocalories consumed during this activity.
     *
     * @var float
     */
    public $calories;

    /**
     * Segmendted efforts.
     *
     * @var DetailedSegmentEffort[]
     */
    public $segmentEfforts;

    /**
     * The name of the device used to record the activity.
     *
     * @var string
     */
    public $deviceName;

    /**
     * The token used to embed a Strava activity.
     *
     * @var string
     */
    public $embedToken;

    /**
     * The splits of this activity in metric units (for runs).
     *
     * @var Split[]
     */
    public $splitsMetric;

    /**
     * The splits of this activity in imperial units (for runs).
     *
     * @var Split[]
     */
    public $splitsStandard;

    /**
     * Laps.
     *
     * @var Lap[]
     */
    public $laps;

    /**
     * Best effort.
     *
     * @var DetailedSegmentEffort[]
     */
    public $bestEfforts;

    /**
     * @var string
     */
    public $locationCity;

    /**
     * @var string
     */
    public $locationState;

    /**
     * @var string
     */
    public $locationCountry;

    /**
     * @var string
     */
    public $fromAcceptedTag;

    /**
     * @var int
     */
    public $sufferScore;

    protected function setMap(PolylineMap $map): void
    {
        $this->map = $map;
    }

    protected function setPhotos(PhotosSummary $summary): void
    {
        $this->photos = $summary;
    }

    protected function setGear(SummaryGear $gear): void
    {
        $this->gear = $gear;
    }

    protected function setSegmentEfforts(array $data): void
    {
        $this->segmentEfforts = Build::multiple($data, DetailedSegmentEffort::class);
    }

    protected function setSplitsMetric(array $data): void
    {
        $this->splitsMetric = Build::multiple($data, Split::class);
    }

    protected function setSplitsStandard(array $data): void
    {
        $this->splitsStandard = Build::multiple($data, Split::class);
    }

    protected function setLaps(array $data): void
    {
        $this->laps = Build::multiple($data, Lap::class);
    }

    protected function setBestEfforts(array $data): void
    {
        $this->bestEfforts = Build::multiple($data, DetailedSegmentEffort::class);
    }

    protected function setAthlete(MetaAthlete $athlete): void
    {
        $this->athlete = $athlete;
    }
}
