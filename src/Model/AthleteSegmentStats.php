<?php

namespace Lsv\Strava\Model;

class AthleteSegmentStats
{
    /**
     * @var int
     */
    public $prElapsedTime;

    /**
     * @var \DateTime
     */
    public $prDate;

    /**
     * @var int
     */
    public $effortCount;

    protected function setPrDate($time): void
    {
        $this->prDate = new \DateTime($time);
    }
}
