<?php

namespace Lsv\Strava\Model;

use Lsv\Strava\Model\Traits\ResourceStateTrait;
use Lsv\Strava\Request\Traits\IdTrait;

class SummaryGear
{
    use ResourceStateTrait;
    use IdTrait;

    /**
     * Whether this gear's is the owner's default one.
     *
     * @var bool
     */
    public $primary;

    /**
     * The gear's name.
     *
     * @var string
     */
    public $name;

    /**
     * The distance logged with this gear.
     *
     * @var float
     */
    public $distance;
}
