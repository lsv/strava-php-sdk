<?php

namespace Lsv\Strava\Model;

class DetailedGear extends SummaryGear
{
    /**
     * The gear's brand name.
     *
     * @var string
     */
    public $brandName;

    /**
     * The gear's model name.
     *
     * @var string
     */
    public $modelName;

    /**
     * The gear's frame type (bike only).
     *
     * @var int
     */
    public $frameType;

    /**
     * The gear's description.
     *
     * @var string
     */
    public $description;
}
