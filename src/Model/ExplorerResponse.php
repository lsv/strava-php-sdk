<?php

namespace Lsv\Strava\Model;

use Lsv\Strava\Build;

class ExplorerResponse
{
    /**
     * The set of segments matching an explorer request.
     *
     * @var ExplorerSegment[]
     */
    public $segments;

    protected function setSegments(array $data): void
    {
        $this->segments = Build::multiple($data, ExplorerSegment::class);
    }
}
