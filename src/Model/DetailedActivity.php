<?php

namespace Lsv\Strava\Model;

use Lsv\Strava\Build;

class DetailedActivity extends SummaryActivity
{
    /**
     * The activity's average tempo.
     *
     * @var float
     */
    public $averageTemp;

    /**
     * Whether the activity has opted out of leaderboard.
     *
     * @var bool
     */
    public $segmentLeaderboardOptOut;

    /**
     * Whether the activity has opted out of leaderboard.
     *
     * @var bool
     */
    public $leaderboardOptOut;

    /**
     * @var string
     */
    public $partnerBrandTag;

    /**
     * Highlighted kudors.
     *
     * @var SummaryAthlete[]
     */
    public $highlightedKudosers;

    protected function setHighlightedKudosers(array $data): void
    {
        $this->highlightedKudosers = Build::multiple($data, SummaryAthlete::class);
    }
}
