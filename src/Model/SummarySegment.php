<?php

namespace Lsv\Strava\Model;

use Lsv\Strava\Model\Traits\AddressTrait;
use Lsv\Strava\Model\Traits\LatitudeLongitudeTrait;
use Lsv\Strava\Model\Traits\ResourceStateTrait;
use Lsv\Strava\Request\Traits\IdTrait;

class SummarySegment
{
    use ResourceStateTrait;
    use LatitudeLongitudeTrait;
    use IdTrait;
    use AddressTrait;

    /**
     * The name of this segment.
     *
     * @var string
     */
    public $name;

    /**
     * May take one of the following values: Ride, Run.
     *
     * @var string
     */
    public $activityType;

    /**
     * The segment's distance, in meters.
     *
     * @var float
     */
    public $distance;

    /**
     * The segment's average grade, in percents.
     *
     * @var float
     */
    public $averageGrade;

    /**
     * The segments's maximum grade, in percents.
     *
     * @var float
     */
    public $maximumGrade;

    /**
     * The segments's highest elevation, in meters.
     *
     * @var float
     */
    public $elevationHigh;

    /**
     * The segments's lowest elevation, in meters.
     *
     * @var float
     */
    public $elevationLow;

    /**
     * The category of the climb.
     *
     * @var int
     */
    public $climbCategory;

    /**
     * Whether this segment is private.
     *
     * @var bool
     */
    public $private;

    /**
     * Summary Effort.
     *
     * @var SummarySegmentEffort
     */
    public $athletePrEffort;

    /**
     * @var bool
     */
    public $hazardous;

    /**
     * @var bool
     */
    public $starred;

    protected function setAthletePrEffort(SummarySegmentEffort $athlete): void
    {
        $this->athletePrEffort = $athlete;
    }
}
