<?php

namespace Lsv\Strava\Model;

class StreamSet
{
    /**
     * @var string
     */
    public $type;

    /**
     * @var array
     */
    public $data;

    /**
     * @var string
     */
    public $seriesType;

    /**
     * @var int
     */
    public $originalSize;

    /**
     * @var string
     */
    public $resolution;
}
