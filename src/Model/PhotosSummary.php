<?php

namespace Lsv\Strava\Model;

class PhotosSummary
{
    /**
     * The number of photos.
     *
     * @var int
     */
    public $count;

    /**
     * The primary photo.
     *
     * @var PhotosSummaryPrimary
     */
    public $primary;

    /**
     * @var bool
     */
    public $usePrimaryPhoto;

    protected function setPrimary(PhotosSummaryPrimary $primary): void
    {
        $this->primary = $primary;
    }
}
