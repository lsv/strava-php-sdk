<?php

namespace Lsv\Strava\Model;

class DetailedClub extends SummaryClub
{
    /**
     * The membership status of the logged-in athlete. May take one of the following values: member, pending.
     *
     * @var string
     */
    public $membership;

    /**
     * Whether the currently logged-in athlete is an administrator of this club.
     *
     * @var bool
     */
    public $admin;

    /**
     * Whether the currently logged-in athlete is the owner of this club.
     *
     * @var bool
     */
    public $owner;

    public $description;

    public $clubType;

    public $postCount;

    public $ownerId;

    /**
     * The number of athletes in the club that the logged-in athlete follows.
     *
     * @var int
     */
    public $followingCount;
}
