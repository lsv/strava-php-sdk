<?php

namespace Lsv\Strava\Model;

use Lsv\Strava\Model\Traits\ResourceStateTrait;
use Lsv\Strava\Request\Traits\IdTrait;

class MetaActivity
{
    use ResourceStateTrait;
    use IdTrait;
}
