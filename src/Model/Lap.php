<?php

namespace Lsv\Strava\Model;

use Lsv\Strava\Model\Traits\ResourceStateTrait;
use Lsv\Strava\Model\Traits\WattsTrait;
use Lsv\Strava\Request\Traits\IdTrait;

class Lap
{
    use ResourceStateTrait;
    use WattsTrait;
    use IdTrait;

    /**
     * Activity.
     *
     * @var MetaActivity
     */
    public $activity;

    /**
     * Athlete.
     *
     * @var MetaAthlete
     */
    public $athlete;

    /**
     * The lap's average cadence.
     *
     * @var float
     */
    public $averageCadence;

    /**
     * The lap's average speed.
     *
     * @var float
     */
    public $averageSpeed;

    /**
     * The lap's distance, in meters.
     *
     * @var float
     */
    public $distance;

    /**
     * The lap's elapsed time, in seconds.
     *
     * @var int
     */
    public $elapsedTime;

    /**
     * The start index of this effort in its activity's stream.
     *
     * @var int
     */
    public $startIndex;

    /**
     * The end index of this effort in its activity's stream.
     *
     * @var int
     */
    public $endIndex;

    /**
     * The index of this lap in the activity it belongs to.
     *
     * @var int
     */
    public $lapIndex;

    /**
     * The maximum speed of this lat, in meters per second.
     *
     * @var float
     */
    public $maxSpeed;

    /**
     * The lap's moving time, in seconds.
     *
     * @var int
     */
    public $movingTime;

    /**
     * The name of the lap.
     *
     * @var string
     */
    public $name;

    /**
     * The athlete's pace zone during this lap.
     *
     * @var int
     */
    public $paceZone;

    /**
     * Split time.
     *
     * @var Split
     */
    public $split;

    /**
     * The time at which the lap was started.
     *
     * @var \DateTime
     */
    public $startDate;

    /**
     * The time at which the lap was started in the local timezone.
     *
     * @var \DateTime
     */
    public $startDateLocal;

    /**
     * The elevation gain of this lap, in meters.
     *
     * @var float
     */
    public $totalElevationGain;

    protected function setActivity(MetaActivity $activity): void
    {
        $this->activity = $activity;
    }

    protected function setAthlete(MetaAthlete $athlete): void
    {
        $this->athlete = $athlete;
    }
}
