<?php

namespace Lsv\Strava\Model;

class TimedZoneRange
{
    /**
     * @var int
     */
    public $min;

    /**
     * @var int
     */
    public $max;

    /**
     * @var int
     */
    public $time;
}
