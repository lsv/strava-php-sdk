<?php

namespace Lsv\Strava\Model;

use Lsv\Strava\Model\Traits\ResourceStateTrait;
use Lsv\Strava\Request\Traits\IdTrait;

class Comment
{
    use ResourceStateTrait;
    use IdTrait;

    /**
     * The identifier of the activity this comment is related to.
     *
     * @var int
     */
    public $activityId;

    /**
     * @var int
     */
    public $postId;

    /**
     * The content of the comment.
     *
     * @var string
     */
    public $text;

    /**
     * @var string
     */
    public $mentionsMetadata;

    /**
     * The time at which this comment was created.
     *
     * @var \DateTime
     */
    public $createdAt;

    /**
     * @var SummaryAthlete
     */
    public $athlete;

    protected function setAthlete(SummaryAthlete $athlete): void
    {
        $this->athlete = $athlete;
    }

    protected function setCreatedAt($time): void
    {
        $this->createdAt = new \DateTime($time);
    }
}
