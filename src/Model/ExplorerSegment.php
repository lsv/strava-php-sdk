<?php

namespace Lsv\Strava\Model;

use Lsv\Strava\Model\Traits\ResourceStateTrait;
use Lsv\Strava\Request\Traits\IdTrait;

class ExplorerSegment
{
    use IdTrait;
    use ResourceStateTrait;

    /**
     * The name of this segment.
     *
     * @var string
     */
    public $name;

    /**
     * The category of the climb.
     *
     * @var int
     */
    public $climbCategory;

    /**
     * The description for the category of the climb May take one of the following values:
     * NC, 4, 3, 2, 1, HC.
     *
     * @var string
     */
    public $climbCategoryDesc;

    /**
     * The segment's average grade, in percents.
     *
     * @var float
     */
    public $avgGrade;

    /**
     * @var LatLng
     */
    public $startLatlng;

    /**
     * @var LatLng
     */
    public $endLatlng;

    /**
     * The segments's evelation difference, in meters.
     *
     * @var float
     */
    public $elevDifference;

    /**
     * The segment's distance, in meters.
     *
     * @var float
     */
    public $distance;

    /**
     * The polyline of the segment.
     *
     * @var string
     */
    public $points;

    /**
     * @var bool
     */
    public $starred;

    protected function setStartLatlng(LatLng $latLng): void
    {
        $this->startLatlng = $latLng;
    }

    protected function setEndLatlng(LatLng $latLng): void
    {
        $this->endLatlng = $latLng;
    }
}
