<?php

namespace Lsv\Strava\Model;

use Lsv\Strava\Model\Traits\ResourceStateTrait;
use Lsv\Strava\Request\Traits\IdTrait;

class PolylineMap
{
    use ResourceStateTrait;
    use IdTrait;

    /**
     * The polyline of the map.
     *
     * @var string
     */
    public $polyline;

    /**
     * The summary polyline of the map.
     *
     * @var string
     */
    public $summaryPolyline;
}
