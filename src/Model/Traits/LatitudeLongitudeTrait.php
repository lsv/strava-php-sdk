<?php

namespace Lsv\Strava\Model\Traits;

use Lsv\Strava\Model\LatLng;

trait LatitudeLongitudeTrait
{
    /**
     * Starting latitude and longitude.
     *
     * @var LatLng
     */
    public $startLatlng;

    /**
     * @var float
     */
    public $startLatitude;

    /**
     * @var float
     */
    public $startLongitude;

    /**
     * Ending latitude and longitude.
     *
     * @var LatLng
     */
    public $endLatlng;

    /**
     * @var float
     */
    public $endLatitude;

    /**
     * @var float
     */
    public $endLongitude;

    protected function setStartLatlng(LatLng $latLng): void
    {
        $this->startLatlng = $latLng;
    }

    protected function setEndLatlng(LatLng $latLng): void
    {
        $this->endLatlng = $latLng;
    }
}
