<?php

namespace Lsv\Strava\Model\Traits;

trait AddressTrait
{
    /**
     * The city.
     *
     * @var string
     */
    public $city;

    /**
     * The state or geographical region.
     *
     * @var string
     */
    public $state;

    /**
     * The country.
     *
     * @var string
     */
    public $country;
}
