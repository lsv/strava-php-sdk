<?php

namespace Lsv\Strava\Model\Traits;

trait StartDateTrait
{
    /**
     * The time at which the activity was started.
     *
     * @var \DateTime
     */
    public $startDate;

    /**
     * The time at which the activity was started in the local timezone.
     *
     * @var \DateTime
     */
    public $startDateLocal;

    /**
     * The timezone of the activity.
     *
     * @var string
     */
    public $timezone;

    /**
     * @var int
     */
    public $utcOffset;

    protected function setStartDate(string $date): void
    {
        $this->startDate = new \DateTime($date);
    }

    protected function setStartDateLocal(string $date): void
    {
        $this->startDateLocal = new \DateTime($date);
    }

    protected function setTimezone($timezone): void
    {
        if (null !== $this->startDateLocal) {
            $this->startDateLocal = $this->startDateLocal->setTimezone(new \DateTimeZone($timezone));
        }

        $this->timezone = $timezone;
    }
}
