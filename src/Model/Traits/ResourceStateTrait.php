<?php

namespace Lsv\Strava\Model\Traits;

trait ResourceStateTrait
{
    /**
     * Resource state, indicates level of detail. Possible values: 1 -> "meta", 2 -> "summary", 3 -> "detail".
     *
     * @var int
     */
    public $resourceState;
}
