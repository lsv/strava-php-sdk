<?php

namespace Lsv\Strava\Model\Traits;

trait WattsTrait
{
    /**
     * Average power output in watts during this activity. Rides only.
     *
     * @var float
     */
    public $averageWatts;

    /**
     * Whether the watts are from a power meter, false if estimated.
     *
     * @var float|bool
     */
    public $deviceWatts;

    /**
     * Rides with power meter data only.
     *
     * @var int|null
     */
    public $maxWatts;

    /**
     * Similar to Normalized Power. Rides with power meter data only.
     *
     * @var int|null
     */
    public $weightedAverageWatts;
}
