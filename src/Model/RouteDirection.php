<?php

namespace Lsv\Strava\Model;

class RouteDirection
{
    /**
     * The distance in the route at which the action applies.
     *
     * @var float
     */
    public $distance;

    /**
     * The action of this direction.
     *
     * @var int
     */
    public $action;

    /**
     * @var string
     */
    public $name;
}
