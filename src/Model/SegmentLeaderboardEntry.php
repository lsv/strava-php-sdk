<?php

namespace Lsv\Strava\Model;

use Lsv\Strava\Model\Traits\StartDateTrait;

class SegmentLeaderboardEntry
{
    use StartDateTrait;

    /**
     * The public name of the athlete.
     *
     * @var string
     */
    public $athleteName;

    /**
     * The elapsed of the segment effort associated with this entry.
     *
     * @var int
     */
    public $elapsedTime;

    /**
     * The moving of the segment effort associated with this entry.
     *
     * @var int
     */
    public $movingTime;

    /**
     * The rank of this entry in the leaderboard.
     *
     * @var int
     */
    public $rank;
}
