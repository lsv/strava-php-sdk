<?php

namespace Lsv\Strava\Model;

use Lsv\Strava\Build;

class ActivityZone
{
    /**
     * @var int
     */
    public $score;

    /**
     * @var TimedZoneRange[]
     */
    public $distributionBuckets;

    /**
     * @var string
     */
    public $type;

    /**
     * @var bool
     */
    public $sensorBased;

    /**
     * @var int
     */
    public $points;

    /**
     * @var bool
     */
    public $customZones;

    /**
     * @var int
     */
    public $max;

    protected function setDistributionBuckets($data): void
    {
        if (is_array($data)) {
            $this->distributionBuckets = Build::multiple($data, TimedZoneRange::class);
        }
    }
}
