<?php

namespace Lsv\Strava\Model;

use Lsv\Strava\Model\Traits\ResourceStateTrait;
use Lsv\Strava\Model\Traits\StartDateTrait;
use Lsv\Strava\Model\Traits\WattsTrait;
use Lsv\Strava\Request\Traits\IdTrait;

class SummarySegmentEffort
{
    use StartDateTrait;
    use IdTrait;
    use ResourceStateTrait;
    use WattsTrait;

    /**
     * The name of the segment on which this effort was performed.
     *
     * @var string
     */
    public $name;

    /**
     * Activity.
     *
     * @var MetaActivity
     */
    public $activity;

    /**
     * Athlete.
     *
     * @var MetaAthlete
     */
    public $athlete;

    /**
     * The effort's elapsed time.
     *
     * @var int
     */
    public $elapsedTime;

    /**
     * The effort's distance in meters.
     *
     * @var float
     */
    public $distance;

    /**
     * Whether this effort is the current best on the leaderboard.
     *
     * @var bool
     */
    public $isKom;

    /**
     * @var array
     */
    public $achievements;

    /**
     * The effort's moving time.
     *
     * @var int
     */
    public $movingTime;

    /**
     * The start index of this effort in its activity's stream.
     *
     * @var int
     */
    public $startIndex;

    /**
     * The end index of this effort in its activity's stream.
     *
     * @var int
     */
    public $endIndex;

    /**
     * The segment.
     *
     * @var SummarySegment
     */
    public $segment;

    /**
     * The rank of the effort on the global leaderboard if it belongs in the top 10 at the time of upload.
     *
     * @var int
     */
    public $komRank;

    /**
     * The rank of the effort on the athlete's leaderboard if it belongs in the top 3 at the time of upload.
     *
     * @var int
     */
    public $prRank;

    protected function setActivity(MetaActivity $activity): void
    {
        $this->activity = $activity;
    }

    protected function setAthlete(MetaAthlete $athlete): void
    {
        $this->athlete = $athlete;
    }

    protected function setSegment(SummarySegment $segment): void
    {
        $this->segment = $segment;
    }
}
