<?php

namespace Lsv\Strava\Model;

class ActivityStats
{
    /**
     * The recent (last 4 weeks) run stats for the athlete.
     *
     * @var ActivityTotal
     */
    public $recentRunTotals;

    /**
     * The all time run stats for the athlete.
     *
     * @var ActivityTotal
     */
    public $allRunTotals;

    /**
     * The recent (last 4 weeks) swim stats for the athlete.
     *
     * @var ActivityTotal
     */
    public $recentSwimTotals;

    /**
     * The longest distance ridden by the athlete.
     *
     * @var float
     */
    public $biggestRideDistance;

    /**
     * The year to date swim stats for the athlete.
     *
     * @var ActivityTotal
     */
    public $ytdSwimTotals;

    /**
     * The all time swim stats for the athlete.
     *
     * @var ActivityTotal
     */
    public $allSwimTotals;

    /**
     * The recent (last 4 weeks) ride stats for the athlete.
     *
     * @var ActivityTotal
     */
    public $recentRideTotals;

    /**
     * The highest climb ridden by the athlete.
     *
     * @var float
     */
    public $biggestClimbElevationGain;

    /**
     * The year to date ride stats for the athlete.
     *
     * @var ActivityTotal
     */
    public $ytdRideTotals;

    /**
     * The all time ride stats for the athlete.
     *
     * @var ActivityTotal
     */
    public $allRideTotals;

    /**
     * The year to date run stats for the athlete.
     *
     * @var ActivityTotal
     */
    public $ytdRunTotals;

    protected function setRecentRideTotals(ActivityTotal $total): void
    {
        $this->recentRideTotals = $total;
    }

    protected function setRecentRunTotals(ActivityTotal $total): void
    {
        $this->recentRunTotals = $total;
    }

    protected function setRecentSwimTotals(ActivityTotal $total): void
    {
        $this->recentSwimTotals = $total;
    }

    protected function setYtdRideTotals(ActivityTotal $total): void
    {
        $this->ytdRideTotals = $total;
    }

    protected function setYtdRunTotals(ActivityTotal $total): void
    {
        $this->ytdRunTotals = $total;
    }

    protected function setAllRunTotals(ActivityTotal $allRunTotals): void
    {
        $this->allRunTotals = $allRunTotals;
    }

    protected function setYtdSwimTotals(ActivityTotal $ytdSwimTotals): void
    {
        $this->ytdSwimTotals = $ytdSwimTotals;
    }

    protected function setAllSwimTotals(ActivityTotal $allSwimTotals): void
    {
        $this->allSwimTotals = $allSwimTotals;
    }

    protected function setAllRideTotals(ActivityTotal $allRideTotals): void
    {
        $this->allRideTotals = $allRideTotals;
    }
}
