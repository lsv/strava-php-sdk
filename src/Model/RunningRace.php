<?php

namespace Lsv\Strava\Model;

use Lsv\Strava\Model\Traits\AddressTrait;
use Lsv\Strava\Model\Traits\StartDateTrait;
use Lsv\Strava\Request\Traits\IdTrait;

class RunningRace
{
    use IdTrait;
    use StartDateTrait;
    use AddressTrait;

    /**
     * The name of this race.
     *
     * @var string
     */
    public $name;

    /**
     * The type of this race.
     *
     * @var int
     */
    public $runningRaceType;

    /**
     * The race's distance, in meters.
     *
     * @var float
     */
    public $distance;

    /**
     * The set of routes that cover this race's course.
     *
     * @var array<int>
     */
    public $routeIds;

    /**
     * The unit system in which the race should be displayed.
     * May take one of the following values: feet, meters.
     *
     * @var string
     */
    public $measurementPreference;

    /**
     * The vanity URL of this race on Strava.
     *
     * @var string
     */
    public $url;

    /**
     * The URL of this race's website.
     *
     * @var string
     */
    public $websiteUrl;
}
