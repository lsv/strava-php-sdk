<?php

namespace Lsv\Strava\Model;

class Split
{
    /**
     * The average speed of this split, in meters per second.
     *
     * @var float
     */
    public $averageSpeed;

    /**
     * The distance of this split, in meters.
     *
     * @var float
     */
    public $distance;

    /**
     * The elapsed time of this split, in seconds.
     *
     * @var int
     */
    public $elapsedTime;

    /**
     * The elevation difference of this split, in meters.
     *
     * @var float
     */
    public $elevationDifference;

    /**
     * The pacing zone of this split.
     *
     * @var int
     */
    public $paceZone;

    /**
     * The moving time of this split, in seconds.
     *
     * @var int
     */
    public $movingTime;

    /**
     * The split.
     *
     * @var int
     */
    public $split;
}
