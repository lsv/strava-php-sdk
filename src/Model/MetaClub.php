<?php

namespace Lsv\Strava\Model;

use Lsv\Strava\Model\Traits\ResourceStateTrait;
use Lsv\Strava\Request\Traits\IdTrait;

class MetaClub
{
    use IdTrait;
    use ResourceStateTrait;

    /**
     * The club's name.
     *
     * @var string
     */
    public $name;
}
