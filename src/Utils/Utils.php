<?php

namespace Lsv\Strava\Utils;

class Utils
{
    public static function camelCase($key): string
    {
        return lcfirst(
            str_replace(
                '_',
                '',
                ucwords(trim($key, '_'), '_')
            )
        );
    }
}
