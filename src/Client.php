<?php

namespace Lsv\Strava;

use Http\Adapter\Guzzle6\Client as GuzzleClient;
use Http\Client\HttpClient;
use Http\Message\Authentication\Bearer;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class Client
{
    /**
     * @var Bearer
     */
    private $bearer;

    /**
     * @var HttpClient
     */
    private $client;

    public function __construct(Bearer $bearer, ?HttpClient $client = null)
    {
        $this->bearer = $bearer;
        $this->client = $client ?: new GuzzleClient();
    }

    public function authenticate(RequestInterface $request): RequestInterface
    {
        return $this->bearer->authenticate($request);
    }

    public function send(RequestInterface $request): ResponseInterface
    {
        return $this->client->sendRequest($request);
    }
}
