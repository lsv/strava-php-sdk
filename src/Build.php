<?php

namespace Lsv\Strava;

use Lsv\Strava\Utils\Utils;

class Build
{
    /**
     * @throws \ReflectionException
     */
    public static function multiple(array $data, string $className): array
    {
        $output = [];
        foreach ($data as $datum) {
            if ($datum) {
                $output[] = self::single($datum, $className);
            }
        }

        return $output;
    }

    /**
     * @param array $data
     *
     * @return mixed
     *
     * @throws \ReflectionException
     */
    public static function single(?array $data, string $className)
    {
        $object = new $className();
        if ($data) {
            $reflection = new \ReflectionClass($object);

            foreach ($data as $key => $datum) {
                if (
                    !self::invokeSetter($object, $key, $reflection, $datum)
                    && !self::invokeParameter($object, $key, $reflection, $datum)
                ) {
                    throw new \InvalidArgumentException(sprintf('Could not parse "%s" with parameter "%s" with value "%s"', $className, $key, json_encode($datum)));
                }
            }
        }

        return $object;
    }

    private static function invokeSetter($object, $key, \ReflectionClass $reflection, $data): bool
    {
        $methods = $reflection->getMethods();
        $camelCase = Utils::camelCase($key);
        $setter = 'set'.ucfirst($camelCase);
        if (null === $data) {
            return false;
        }

        if ('' === $data) {
            return false;
        }

        foreach ($methods as $method) {
            if ($method->getName() === $setter) {
                foreach ($method->getParameters() as $parameter) {
                    if (
                        null !== $parameter->getType()
                        && class_exists($parameter->getType()->getName())
                    ) {
                        $parameterClass = $parameter->getType()->getName();
                        $data = self::single($data, $parameterClass);
                        break;
                    }
                }

                $method->setAccessible(true);
                $method->invoke($object, $data);

                return true;
            }
        }

        return false;
    }

    private static function invokeParameter($object, $key, \ReflectionClass $reflection, $data): bool
    {
        $properties = $reflection->getProperties();
        $camelCase = Utils::camelCase($key);

        foreach ($properties as $property) {
            if ($property->getName() === $camelCase) {
                $object->{$property->getName()} = $data;

                return true;
            }
        }

        return false;
    }
}
