<?php

namespace Lsv\StravaTest\Request\Athletes;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Lsv\Strava\Client;
use Lsv\Strava\Request\Athletes\GetAuthenticatedAthlete;
use Lsv\StravaTest\Request\BaseRequestTest;

class GetAuthenticatedAthleteTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__.'/stub/get_authenticated_athlete.json')),
        ]);
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_request(): void
    {
        $request = (new GetAuthenticatedAthlete($this->client))->getRequest();

        $this->assertSame('/api/v3/athlete', $request->getUri()->getPath());
        $this->assertSame('GET', $request->getMethod());
    }

    /**
     * @test
     */
    public function can_parse_result(): void
    {
        $activity = new GetAuthenticatedAthlete($this->client);
        $athlete = $activity->execute();

        $this->assertSame(5, $athlete->followerCount);
        $this->assertSame(5, $athlete->friendCount);
        $this->assertSame(0, $athlete->mutualFriendCount);
        $this->assertSame(1, $athlete->athleteType);
        $this->assertSame('%m/%d/%Y', $athlete->datePreference);
        $this->assertSame('feet', $athlete->measurementPreference);
        $this->assertCount(0, $athlete->clubs);
        $this->assertNull($athlete->ftp);
        $this->assertSame(0, $athlete->weight);

        $this->assertCount(1, $athlete->bikes);
        $this->assertSame('b12345678987655', $athlete->bikes[0]->id);
        $this->assertTrue($athlete->bikes[0]->primary);
        $this->assertSame('EMC', $athlete->bikes[0]->name);
        $this->assertSame(2, $athlete->bikes[0]->resourceState);
        $this->assertSame(0, $athlete->bikes[0]->distance);

        $this->assertCount(1, $athlete->shoes);
        $this->assertSame('g12345678987655', $athlete->shoes[0]->id);
        $this->assertTrue($athlete->shoes[0]->primary);
        $this->assertSame('adidas', $athlete->shoes[0]->name);
        $this->assertSame(2, $athlete->shoes[0]->resourceState);
        $this->assertSame(4904, $athlete->shoes[0]->distance);
    }
}
