<?php

namespace Lsv\StravaTest\Request\Athletes;

use DateTime;
use DateTimeZone;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Lsv\Strava\Client;
use Lsv\Strava\Request\Athletes\GetActivities;
use Lsv\StravaTest\Request\BaseRequestTest;

class GetActivitiesTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/get_activities.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_request(): void
    {
        $request = (new GetActivities($this->client))
            ->getRequest();

        $this->assertSame('/api/v3/athletes/activities', $request->getUri()->getPath());
        $this->assertSame('GET', $request->getMethod());
    }

    /**
     * @test
     */
    public function can_make_request_with_before_and_after(): void
    {
        $before = new DateTime('2018-10-24 10:10:00', new DateTimeZone('Europe/Amsterdam'));
        $after = new DateTime('2017-05-22 22:30:00', new DateTimeZone('Europe/Amsterdam'));

        $request = (new GetActivities($this->client))
            ->setBefore($before)
            ->setAfter($after)
            ->getRequest();

        $this->assertSame('/api/v3/athletes/activities', $request->getUri()->getPath());
        $this->assertSame('before=1540368600&after=1495485000', $request->getUri()->getQuery());
    }

    /**
     * @test
     */
    public function can_make_request_with_pagination(): void
    {
        $request = (new GetActivities($this->client))
            ->setPerPage(50)
            ->setPageNumber(3)
            ->getRequest();

        $this->assertSame('per_page=50&page=3', $request->getUri()->getQuery());
    }

    /**
     * @test
     */
    public function can_parse_response(): void
    {
        $response = (new GetActivities($this->client))
            ->execute();

        $this->assertCount(2, $response);

        $this->assertSame('154504250376823', $response[0]->id);
        $this->assertSame('1234567809', $response[1]->id);
    }
}
