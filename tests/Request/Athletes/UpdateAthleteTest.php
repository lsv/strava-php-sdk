<?php

namespace Lsv\StravaTest\Request\Athletes;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Lsv\Strava\Client;
use Lsv\Strava\Request\Athletes\UpdateAthlete;
use Lsv\StravaTest\Request\BaseRequestTest;

class UpdateAthleteTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/update_athlete.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_request(): void
    {
        $request = (new UpdateAthlete($this->client, 114.3))->getRequest();

        $this->assertSame('/api/v3/athlete', $request->getUri()->getPath());

        // Some differences on php versions, where some gives 114,3 some gives 114.3
        // Dont know why
        $this->assertTrue(
            'weight=114.3' === $request->getUri()->getQuery()
            || 'weight=114,3' === $request->getUri()->getQuery()
        );

        $this->assertSame('PUT', $request->getMethod());
    }

    /**
     * @test
     */
    public function can_parse_result(): void
    {
        $athlete = (new UpdateAthlete($this->client, 114.3))->execute();

        $this->assertSame('12345678987655098765444', $athlete->id);
    }
}
