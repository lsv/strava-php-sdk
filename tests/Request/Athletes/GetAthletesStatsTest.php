<?php

namespace Lsv\StravaTest\Request\Athletes;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedAthlete;
use Lsv\Strava\Model\MetaAthlete;
use Lsv\Strava\Model\SummaryAthlete;
use Lsv\Strava\Request\Athletes\GetAthletesStats;
use Lsv\StravaTest\Request\BaseRequestTest;
use stdClass;

class GetAthletesStatsTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/get_athlete_stats.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_request(): void
    {
        $request = (new GetAthletesStats($this->client, 333))->getRequest();

        $this->assertSame('/api/v3/athletes/333/stats', $request->getUri()->getPath());
        $this->assertSame('GET', $request->getMethod());
    }

    /**
     * @test
     */
    public function can_make_request_from_meta_athlete(): void
    {
        $meta = new MetaAthlete();
        $meta->id = 1334;

        $request = (new GetAthletesStats($this->client, $meta))->getRequest();

        $this->assertSame('/api/v3/athletes/1334/stats', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_from_summary_athlete(): void
    {
        $summary = new SummaryAthlete();
        $summary->id = 334;

        $request = (new GetAthletesStats($this->client, $summary))->getRequest();

        $this->assertSame('/api/v3/athletes/334/stats', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_from_detailed_athlete(): void
    {
        $detailed = new DetailedAthlete();
        $detailed->id = 1432;

        $request = (new GetAthletesStats($this->client, $detailed))->getRequest();

        $this->assertSame('/api/v3/athletes/1432/stats', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_with_pagination(): void
    {
        $request = (new GetAthletesStats($this->client, 333))
            ->setPageNumber(3)
            ->setPerPage(50)
            ->getRequest();

        $this->assertSame('/api/v3/athletes/333/stats', $request->getUri()->getPath());
        $this->assertSame('page=3&per_page=50', $request->getUri()->getQuery());
    }

    /**
     * @test
     */
    public function can_parse_response(): void
    {
        $response = (new GetAthletesStats($this->client, 333))->execute();

        $this->assertSame(0.8008281904610115, $response->biggestRideDistance);

        $this->assertSame(5.962134, $response->recentRunTotals->distance);
        $this->assertSame(5.962134, $response->allRunTotals->distance);
        $this->assertSame(5.962134, $response->recentSwimTotals->distance);
        $this->assertSame(5.962134, $response->ytdSwimTotals->distance);
        $this->assertSame(5.962134, $response->allSwimTotals->distance);
        $this->assertSame(5.962134, $response->allSwimTotals->distance);
        $this->assertSame(5.962134, $response->recentRideTotals->distance);
        $this->assertSame(5.962134, $response->ytdRideTotals->distance);
        $this->assertSame(5.962134, $response->allRideTotals->distance);
        $this->assertSame(5.962134, $response->ytdRunTotals->distance);

        $this->assertSame(6.027456183070403, $response->biggestClimbElevationGain);
    }

    /**
     * @test
     */
    public function can_not_use_wrong_object_as_activity_url(): void
    {
        $this->expectException(InvalidArgumentException::class);
        (new GetAthletesStats($this->client, new stdClass()))->execute();
    }
}
