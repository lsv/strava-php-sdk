<?php

namespace Lsv\StravaTest\Request\Athletes;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Lsv\Strava\Client;
use Lsv\Strava\Request\Athletes\GetZones;
use Lsv\StravaTest\Request\BaseRequestTest;

class GetZonesTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__.'/stub/get_zones.json')),
        ]);
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_request(): void
    {
        $request = (new GetZones($this->client))->getRequest();

        $this->assertSame('/api/v3/athlete/zones', $request->getUri()->getPath());
        $this->assertSame('GET', $request->getMethod());
    }

    /**
     * @test
     */
    public function can_parse_result(): void
    {
        $zones = (new GetZones($this->client))->execute();
        $this->assertCount(1, $zones);
        $zone = $zones[0];

        $this->assertSame('power', $zone->type);
        $this->assertSame(3, $zone->resourceState);
        $this->assertTrue($zone->sensorBased);
        $this->assertCount(11, $zone->distributionBuckets);
        $bucket = $zone->distributionBuckets[0];
        $this->assertSame(0, $bucket->max);
        $this->assertSame(0, $bucket->min);
        $this->assertSame(1498, $bucket->time);
    }
}
