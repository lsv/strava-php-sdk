<?php

namespace Lsv\StravaTest\Request\Clubs;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Lsv\Strava\Client;
use Lsv\Strava\Request\Clubs\ListAthleteClubs;
use Lsv\StravaTest\Request\BaseRequestTest;

class ListAthleteClubsTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/list_athlete_clubs.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_request(): void
    {
        $request = (new ListAthleteClubs($this->client))
            ->getRequest();

        $this->assertSame('/api/v3/athlete/clubs', $request->getUri()->getPath());
        $this->assertSame('GET', $request->getMethod());
    }

    /**
     * @test
     */
    public function can_make_request_with_pagination(): void
    {
        $request = (new ListAthleteClubs($this->client))
            ->setPageNumber(3)
            ->setPerPage(50)
            ->getRequest();

        $this->assertSame('/api/v3/athlete/clubs', $request->getUri()->getPath());
        $this->assertSame('page=3&per_page=50', $request->getUri()->getQuery());
    }

    /**
     * @test
     */
    public function can_parse_response(): void
    {
        $response = (new ListAthleteClubs($this->client))
            ->execute();

        $this->assertCount(1, $response);

        $this->assertSame('231407', $response[0]->id);
    }
}
