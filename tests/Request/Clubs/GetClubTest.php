<?php

namespace Lsv\StravaTest\Request\Clubs;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedClub;
use Lsv\Strava\Model\MetaClub;
use Lsv\Strava\Model\SummaryClub;
use Lsv\Strava\Request\Clubs\GetClub;
use Lsv\StravaTest\Request\BaseRequestTest;
use stdClass;

class GetClubTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/get_club.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_request(): void
    {
        $request = (new GetClub($this->client, '333345456454562343455436452434'))->getRequest();

        $this->assertSame('/api/v3/clubs/333345456454562343455436452434', $request->getUri()->getPath());
        $this->assertSame('GET', $request->getMethod());
    }

    /**
     * @test
     */
    public function can_make_request_from_meta_club(): void
    {
        $meta = new MetaClub();
        $meta->id = 1334;

        $request = (new GetClub($this->client, $meta))->getRequest();

        $this->assertSame('/api/v3/clubs/1334', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_from_summary_club(): void
    {
        $summary = new SummaryClub();
        $summary->id = 334;

        $request = (new GetClub($this->client, $summary))->getRequest();

        $this->assertSame('/api/v3/clubs/334', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_from_detailed_club(): void
    {
        $detailed = new DetailedClub();
        $detailed->id = 1432;

        $request = (new GetClub($this->client, $detailed))->getRequest();

        $this->assertSame('/api/v3/clubs/1432', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_parse_response(): void
    {
        $response = (new GetClub($this->client, 1))->execute();

        $this->assertSame('1', $response->id);
        $this->assertSame(3, $response->resourceState);
        $this->assertSame('Team Strava Cycling', $response->name);
        $this->assertSame(
            'https://dgalywyr863hv.cloudfront.net/pictures/clubs/1/1582/4/medium.jpg',
            $response->profileMedium
        );
        $this->assertSame('https://dgalywyr863hv.cloudfront.net/pictures/clubs/1/1582/4/large.jpg', $response->profile);
        $this->assertSame(
            'https://dgalywyr863hv.cloudfront.net/pictures/clubs/1/4328276/1/large.jpg',
            $response->coverPhoto
        );
        $this->assertSame(
            'https://dgalywyr863hv.cloudfront.net/pictures/clubs/1/4328276/1/small.jpg',
            $response->coverPhotoSmall
        );
        $this->assertSame('cycling', $response->sportType);
        $this->assertSame('San Francisco', $response->city);
        $this->assertSame('California', $response->state);
        $this->assertSame('United States', $response->country);
        $this->assertTrue($response->private);
        $this->assertSame(116, $response->memberCount);
        $this->assertFalse($response->featured);
        $this->assertFalse($response->verified);
        $this->assertSame('team-strava-bike', $response->url);
        $this->assertSame('member', $response->membership);
        $this->assertFalse($response->admin);
        $this->assertFalse($response->owner);
        $this->assertSame('Private club for Cyclists who work at Strava.', $response->description);
        $this->assertSame('company', $response->clubType);
        $this->assertSame(29, $response->postCount);
        $this->assertSame(759, $response->ownerId);
        $this->assertSame(107, $response->followingCount);
    }

    /**
     * @test
     */
    public function can_not_use_wrong_object_as_activity_url(): void
    {
        $this->expectException(InvalidArgumentException::class);
        (new GetClub($this->client, new stdClass()))->execute();
    }
}
