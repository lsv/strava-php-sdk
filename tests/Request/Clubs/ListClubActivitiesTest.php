<?php

namespace Lsv\StravaTest\Request\Clubs;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedClub;
use Lsv\Strava\Model\MetaClub;
use Lsv\Strava\Model\SummaryClub;
use Lsv\Strava\Request\Clubs\ListClubActivities;
use Lsv\StravaTest\Request\BaseRequestTest;
use stdClass;

class ListClubActivitiesTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/list_club_activities.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_request(): void
    {
        $request = (new ListClubActivities($this->client, '666532'))->getRequest();

        $this->assertSame('/api/v3/clubs/666532/activities', $request->getUri()->getPath());
        $this->assertSame('GET', $request->getMethod());
    }

    /**
     * @test
     */
    public function can_make_request_from_meta_club(): void
    {
        $meta = new MetaClub();
        $meta->id = 1334;

        $request = (new ListClubActivities($this->client, $meta))->getRequest();

        $this->assertSame('/api/v3/clubs/1334/activities', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_from_summary_club(): void
    {
        $summary = new SummaryClub();
        $summary->id = 334;

        $request = (new ListClubActivities($this->client, $summary))->getRequest();

        $this->assertSame('/api/v3/clubs/334/activities', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_from_detailed_club(): void
    {
        $detailed = new DetailedClub();
        $detailed->id = 1432;

        $request = (new ListClubActivities($this->client, $detailed))->getRequest();

        $this->assertSame('/api/v3/clubs/1432/activities', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_with_pagination(): void
    {
        $request = (new ListClubActivities($this->client, 333))
            ->setPageNumber(3)
            ->setPerPage(50)
            ->getRequest();

        $this->assertSame('/api/v3/clubs/333/activities', $request->getUri()->getPath());
        $this->assertSame('page=3&per_page=50', $request->getUri()->getQuery());
    }

    /**
     * @test
     */
    public function can_parse_response(): void
    {
        $response = (new ListClubActivities($this->client, 1))->execute();

        $this->assertCount(1, $response);

        $this->assertSame('World Championship', $response[0]->name);
        $this->assertSame(1, $response[0]->athlete->resourceState);
    }

    /**
     * @test
     */
    public function can_not_use_wrong_object_as_activity_url(): void
    {
        $this->expectException(InvalidArgumentException::class);
        (new ListClubActivities($this->client, new stdClass()))->execute();
    }
}
