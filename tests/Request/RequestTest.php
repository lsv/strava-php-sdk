<?php

namespace Lsv\StravaTest\Request;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Http\Client\Exception\HttpException;
use Lsv\Strava\Request\Athletes\GetActivities;
use Lsv\Strava\Request\Athletes\UpdateAthlete;

class RequestTest extends BaseRequestTest
{
    /**
     * @test
     */
    public function will_get_exception_when_access_denied(): void
    {
        $this->expectException(HttpException::class);
        $this->expectExceptionMessage('401 Unauthorized');

        $mockHandler = new MockHandler(
            [
                new Response(401),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $client = $this->getClient($stack);
        (new GetActivities($client))->execute();
    }

    /**
     * @test
     */
    public function will_throw_exception_when_15_minute_request_limit_reached(): void
    {
        $this->expectException(HttpException::class);
        $this->expectExceptionMessage('15-minute request limit reached');

        $headers = [
            'X-RateLimit-Limit' => '10,100',
            'X-RateLimit-Usage' => '11,50',
        ];

        $mockHandler = new MockHandler(
            [
                new Response(401, $headers),
            ]
        );

        $stack = HandlerStack::create($mockHandler);
        $client = $this->getClient($stack);
        (new GetActivities($client))->execute();
    }

    /**
     * @test
     */
    public function will_throw_exception_when_daily_request_limit_reached(): void
    {
        $this->expectException(HttpException::class);
        $this->expectExceptionMessage('Daily request limit reached');

        $headers = [
            'X-RateLimit-Limit' => '10,100',
            'X-RateLimit-Usage' => '9,101',
        ];

        $mockHandler = new MockHandler(
            [
                new Response(401, $headers),
            ]
        );

        $stack = HandlerStack::create($mockHandler);
        $client = $this->getClient($stack);
        (new GetActivities($client))->execute();
    }

    /**
     * @test
     */
    public function will_not_make_double_response_if_requested_twice(): void
    {
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/Athletes/stub/update_athlete.json')),
                new Response(200),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $client = $this->getClient($stack);

        $obj = new UpdateAthlete($client, 114.3);

        $request = $obj->getRequest();
        $this->assertSame('/api/v3/athlete', $request->getUri()->getPath());
        $response = json_decode($obj->getResponse()->getBody(), true, 512, JSON_THROW_ON_ERROR);
        $response2 = $obj->execute();
        $this->assertSame($response['username'], $response2->username);
    }
}
