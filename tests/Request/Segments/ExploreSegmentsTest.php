<?php

namespace Lsv\StravaTest\Request\Segments;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Lsv\Strava\Request\Segments\ExploreSegments;
use Lsv\StravaTest\Request\BaseRequestTest;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;

class ExploreSegmentsTest extends BaseRequestTest
{
    private ExploreSegments $obj;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/explore_segments.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $client = $this->getClient($stack);
        $this->obj = new ExploreSegments($client, 12.53, 15.99, -12.77, -15.87);
    }

    /**
     * @test
     */
    public function can_make_request(): void
    {
        $request = $this->obj->getRequest();

        $this->assertSame('/api/v3/segments/explore', $request->getUri()->getPath());

        $this->assertTrue(
            'bounds%5B0%5D=12,53&bounds%5B1%5D=15,99&bounds%5B2%5D=-12,77&bounds%5B3%5D=-15,87' === $request->getUri(
            )->getQuery()
            ||
            'bounds%5B0%5D=12.53&bounds%5B1%5D=15.99&bounds%5B2%5D=-12.77&bounds%5B3%5D=-15.87' === $request->getUri(
            )->getQuery()
        );
        $this->assertSame('GET', $request->getMethod());
    }

    public function dataProvider(): array
    {
        return [
            ['setActivityType', 'activity_type', 'running'],
            ['setMinimumClimbingCategory', 'min_cat', 25],
            ['setMaximumClimbingCategory', 'max_cat', 4.4],
            ['setMinimumClimbingCategory', 'min_cat', 0.7],
            ['setMaximumClimbingCategory', 'max_cat', -10.3],
        ];
    }

    /**
     * @dataProvider dataProvider
     *
     * @test
     *
     * @param string $key
     * @param mixed  $data
     */
    public function can_make_request_with_optionals(string $setter, $key, $data): void
    {
        $explore = $this->obj;
        $explore->{$setter}($data);
        $request = $explore->getRequest();

        $this->assertStringEndsWith("{$key}={$data}", $request->getUri()->getQuery());
    }

    /**
     * @test
     */
    public function can_not_make_request_with_wrong_activity_type(): void
    {
        $this->expectException(InvalidOptionsException::class);
        $this->expectExceptionMessage(
            'The option "activity_type" with value "does_not_exists" is invalid. Accepted values are: "running", "riding"'
        );
        $this->obj
            ->setActivityType('does_not_exists')
            ->getRequest();
    }

    /**
     * @test
     */
    public function can_parse_response(): void
    {
        $response = $this->obj->execute();
        $this->assertCount(1, $response->segments);
        $segment = $response->segments[0];

        $this->assertSame('229781', $segment->id);
        $this->assertSame(2, $segment->resourceState);
        $this->assertSame('Hawk Hill', $segment->name);
        $this->assertSame(1, $segment->climbCategory);
        $this->assertSame('4', $segment->climbCategoryDesc);
        $this->assertSame(5.7, $segment->avgGrade);
        $this->assertSame(37.8331119, $segment->startLatlng->latitude);
        $this->assertSame(-122.4834356, $segment->startLatlng->longitude);
        $this->assertSame(37.8280722, $segment->endLatlng->latitude);
        $this->assertSame(-122.4981393, $segment->endLatlng->longitude);
        $this->assertSame(152.8, $segment->elevDifference);
        $this->assertSame(2684.8, $segment->distance);
        $this->assertSame(
            '}g|eFnpqjVl@En@Md@HbAd@d@^h@Xx@VbARjBDh@OPQf@w@d@k@XKXDFPH\\EbGT`AV`@v@|@NTNb@?XOb@cAxAWLuE@eAFMBoAv@eBt@q@b@}@tAeAt@i@dAC`AFZj@dB?~@[h@MbAVn@b@b@\\d@Eh@Qb@_@d@eB|@c@h@WfBK|AMpA?VF\\\\t@f@t@h@j@|@b@hCb@b@XTd@Bl@GtA?jAL`ALp@Tr@RXd@Rx@Pn@^Zh@Tx@Zf@`@FTCzDy@f@Yx@m@n@Op@VJr@',
            $segment->points
        );
        $this->assertFalse($segment->starred);
    }
}
