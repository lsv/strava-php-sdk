<?php

namespace Lsv\StravaTest\Request\Segments;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedClub;
use Lsv\Strava\Model\MetaClub;
use Lsv\Strava\Model\SummaryClub;
use Lsv\Strava\Model\SummarySegment;
use Lsv\Strava\Request\Segments\GetSegmentLeaderboard;
use Lsv\StravaTest\Request\BaseRequestTest;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;

class GetSegmentLeaderboardTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/get_segment_leaderboard.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_request(): void
    {
        $request = (new GetSegmentLeaderboard($this->client, '12353452423453456344234'))->getRequest();

        $this->assertSame('/api/v3/segments/12353452423453456344234/leaderboard', $request->getUri()->getPath());
        $this->assertSame('GET', $request->getMethod());
    }

    public function dataProvider(): array
    {
        return [
            ['setGender', 'gender', 'M'],
            ['setGender', 'gender', 'F'],
            ['setAgeGroup', 'age_group', '0_19'],
            ['setAgeGroup', 'age_group', '20_24'],
            ['setAgeGroup', 'age_group', '25_34'],
            ['setAgeGroup', 'age_group', '35_44'],
            ['setAgeGroup', 'age_group', '45_54'],
            ['setAgeGroup', 'age_group', '55_64'],
            ['setAgeGroup', 'age_group', '65_69'],
            ['setAgeGroup', 'age_group', '70_74'],
            ['setAgeGroup', 'age_group', '75_plus'],
            ['setWeightClass', 'weight_class', '125_149'],
            ['setWeightClass', 'weight_class', '150_164'],
            ['setWeightClass', 'weight_class', '165_179'],
            ['setWeightClass', 'weight_class', '180_199'],
            ['setWeightClass', 'weight_class', '200_224'],
            ['setWeightClass', 'weight_class', '225_249'],
            ['setWeightClass', 'weight_class', '250_plus'],
            ['setWeightClass', 'weight_class', '0_54'],
            ['setWeightClass', 'weight_class', '55_64'],
            ['setWeightClass', 'weight_class', '65_74'],
            ['setWeightClass', 'weight_class', '75_84'],
            ['setWeightClass', 'weight_class', '85_94'],
            ['setWeightClass', 'weight_class', '95_104'],
            ['setWeightClass', 'weight_class', '105_114'],
            ['setWeightClass', 'weight_class', '115_plus'],
            ['setFollowing', 'following', null],
            ['setClubId', 'club_id', '43223423454456345123234'],
            ['setDateRange', 'date_range', 'this_year'],
            ['setDateRange', 'date_range', 'this_month'],
            ['setDateRange', 'date_range', 'this_week'],
            ['setDateRange', 'date_range', 'today'],
        ];
    }

    /**
     * @dataProvider dataProvider
     *
     * @test
     *
     * @param string $key
     * @param mixed  $data
     */
    public function can_make_request_with_optionals(string $setter, $key, $data): void
    {
        $explore = new GetSegmentLeaderboard($this->client, '123');
        if ($data) {
            $explore->{$setter}($data);
        } else {
            $data = 1;
            $explore->{$setter}();
        }

        $request = $explore->getRequest();

        $this->assertSame("{$key}={$data}", $request->getUri()->getQuery());
    }

    /**
     * @test
     */
    public function can_not_make_request_with_wrong_age_group(): void
    {
        $this->expectException(InvalidOptionsException::class);
        (new GetSegmentLeaderboard($this->client, '12353452423453456344234'))
            ->setAgeGroup('does_not_exists')
            ->getRequest();
    }

    public function ageDataProvider(): array
    {
        return [
            [0, '0_19'],
            [19, '0_19'],
            [20, '20_24'],
            [54, '45_54'],
            [80, '75_plus'],
        ];
    }

    /**
     * @dataProvider ageDataProvider
     *
     * @test
     *
     * @param int    $age
     * @param string $expected
     */
    public function can_make_request_with_age($age, $expected): void
    {
        $request = (new GetSegmentLeaderboard($this->client, '12353452423453456344234'))
            ->setAge($age)
            ->getRequest();
        $this->assertSame("age_group={$expected}", $request->getUri()->getQuery());
    }

    /**
     * @test
     */
    public function can_not_make_request_with_age_out_of_range(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Can not find a age group for the age \'-10\'');
        (new GetSegmentLeaderboard($this->client, '12353452423453456344234'))
            ->setAge(-10)
            ->getRequest();
    }

    public function weightDataProvider(): array
    {
        return [
            [0, '0_54'],
            [54, '0_54'],
            [55, '55_64'],
            [115, '115_plus'],
            [124, '115_plus'],
            [125, '125_149'],
        ];
    }

    /**
     * @dataProvider weightDataProvider
     *
     * @test
     *
     * @param int    $weight
     * @param string $expected
     */
    public function can_make_request_with_weight($weight, $expected): void
    {
        $request = (new GetSegmentLeaderboard($this->client, '12353452423453456344234'))
            ->setWeight($weight)
            ->getRequest();

        $this->assertSame("weight_class={$expected}", $request->getUri()->getQuery());
    }

    /**
     * @test
     */
    public function can_not_make_request_with_wrong_weight_class(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Can not find a weight class for the weight \'-10\'');

        (new GetSegmentLeaderboard($this->client, '12353452423453456344234'))
            ->setWeight(-10)
            ->getRequest();
    }

    /**
     * @test
     */
    public function can_not_make_request_with_wrong_date_range(): void
    {
        $this->expectException(InvalidOptionsException::class);
        $this->expectExceptionMessage(
            'The option "date_range" with value "does_not_exists" is invalid. Accepted values are: "this_year", "this_month", "this_week", "today"'
        );

        (new GetSegmentLeaderboard($this->client, '12353452423453456344234'))
            ->setDateRange('does_not_exists')
            ->getRequest();
    }

    /**
     * @test
     */
    public function can_make_request_from_meta_club(): void
    {
        $club = new MetaClub();
        $club->id = 123;

        $request = (new GetSegmentLeaderboard($this->client, '12353452423453456344234'))
            ->setClub($club)
            ->getRequest();

        $this->assertSame('club_id=123', $request->getUri()->getQuery());
    }

    /**
     * @test
     */
    public function can_make_request_from_summary_club(): void
    {
        $club = new SummaryClub();
        $club->id = 123;

        $request = (new GetSegmentLeaderboard($this->client, '12353452423453456344234'))
            ->setClub($club)
            ->getRequest();

        $this->assertSame('club_id=123', $request->getUri()->getQuery());
    }

    /**
     * @test
     */
    public function can_make_request_from_detailed_club(): void
    {
        $club = new DetailedClub();
        $club->id = 123;
        $request = (new GetSegmentLeaderboard($this->client, '12353452423453456344234'))
            ->setClub($club)
            ->getRequest();

        $this->assertSame('club_id=123', $request->getUri()->getQuery());
    }

    /**
     * @test
     */
    public function can_make_request_from_summary_segment(): void
    {
        $segment = new SummarySegment();
        $segment->id = 123;
        $request = (new GetSegmentLeaderboard($this->client, $segment))
            ->getRequest();

        $this->assertSame('/api/v3/segments/123/leaderboard', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_with_pagination(): void
    {
        $request = (new GetSegmentLeaderboard($this->client, '12353452423453456344234'))
            ->setPerPage(50)
            ->setPageNumber(3)
            ->getRequest();

        $this->assertSame('per_page=50&page=3', $request->getUri()->getQuery());
    }

    /**
     * @test
     */
    public function can_parse_response(): void
    {
        $response = (new GetSegmentLeaderboard($this->client, '1'))->execute();

        $this->assertSame(30756, $response->entryCount);
        $this->assertSame('kom', $response->komType);
        $this->assertCount(1, $response->entries);
        $entry = $response->entries[0];
        $this->assertSame('Peter S.', $entry->athleteName);
        $this->assertSame(346, $entry->elapsedTime);
        $this->assertSame(346, $entry->movingTime);
        $this->assertSame('03.04.1993', $entry->startDate->format('d.m.Y'));
        $this->assertSame('03.04.1993', $entry->startDateLocal->format('d.m.Y'));
        $this->assertSame(1, $entry->rank);
    }
}
