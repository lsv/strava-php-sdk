<?php

namespace Lsv\StravaTest\Request\Segments;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedSegment;
use Lsv\Strava\Model\SummarySegment;
use Lsv\Strava\Request\Segments\GetSegment;
use Lsv\StravaTest\Request\BaseRequestTest;
use stdClass;

class GetSegmentTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/get_segments.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_request(): void
    {
        $request = (new GetSegment($this->client, '234523421343239048238904234'))->getRequest();

        $this->assertSame('/api/v3/segments/234523421343239048238904234', $request->getUri()->getPath());
        $this->assertSame('GET', $request->getMethod());
    }

    /**
     * @test
     */
    public function can_make_request_with_summary(): void
    {
        $summary = new SummarySegment();
        $summary->id = 123;

        $request = (new GetSegment($this->client, $summary))->getRequest();

        $this->assertSame('/api/v3/segments/123', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_with_detailed(): void
    {
        $detailed = new DetailedSegment();
        $detailed->id = '123';

        $request = (new GetSegment($this->client, $detailed))->getRequest();

        $this->assertSame('/api/v3/segments/123', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_parse_response(): void
    {
        $segment = (new GetSegment($this->client, 1))->execute();
        $this->assertSame('229781', $segment->id);
        $this->assertSame(2428, $segment->starCount);
    }

    /**
     * @test
     */
    public function can_not_use_wrong_object_as_activity_url(): void
    {
        $this->expectException(InvalidArgumentException::class);
        (new GetSegment($this->client, new stdClass()))->execute();
    }
}
