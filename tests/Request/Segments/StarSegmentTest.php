<?php

namespace Lsv\StravaTest\Request\Segments;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Lsv\Strava\Client;
use Lsv\Strava\Request\Segments\StarSegment;
use Lsv\StravaTest\Request\BaseRequestTest;

class StarSegmentTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/get_segments.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_request(): void
    {
        $request = (new StarSegment($this->client, '234523421343239048238904234'))->getRequest();

        $this->assertSame('/api/v3/segments/234523421343239048238904234', $request->getUri()->getPath());
        $this->assertSame('PUT', $request->getMethod());
    }
}
