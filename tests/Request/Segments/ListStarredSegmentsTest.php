<?php

namespace Lsv\StravaTest\Request\Segments;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Lsv\Strava\Client;
use Lsv\Strava\Request\Segments\ListStarredSegments;
use Lsv\StravaTest\Request\BaseRequestTest;

class ListStarredSegmentsTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/list_starred_segments.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_request(): void
    {
        $request = (new ListStarredSegments($this->client))
            ->getRequest();

        $this->assertSame('/api/v3/segments/starred', $request->getUri()->getPath());
        $this->assertSame('GET', $request->getMethod());
    }

    /**
     * @test
     */
    public function can_make_request_with_pagination(): void
    {
        $request = (new ListStarredSegments($this->client))
            ->setPerPage(50)
            ->setPageNumber(3)
            ->getRequest();

        $this->assertSame('per_page=50&page=3', $request->getUri()->getQuery());
    }

    /**
     * @test
     */
    public function can_parse_response(): void
    {
        $response = (new ListStarredSegments($this->client))->execute();

        $this->assertCount(1, $response);
        $segment = $response[0];

        $this->assertSame('229781', $segment->id);
        $this->assertSame(2428, $segment->starCount);
    }
}
