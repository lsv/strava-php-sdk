<?php

namespace Lsv\StravaTest\Request\Streams;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedSegmentEffort;
use Lsv\Strava\Model\SummarySegmentEffort;
use Lsv\Strava\Request\Streams\GetSegmentEffortStreams;
use Lsv\StravaTest\Request\BaseRequestTest;
use stdClass;

class GetSegmentEffortStreamsTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/get_segment_effort_streams.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_request(): void
    {
        $request = (new GetSegmentEffortStreams(
            $this->client,
            '3029482109348230482109438230984209348',
            ['key1', 'key2']
        ))->getRequest();

        $this->assertSame(
            '/api/v3/segment_efforts/3029482109348230482109438230984209348/streams',
            $request->getUri()->getPath()
        );
        $this->assertSame('key_by_type=1&keys%5B0%5D=key1&keys%5B1%5D=key2', $request->getUri()->getQuery());
        $this->assertSame('GET', $request->getMethod());
    }

    /**
     * @test
     */
    public function can_make_request_from_summary(): void
    {
        $summary = new SummarySegmentEffort();
        $summary->id = '3321';

        $request = (new GetSegmentEffortStreams($this->client, $summary, ['key1', 'key2']))->getRequest();

        $this->assertSame('/api/v3/segment_efforts/3321/streams', $request->getUri()->getPath());
        $this->assertSame('GET', $request->getMethod());
    }

    /**
     * @test
     */
    public function can_make_request_from_detailed(): void
    {
        $detailed = new DetailedSegmentEffort();
        $detailed->id = '3464324234213265767586786345345365';

        $request = (new GetSegmentEffortStreams($this->client, $detailed, ['key1', 'key2']))->getRequest();

        $this->assertSame(
            '/api/v3/segment_efforts/3464324234213265767586786345345365/streams',
            $request->getUri()->getPath()
        );
    }

    /**
     * @test
     */
    public function can_parse_response(): void
    {
        $streams = (new GetSegmentEffortStreams($this->client, 1, ['key1', 'key2']))->execute();

        $this->assertCount(1, $streams);
        $stream = $streams[0];

        $this->assertCount(14, $stream->data);
        $this->assertSame(904.5, $stream->data[0]);
        $this->assertSame('distance', $stream->type);
        $this->assertSame('distance', $stream->seriesType);
        $this->assertSame(14, $stream->originalSize);
        $this->assertSame('high', $stream->resolution);
    }

    /**
     * @test
     */
    public function can_not_use_wrong_object_as_activity_url(): void
    {
        $this->expectException(InvalidArgumentException::class);
        (new GetSegmentEffortStreams($this->client, new stdClass(), []))->execute();
    }
}
