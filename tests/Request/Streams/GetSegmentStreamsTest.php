<?php

namespace Lsv\StravaTest\Request\Streams;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedSegment;
use Lsv\Strava\Model\SummarySegment;
use Lsv\Strava\Request\Streams\GetSegmentStreams;
use Lsv\StravaTest\Request\BaseRequestTest;
use stdClass;

class GetSegmentStreamsTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/get_segment_streams.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_request(): void
    {
        $request = (new GetSegmentStreams(
            $this->client,
            '3029482109348230482109438230984209348',
            ['key1', 'key2']
        ))->getRequest();

        $this->assertSame(
            '/api/v3/segments/3029482109348230482109438230984209348/streams',
            $request->getUri()->getPath()
        );
        $this->assertSame('key_by_type=1&keys%5B0%5D=key1&keys%5B1%5D=key2', $request->getUri()->getQuery());
        $this->assertSame('GET', $request->getMethod());
    }

    /**
     * @test
     */
    public function can_make_request_from_summary(): void
    {
        $summary = new SummarySegment();
        $summary->id = '3321';

        $request = (new GetSegmentStreams($this->client, $summary, ['key1', 'key2']))->getRequest();

        $this->assertSame('/api/v3/segments/3321/streams', $request->getUri()->getPath());
        $this->assertSame('GET', $request->getMethod());
    }

    /**
     * @test
     */
    public function can_make_request_from_detailed(): void
    {
        $detailed = new DetailedSegment();
        $detailed->id = '3464324234213265767586786345345365';

        $request = (new GetSegmentStreams($this->client, $detailed, ['key1', 'key2']))->getRequest();

        $this->assertSame('/api/v3/segments/3464324234213265767586786345345365/streams', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_parse_response(): void
    {
        $streams = (new GetSegmentStreams($this->client, 1, ['key1', 'key2']))->execute();

        $this->assertCount(3, $streams);
        $stream = $streams[0];

        $this->assertCount(2, $stream->data);
        $this->assertSame(37.833112, $stream->data[0][0]);
        $this->assertSame('latlng', $stream->type);
        $this->assertSame('distance', $stream->seriesType);
        $this->assertSame(2, $stream->originalSize);
        $this->assertSame('high', $stream->resolution);
    }

    /**
     * @test
     */
    public function can_not_use_wrong_object_as_activity_url(): void
    {
        $this->expectException(InvalidArgumentException::class);
        (new GetSegmentStreams($this->client, new stdClass(), []))->execute();
    }
}
