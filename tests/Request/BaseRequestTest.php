<?php

namespace Lsv\StravaTest\Request;

use GuzzleHttp\HandlerStack;
use Http\Adapter\Guzzle6\Client;
use Http\Message\Authentication\Bearer;
use Lsv\Strava\Client as StravaClient;
use PHPUnit\Framework\TestCase;

abstract class BaseRequestTest extends TestCase
{
    protected function getClient(?HandlerStack $handler = null): StravaClient
    {
        $options = [];
        if ($handler) {
            $options['handler'] = $handler;
        }

        $bearer = new Bearer('token');

        return new StravaClient($bearer, new Client(new \GuzzleHttp\Client($options)));
    }
}
