<?php

namespace Lsv\StravaTest\Request\Routes;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use Lsv\Strava\Client;
use Lsv\Strava\Model\Route;
use Lsv\Strava\Request\Routes\GetRoute;
use Lsv\StravaTest\Request\BaseRequestTest;
use stdClass;

class GetRouteTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/get_route.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_request(): void
    {
        $request = (new GetRoute($this->client, '333345456454562343455436452434'))->getRequest();

        $this->assertSame('/api/v3/routes/333345456454562343455436452434', $request->getUri()->getPath());
        $this->assertSame('GET', $request->getMethod());
    }

    /**
     * @test
     */
    public function can_make_request_from_route(): void
    {
        $route = new Route();
        $route->id = 1334;

        $request = (new GetRoute($this->client, $route))->getRequest();

        $this->assertSame('/api/v3/routes/1334', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_parse_response(): void
    {
        $response = (new GetRoute($this->client, 1))->execute();

        $this->assertTrue($response->private);
        $this->assertSame(0.8008282, $response->distance);
        $this->assertSame('134815', $response->athlete->id);
        $this->assertSame('aeiou', $response->description);
        $this->assertSame(6.0274563, $response->elevationGain);
        $this->assertSame(5, $response->type);

        $this->assertCount(1, $response->segments);
        $segment = $response->segments[0];
        $this->assertSame('aeiou', $segment->country);
        $this->assertTrue($segment->private);
        $this->assertSame(6.846853, $segment->athletePrEffort->distance);
        $this->assertSame(7.386282, $segment->elevationLow);

        $this->assertTrue($response->starred);

        $this->assertCount(1, $response->directions);
        $direction = $response->directions[0];
        $this->assertSame(7.4577446, $direction->distance);
        $this->assertSame('aeiou', $direction->name);
        $this->assertSame(1, $direction->action);

        $this->assertSame(2, $response->subType);
        $this->assertSame('aeiou', $response->name);
        $this->assertSame('1', $response->id);
        $this->assertSame('aeiou', $response->map->summaryPolyline);
        $this->assertSame('aeiou', $response->map->id);
        $this->assertSame('aeiou', $response->map->polyline);
    }

    /**
     * @test
     */
    public function can_not_use_wrong_object_as_activity_url(): void
    {
        $this->expectException(InvalidArgumentException::class);
        (new GetRoute($this->client, new stdClass()))->execute();
    }
}
