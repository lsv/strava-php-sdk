<?php

namespace Lsv\StravaTest\Request\Routes;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedAthlete;
use Lsv\Strava\Model\MetaAthlete;
use Lsv\Strava\Model\SummaryAthlete;
use Lsv\Strava\Request\Routes\ListAthleteRoutes;
use Lsv\StravaTest\Request\BaseRequestTest;
use stdClass;

class ListAthleteRoutesTest extends BaseRequestTest
{
    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/list_athletes_routes.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    private Client $client;

    /**
     * @test
     */
    public function can_make_request(): void
    {
        $request = (new ListAthleteRoutes($this->client, '3242343453455345242134'))->getRequest();

        $this->assertSame('/api/v3/athletes/3242343453455345242134/routes', $request->getUri()->getPath());
        $this->assertSame('GET', $request->getMethod());
    }

    /**
     * @test
     */
    public function can_make_request_from_meta_athlete(): void
    {
        $meta = new MetaAthlete();
        $meta->id = 1334;

        $request = (new ListAthleteRoutes($this->client, $meta))->getRequest();

        $this->assertSame('/api/v3/athletes/1334/routes', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_from_summary_athlete(): void
    {
        $summary = new SummaryAthlete();
        $summary->id = 334;

        $request = (new ListAthleteRoutes($this->client, $summary))->getRequest();

        $this->assertSame('/api/v3/athletes/334/routes', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_from_detailed_athlete(): void
    {
        $detailed = new DetailedAthlete();
        $detailed->id = 1432;

        $request = (new ListAthleteRoutes($this->client, $detailed))->getRequest();

        $this->assertSame('/api/v3/athletes/1432/routes', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_with_pagination(): void
    {
        $request = (new ListAthleteRoutes($this->client, 123))
            ->setPageNumber(3)
            ->setPerPage(50)
            ->getRequest();

        $this->assertSame('/api/v3/athletes/123/routes', $request->getUri()->getPath());
        $this->assertSame('page=3&per_page=50', $request->getUri()->getQuery());
    }

    /**
     * @test
     */
    public function can_parse_response(): void
    {
        $response = (new ListAthleteRoutes($this->client, 1))->execute();

        $this->assertCount(1, $response);
        $this->assertSame('1', $response[0]->id);
    }

    /**
     * @test
     */
    public function can_not_use_wrong_object_as_activity_url(): void
    {
        $this->expectException(InvalidArgumentException::class);
        (new ListAthleteRoutes($this->client, new stdClass()))->execute();
    }
}
