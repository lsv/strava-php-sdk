<?php

namespace Lsv\StravaTest\Request\Routes;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use Lsv\Strava\Client;
use Lsv\Strava\Model\Route;
use Lsv\Strava\Request\Routes\ExportRouteGpx;
use Lsv\StravaTest\Request\BaseRequestTest;
use stdClass;

class ExportRouteGpxTest extends BaseRequestTest
{
    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/uploadfile.gpx')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    private Client $client;

    /**
     * @test
     */
    public function can_make_request(): void
    {
        $request = (new ExportRouteGpx($this->client, '333345456454562343455436452434'))->getRequest();

        $this->assertSame('/api/v3/routes/333345456454562343455436452434/export_gpx', $request->getUri()->getPath());
        $this->assertSame('GET', $request->getMethod());
    }

    /**
     * @test
     */
    public function can_make_request_from_route(): void
    {
        $route = new Route();
        $route->id = 1334;

        $request = (new ExportRouteGpx($this->client, $route))->getRequest();

        $this->assertSame('/api/v3/routes/1334/export_gpx', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_parse_response(): void
    {
        $response = (new ExportRouteGpx($this->client, 1))->execute();

        $this->assertStringEqualsFile(__DIR__.'/stub/uploadfile.gpx', $response);
    }

    /**
     * @test
     */
    public function can_not_use_wrong_object_as_activity_url(): void
    {
        $this->expectException(InvalidArgumentException::class);
        (new ExportRouteGpx($this->client, new stdClass()))->execute();
    }
}
