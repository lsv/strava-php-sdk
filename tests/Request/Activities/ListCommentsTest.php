<?php

namespace Lsv\StravaTest\Request\Activities;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedActivity;
use Lsv\Strava\Model\MetaActivity;
use Lsv\Strava\Model\SummaryActivity;
use Lsv\Strava\Request\Activities\ListComments;
use Lsv\StravaTest\Request\BaseRequestTest;
use stdClass;

class ListCommentsTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/list_comments.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_correct_url(): void
    {
        $request = (new ListComments($this->client, 1))
            ->setPageNumber(2)
            ->setPerPage(50)
            ->getRequest();

        $this->assertSame('/api/v3/activities/1/comments', $request->getUri()->getPath());
        $this->assertSame('page=2&per_page=50', $request->getUri()->getQuery());
    }

    /**
     * @test
     */
    public function can_make_request_from_meta_activity(): void
    {
        $meta = new MetaActivity();
        $meta->id = 10;

        $request = (new ListComments($this->client, $meta))->getRequest();

        $this->assertSame('/api/v3/activities/10/comments', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_from_summary_activity(): void
    {
        $summary = new SummaryActivity();
        $summary->id = '2313453253453413213';

        $request = (new ListComments($this->client, $summary))->getRequest();

        $this->assertSame('/api/v3/activities/2313453253453413213/comments', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_from_detailed_activity(): void
    {
        $detailed = new DetailedActivity();
        $detailed->id = 22;

        $request = (new ListComments($this->client, $detailed))->getRequest();

        $this->assertSame('/api/v3/activities/22/comments', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_with_pagination(): void
    {
        $request = (new ListComments($this->client, 123))
            ->setPageNumber(3)
            ->setPerPage(50)
            ->getRequest();

        $this->assertSame('page=3&per_page=50', $request->getUri()->getQuery());
    }

    /**
     * @test
     */
    public function can_parse_results(): void
    {
        $response = (new ListComments($this->client, 1))->execute();

        $this->assertCount(2, $response);
        $comment = $response[0];

        $this->assertSame('12345678987654321', $comment->id);
        $this->assertSame(12345678987654321, $comment->activityId);
        $this->assertNull($comment->postId);
        $this->assertSame(2, $comment->resourceState);
        $this->assertSame('Good job and keep the cat pictures coming!', $comment->text);
        $this->assertNull($comment->mentionsMetadata);
        $this->assertSame('08.02.2018 19:25:39', $comment->createdAt->format('d.m.Y H:i:s'));
        $this->assertSame('psagan', $comment->athlete->username);
    }

    /**
     * @test
     */
    public function can_not_use_wrong_object_as_activity_url(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $fake = new stdClass();
        (new ListComments($this->client, $fake))->execute();
    }
}
