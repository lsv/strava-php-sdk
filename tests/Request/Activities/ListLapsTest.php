<?php

namespace Lsv\StravaTest\Request\Activities;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedActivity;
use Lsv\Strava\Model\MetaActivity;
use Lsv\Strava\Model\SummaryActivity;
use Lsv\Strava\Request\Activities\ListLaps;
use Lsv\StravaTest\Request\BaseRequestTest;
use stdClass;

class ListLapsTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/list_laps.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_correct_url(): void
    {
        $request = (new ListLaps($this->client, 1))->getRequest();

        $this->assertSame('/api/v3/activities/1/laps', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_from_meta_activity(): void
    {
        $meta = new MetaActivity();
        $meta->id = 10;

        $request = (new ListLaps($this->client, $meta))->getRequest();

        $this->assertSame('/api/v3/activities/10/laps', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_from_summary_activity(): void
    {
        $summary = new SummaryActivity();
        $summary->id = '2313453253453413213';

        $request = (new ListLaps($this->client, $summary))->getRequest();

        $this->assertSame('/api/v3/activities/2313453253453413213/laps', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_from_detailed_activity(): void
    {
        $detailed = new DetailedActivity();
        $detailed->id = 22;

        $request = (new ListLaps($this->client, $detailed))->getRequest();

        $this->assertSame('/api/v3/activities/22/laps', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_parse_results(): void
    {
        $response = (new ListLaps($this->client, 1))->execute();

        $this->assertCount(2, $response);
        $lap = $response[0];

        $this->assertSame('12345678987654321', $lap->id);
    }

    /**
     * @test
     */
    public function can_not_use_wrong_object_as_activity_url(): void
    {
        $this->expectException(InvalidArgumentException::class);
        (new ListLaps($this->client, new stdClass()))->execute();
    }
}
