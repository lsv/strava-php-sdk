<?php

namespace Lsv\StravaTest\Request\Activities;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedActivity;
use Lsv\Strava\Model\MetaActivity;
use Lsv\Strava\Model\SummaryActivity;
use Lsv\Strava\Model\SummaryGear;
use Lsv\Strava\Request\Activities\UpdateActivity;
use Lsv\StravaTest\Request\BaseRequestTest;
use stdClass;

class UpdateActivityTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/update_activity.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_correct_url(): void
    {
        $request = (new UpdateActivity($this->client, 1))->getRequest();

        $this->assertSame('/api/v3/activities/1', $request->getUri()->getPath());
        $this->assertSame('PUT', $request->getMethod());
    }

    /**
     * @test
     */
    public function can_make_correct_body(): void
    {
        $expectedResult = [
            'name' => 'name',
            'commute' => 1,
            'gear_id' => 'g321',
            'trainer' => 1,
            'description' => 'desc',
            'type' => 'type',
        ];

        $request = (new UpdateActivity($this->client, 1))
            ->setName('name')
            ->setCommute(true)
            ->setGearId('g321')
            ->setTrainer(true)
            ->setDescription('desc')
            ->setType('type')
            ->getRequest();

        parse_str($request->getBody(), $content);
        foreach ($expectedResult as $key => $value) {
            $this->assertArrayHasKey($key, $content);
            $this->assertEquals($value, $content[$key]);
        }
    }

    /**
     * @test
     */
    public function can_set_gear_by_summary_gear(): void
    {
        $expectedResult = [
            'gear_id' => 'g332',
        ];

        $gear = new SummaryGear();
        $gear->id = 'g332';
        $request = (new UpdateActivity($this->client, 1))
            ->setGearId($gear)
            ->getRequest();

        parse_str($request->getBody(), $content);
        foreach ($expectedResult as $key => $value) {
            $this->assertArrayHasKey($key, $content);
            $this->assertEquals($value, $content[$key]);
        }
    }

    /**
     * @test
     */
    public function can_make_request_from_meta_activity(): void
    {
        $meta = new MetaActivity();
        $meta->id = 10;

        $request = (new UpdateActivity($this->client, $meta))->getRequest();

        $this->assertSame('/api/v3/activities/10', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_from_summary_activity(): void
    {
        $summary = new SummaryActivity();
        $summary->id = '2313453253453413213';

        $request = (new UpdateActivity($this->client, $summary))->getRequest();

        $this->assertSame('/api/v3/activities/2313453253453413213', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_from_detailed_activity(): void
    {
        $detailed = new DetailedActivity();
        $detailed->id = 22;

        $request = (new UpdateActivity($this->client, $detailed))->getRequest();

        $this->assertSame('/api/v3/activities/22', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_parse_results(): void
    {
        $response = (new UpdateActivity($this->client, 1))->execute();

        $this->assertSame('12345678987654321', $response->id);
    }

    /**
     * @test
     */
    public function can_not_use_wrong_object_as_activity_url(): void
    {
        $this->expectException(InvalidArgumentException::class);
        (new UpdateActivity($this->client, new stdClass()))->execute();
    }
}
