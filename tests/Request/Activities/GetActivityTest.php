<?php

namespace Lsv\StravaTest\Request\Activities;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedActivity;
use Lsv\Strava\Model\MetaActivity;
use Lsv\Strava\Model\SummaryActivity;
use Lsv\Strava\Request\Activities\GetActivity;
use Lsv\StravaTest\Request\BaseRequestTest;
use stdClass;

class GetActivityTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/get_activity.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_request(): void
    {
        $activity = (new GetActivity($this->client, 133))
            ->setIncludeAllEfforts();

        $request = $activity->getRequest();
        $this->assertSame('GET', $request->getMethod());
        $this->assertSame('/api/v3/activities/133', $request->getUri()->getPath());
        $this->assertSame('include_all_efforts=1', $request->getUri()->getQuery());

        $activity = new GetActivity($this->client, 133);
        $request = $activity->getRequest();
        $this->assertSame('', $request->getUri()->getQuery());
    }

    /**
     * @test
     */
    public function can_make_request_from_meta_activity(): void
    {
        $meta = new MetaActivity();
        $meta->id = 10;
        $request = (new GetActivity($this->client, $meta))->getRequest();

        $this->assertSame('/api/v3/activities/10', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_from_summary_activity(): void
    {
        $summary = new SummaryActivity();
        $summary->id = '2313453253453413213';
        $request = (new GetActivity($this->client, $summary))->getRequest();

        $this->assertSame('/api/v3/activities/2313453253453413213', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_from_detailed_activity(): void
    {
        $meta = new DetailedActivity();
        $meta->id = 22;
        $request = (new GetActivity($this->client, $meta))->getRequest();

        $this->assertSame('/api/v3/activities/22', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_get_activity(): void
    {
        $activity = (new GetActivity($this->client, 333))
            ->setIncludeAllEfforts();

        $details = $activity->execute();

        $this->assertSame('134815', $details->athlete->id);
        $this->assertSame(1, $details->athlete->resourceState);
        $this->assertSame(37.83, $details->startLatlng->latitude);
        $this->assertSame(-122.26, $details->startLatlng->longitude);
        $this->assertSame(37.83, $details->endLatlng->latitude);
        $this->assertSame(-122.26, $details->endLatlng->longitude);
        $this->assertSame('a1410355832', $details->map->id);
        $this->assertSame(
            'ki{eFvqfiVqAWQIGEEKAYJgBVqDJ{BHa@jAkNJw@Pw@V{APs@^aABQAOEQGKoJ_FuJkFqAo@{A}@sH{DiAs@Q]?WVy@`@oBt@_CB]KYMMkB{AQEI@WT{BlE{@zAQPI@ICsCqA_BcAeCmAaFmCqIoEcLeG}KcG}A}@cDaBiDsByAkAuBqBi@y@_@o@o@kB}BgIoA_EUkAMcACa@BeBBq@LaAJe@b@uA`@_AdBcD`@iAPq@RgALqAB{@EqAyAoOCy@AmCBmANqBLqAZkB\iCPiBJwCCsASiCq@iD]eA]y@[i@w@mAa@i@k@g@kAw@i@Ya@Q]EWFMLa@~BYpAFNpA`Aj@n@X`@V`AHh@JfB@xAMvAGZGHIDIAWOEQNcC@sACYK[MSOMe@QKKKYOs@UYQISCQ?Q@WNo@r@OHGAGCKOQ_BU}@MQGG]Io@@c@FYNg@d@s@d@ODQAMOMaASs@_@a@SESAQDqBn@a@RO?KK?UBU\kA@Y?WMo@Iy@GWQ_@WSSGg@AkABQB_Ap@_A^o@b@Q@o@IS@OHi@n@OFS?OI}@iAQMQGQC}@DOIIUK{@IUOMyBo@kASOKIQCa@L[|AgATWN[He@?QKw@FOPCh@Fx@l@TDLELKl@aAHIJEX@r@ZTDV@LENQVg@RkA@c@MeA?WFOPMf@Ej@Fj@@LGHKDM?_@_@iC?a@HKRIl@NT?FCHMFW?YEYGWQa@GYBiAIq@Gq@L_BHSHK|@WJETSLQZs@z@_A~@uA^U`@G\CRB\Tl@p@Th@JZ^bB`@lAHLXVLDP?LGFSKiDBo@d@wBVi@R]VYVE\@`@Lh@Fh@CzAk@RSDQA]GYe@eAGWSiBAWBWBIJORK`@KPOPSTg@h@}Ad@o@F[E_@EGMKUGmAEYGMIMYKs@?a@J}@@_BD_@HQJMx@e@LKHKHWAo@UoAAWFmAH}@?w@C[YwAAc@HSNM|Ao@rA}@zAq@`@a@j@eAxAuBXQj@MXSR[b@gAFg@?YISOGaAHi@Xw@v@_@d@WRSFqARUHQJc@d@m@`A[VSFUBcAEU@WFULUPa@v@Y~@UrBc@dBI~@?l@P~ABt@N`HEjA]zAEp@@p@TrBCl@CTQb@k@dAg@jAU^KJYLK@k@A[Js@d@a@b@]RgBl@[FMAw@[]G]?m@D_@F]P[Vu@t@[TMF_@Do@E_@@q@P]PWZUZw@vAkAlAGJOj@IlAMd@OR{@p@a@d@sBpD]v@a@`Aa@n@]TODgBVk@Pe@^cBfBc@Rs@La@RSPm@|@wCpDS^Wp@QZML{@l@qBbCYd@k@lAIVCZBZNTr@`@RRHZANIZQPKDW@e@CaASU?I@YTKRQx@@\VmALYRQLCL?v@P|@D\GJEFKDM@OCa@COOYIGm@YMUCM@]JYr@uAx@kAt@}@jAeAPWbAkBj@s@bAiAz@oAj@m@VQlAc@VQ~@aA`Au@p@Q`AIv@MZORUV_@p@iB|AoCh@q@dAaANUNWH[N{AJ[^m@t@_Av@wA\a@`@W`@In@Al@B^E`@Wl@u@\[VQ\K`@Eb@?R@dAZP@d@CRExAs@\Yt@{@LG\MjAATINOXo@d@kAl@_AHYBOCe@QiBCm@Fq@\wADo@AyGEeBWuB@YHu@Tu@Lk@VcCTo@d@aA\WJE`@G~@FP?VI\U~@sANO`@SfAMj@U\WjAsAXS`@UNENALBHFFL?^Ml@Uj@]b@q@RUJSPkChEc@XcAb@sA|@]PaA\OJKNER?TDTNj@Jn@?p@OfC@ZR`B@VCV_@n@{@l@WbACv@OlABnAPl@LNNHbBBNBLFFJ@^GLg@x@i@|AMP[X}@XOJKPET?l@LhAFXp@fBDRCd@S\_@Ps@PQ@}A]S?QDe@V]b@MR[fAKt@ErAF~CANILYDKGIKe@{@Yy@e@sB[gA[c@e@YUCU?WBUHUNQPq@`AiArAMV[^e@Zc@JQJKNMz@?r@Bb@PfAAfA@VVbADn@E`@KHSEe@SMAKDKFM\^dDCh@m@LoAQ_@@MFOZLfBEl@QbASd@KLQBOAaAc@QAQ@QHc@v@ONMJOBOCg@c@]O[EMBKFGL?RHv@ARERGNe@h@{@h@WVGNDt@JLNFPFz@LdBf@f@PJNHPF`ADPJJJDl@I`@B^Tp@bALJNDNALIf@i@PGPCt@DNE`@Uv@[dAw@RITGRCtAARBPJLPJRZxB?VEX_@vAAR?RDNHJJBh@UnBm@h@IRDRJNNJPNbBFRJLLBLCzAmAd@Uf@Gf@?P@PFJNHPFTH`BDTHNJJJ@LG`@m@^YPER@RDPHNNJRLn@HRLN^VNPHTFX@\UlDFb@FHh@NP@HKPsB?}ASkCQ{@[y@q@}@cA{@KOCQDa@t@{CFGJCf@Nl@ZtA~@r@p@`@h@rAxBd@rA\fARdAPjANrB?f@AtBCd@QfBkAjJOlBChA?rBFrBNlBdAfKFzAC~@Iz@Mz@Sv@s@jBmAxBi@hAWt@Sv@Qx@O`BA`@?dAPfBVpAd@`BfBlFf@fBdA~Cr@pAz@fApBhBjAt@H?IL?FBFJLx@^lHvDvh@~XnElCbAd@pGhDbAb@nAr@`Ad@`GhDnBbAxCbBrWhNJJDPARGP_@t@Qh@]pAUtAoA`Ny@jJApBBNFLJFJBv@Hb@HBF?\\',
            $details->map->polyline
        );
        $this->assertSame(3, $details->map->resourceState);
        $this->assertSame(
            'ki{eFvqfiVsBmA`Feh@qg@iX`B}JeCcCqGjIq~@kf@cM{KeHeX`@_GdGkSeBiXtB}YuEkPwFyDeAzAe@pC~DfGc@bIOsGmCcEiD~@oBuEkFhBcBmDiEfAVuDiAuD}NnDaNiIlCyDD_CtJKv@wGhD]YyEzBo@g@uKxGmHpCGtEtI~AuLrHkAcAaIvEgH_EaDR_FpBuBg@sNxHqEtHgLoTpIiCzKNr[sB|Es\\`JyObYeMbGsMnPsAfDxAnD}DBu@bCx@{BbEEyAoD`AmChNoQzMoGhOwX|[yIzBeFKg[zAkIdU_LiHxK}HzEh@vM_BtBg@xGzDbCcF~GhArHaIfByAhLsDiJuC?_HbHd@nL_Cz@ZnEkDDy@hHwJLiCbIrNrIvN_EfAjDWlEnEiAfBxDlFkBfBtEfDaAzBvDKdFx@|@XgJmDsHhAgD`GfElEzOwBnYdBxXgGlSc@bGdHpW|HdJztBnhAgFxc@HnCvBdA',
            $details->map->summaryPolyline
        );
        $this->assertCount(1, $details->segmentEfforts);
        $segment = $details->segmentEfforts[0];
        $this->assertSame('12345678987654321', $segment->id);
        $this->assertSame(2, $segment->resourceState);
        $this->assertSame('Tunnel Rd.', $segment->name);
        $this->assertSame('12345678987654321', $segment->activity->id);
        $this->assertSame(1, $segment->activity->resourceState);
        $this->assertSame('134815', $segment->athlete->id);
        $this->assertSame(1, $segment->athlete->resourceState);

        $this->assertSame('673683', $segment->segment->id);
        $this->assertSame(2, $segment->segment->resourceState);
        $this->assertSame('Tunnel Rd.', $segment->segment->name);
        $this->assertSame('Ride', $segment->segment->activityType);
        $this->assertSame(9220.7, $segment->segment->distance);
        $this->assertSame(37.8346153, $segment->segment->startLatlng->latitude);
        $this->assertSame(-122.2520872, $segment->segment->startLatlng->longitude);
        $this->assertSame(37.8476261, $segment->segment->endLatlng->latitude);
        $this->assertSame(-122.2008944, $segment->segment->endLatlng->longitude);

        $this->assertCount(1, $details->splitsMetric);
        $this->assertSame(1001.5, $details->splitsMetric[0]->distance);
        $this->assertSame(141, $details->splitsMetric[0]->elapsedTime);
        $this->assertSame(4.4, $details->splitsMetric[0]->elevationDifference);
        $this->assertSame(141, $details->splitsMetric[0]->movingTime);
        $this->assertSame(1, $details->splitsMetric[0]->split);
        $this->assertSame(7.1, $details->splitsMetric[0]->averageSpeed);
        $this->assertSame(0, $details->splitsMetric[0]->paceZone);

        $this->assertCount(1, $details->splitsStandard);
        $this->assertSame(1001.5, $details->splitsStandard[0]->distance);
        $this->assertSame(141, $details->splitsStandard[0]->elapsedTime);
        $this->assertSame(4.4, $details->splitsStandard[0]->elevationDifference);
        $this->assertSame(141, $details->splitsStandard[0]->movingTime);
        $this->assertSame(1, $details->splitsStandard[0]->split);
        $this->assertSame(7.1, $details->splitsStandard[0]->averageSpeed);
        $this->assertSame(0, $details->splitsStandard[0]->paceZone);

        $this->assertCount(1, $details->laps);
        $this->assertSame('4479306946', $details->laps[0]->id);
        $this->assertSame('1410355832', $details->laps[0]->activity->id);
        $this->assertSame('134815', $details->laps[0]->athlete->id);

        $this->assertSame('b12345678987654321', $details->gear->id);
        $this->assertTrue($details->gear->primary);
        $this->assertSame('Tarmac', $details->gear->name);

        $this->assertNull($details->photos->primary->id);
        $this->assertSame('3FDGKL3-204E-4867-9E8D-89FC79EAAE17', $details->photos->primary->uniqueId);
        $this->assertCount(2, $details->photos->primary->urls);
        $this->assertSame(2, $details->photos->count);

        $this->assertCount(1, $details->highlightedKudosers);
        $this->assertSame('strava://athletes/12345678987654321', $details->highlightedKudosers[0]->destinationUrl);
        $this->assertSame('Marianne V.', $details->highlightedKudosers[0]->displayName);
        $this->assertSame(
            'https://dgalywyr863hv.cloudfront.net/pictures/athletes/12345678987654321/12345678987654321/3/medium.jpg',
            $details->highlightedKudosers[0]->avatarUrl
        );
        $this->assertTrue($details->highlightedKudosers[0]->showName);
    }

    /**
     * @test
     */
    public function can_not_use_wrong_object_as_activity_url(): void
    {
        $this->expectException(InvalidArgumentException::class);
        (new GetActivity($this->client, new stdClass()))->execute();
    }
}
