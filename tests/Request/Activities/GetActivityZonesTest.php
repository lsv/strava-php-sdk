<?php

namespace Lsv\StravaTest\Request\Activities;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedActivity;
use Lsv\Strava\Model\MetaActivity;
use Lsv\Strava\Model\SummaryActivity;
use Lsv\Strava\Request\Activities\GetActivityZones;
use Lsv\StravaTest\Request\BaseRequestTest;
use stdClass;

class GetActivityZonesTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/get_activity_zones.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_correct_url(): void
    {
        $request = (new GetActivityZones($this->client, 1))->getRequest();

        $this->assertSame('/api/v3/activities/1/zones', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_from_meta_activity(): void
    {
        $meta = new MetaActivity();
        $meta->id = 10;

        $request = (new GetActivityZones($this->client, $meta))->getRequest();

        $this->assertSame('/api/v3/activities/10/zones', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_from_summary_activity(): void
    {
        $summary = new SummaryActivity();
        $summary->id = '2313453253453413213';

        $request = (new GetActivityZones($this->client, $summary))->getRequest();

        $this->assertSame('/api/v3/activities/2313453253453413213/zones', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_from_detailed_activity(): void
    {
        $meta = new DetailedActivity();
        $meta->id = 22;

        $request = (new GetActivityZones($this->client, $meta))->getRequest();

        $this->assertSame('/api/v3/activities/22/zones', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_parse_results(): void
    {
        $response = (new GetActivityZones($this->client, 1))->execute();

        $this->assertCount(2, $response);
        $zone = $response[0];

        $this->assertSame(0, $zone->score);
        $this->assertTrue($zone->sensorBased);
        $this->assertTrue($zone->customZones);
        $this->assertSame(1, $zone->max);
        $this->assertSame('heartrate', $zone->type);
        $this->assertSame(6, $zone->points);
        $this->assertCount(1, $zone->distributionBuckets);
        $this->assertSame(1498, $zone->distributionBuckets[0]->time);
    }

    /**
     * @test
     */
    public function can_not_use_wrong_object_as_activity_url(): void
    {
        $this->expectException(InvalidArgumentException::class);
        (new GetActivityZones($this->client, new stdClass()))->execute();
    }
}
