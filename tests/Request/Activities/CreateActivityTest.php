<?php

namespace Lsv\StravaTest\Request\Activities;

use DateTime;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Lsv\Strava\Request\Activities\CreateActivity;
use Lsv\StravaTest\Request\BaseRequestTest;

class CreateActivityTest extends BaseRequestTest
{
    private CreateActivity $object;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/create_activity.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $client = $this->getClient($stack);
        $this->object = new CreateActivity($client, 'name', 'run', 10);
    }

    /**
     * @test
     */
    public function can_make_request(): void
    {
        $request = $this->object
            ->setStartDate(new DateTime('2018-12-24 10:00:00Z'))
            ->setDescription('description')
            ->setDistance(30)
            ->setTrainer(false)
            ->setPhotoIds([1, 2, 3, 4])
            ->setCommute(true)
            ->getRequest();

        $this->assertSame('POST', $request->getMethod());
        $this->assertSame('/api/v3/activities', $request->getUri()->getPath());
        parse_str($request->getBody(), $content);
        $this->assertArrayHasKey('start_date_local', $content);
        $this->assertSame('2018-12-24T10:00:00+00:00', $content['start_date_local']);
        $this->assertArrayHasKey('name', $content);
        $this->assertSame('name', $content['name']);
        $this->assertArrayHasKey('type', $content);
        $this->assertSame('run', $content['type']);
        $this->assertArrayHasKey('elapsed_time', $content);
        $this->assertSame('10', $content['elapsed_time']);
        $this->assertArrayHasKey('description', $content);
        $this->assertSame('description', $content['description']);
        $this->assertArrayHasKey('distance', $content);
        $this->assertSame('30', $content['distance']);
        $this->assertArrayHasKey('trainer', $content);
        $this->assertSame('0', $content['trainer']);
        $this->assertArrayHasKey('photo_ids', $content);
        $this->assertSame('1,2,3,4', $content['photo_ids']);
        $this->assertArrayHasKey('commute', $content);
        $this->assertSame('1', $content['commute']);
    }

    /**
     * @test
     */
    public function can_create_activity_with_required_options(): void
    {
        $details = $this->object->execute();

        $this->assertSame('123456778928065', $details->id);
        $this->assertSame(3, $details->resourceState);
        $this->assertSame('12343545645788', $details->athlete->id);
        $this->assertSame('Chill Day', $details->name);
        $this->assertSame(18373, $details->movingTime);
        $this->assertSame(18373, $details->elapsedTime);
        $this->assertSame('Ride', $details->type);
        $this->assertSame('20.02.2018 18:02', $details->startDate->format('d.m.Y H:i'));
        $this->assertSame('20.02.2018 02:02', $details->startDateLocal->format('d.m.Y H:i'));
        $this->assertSame('-08:00', $details->startDateLocal->getTimezone()->getName());
        $this->assertSame(1, $details->athleteCount);
        $this->assertSame('a12345678908766', $details->map->id);
        $this->assertSame('b453542543', $details->gearId);
        $this->assertSame('e65c2a814xxxxxxxxxxxxxxxxx68', $details->embedToken);
    }

    /**
     * @test
     */
    public function can_create_activity_with_optional_options(): void
    {
        $details = $this->object
            ->setDescription('description')
            ->setDistance(30)
            ->setTrainer(false)
            ->setPhotoIds([1, 2, 3, 4])
            ->setCommute(true)
            ->execute();

        $this->assertSame('123456778928065', $details->id);
    }
}
