<?php

namespace Lsv\StravaTest\Request\Activities;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedActivity;
use Lsv\Strava\Model\MetaActivity;
use Lsv\Strava\Model\SummaryActivity;
use Lsv\Strava\Request\Activities\ListKudoers;
use Lsv\StravaTest\Request\BaseRequestTest;
use stdClass;

class ListKudoersTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/list_kudoers.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_correct_url(): void
    {
        $request = (new ListKudoers($this->client, 1))
            ->setPageNumber(2)
            ->setPerPage(50)
            ->getRequest();

        $this->assertSame('/api/v3/activities/1/kudos', $request->getUri()->getPath());
        $this->assertSame('page=2&per_page=50', $request->getUri()->getQuery());
    }

    /**
     * @test
     */
    public function can_make_request_from_meta_activity(): void
    {
        $meta = new MetaActivity();
        $meta->id = 10;

        $request = (new ListKudoers($this->client, $meta))->getRequest();

        $this->assertSame('/api/v3/activities/10/kudos', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_from_summary_activity(): void
    {
        $summary = new SummaryActivity();
        $summary->id = '2313453253453413213';

        $request = (new ListKudoers($this->client, $summary))->getRequest();

        $this->assertSame('/api/v3/activities/2313453253453413213/kudos', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_from_detailed_activity(): void
    {
        $detailed = new DetailedActivity();
        $detailed->id = 22;
        $request = (new ListKudoers($this->client, $detailed))->getRequest();

        $this->assertSame('/api/v3/activities/22/kudos', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_with_pagination(): void
    {
        $request = (new ListKudoers($this->client, 123))
            ->setPageNumber(3)
            ->setPerPage(50)
            ->getRequest();

        $this->assertSame('page=3&per_page=50', $request->getUri()->getQuery());
    }

    /**
     * @test
     */
    public function can_parse_results(): void
    {
        $response = (new ListKudoers($this->client, 1))->execute();

        $this->assertCount(2, $response);
        $athlete = $response[0];

        $this->assertSame('psagan', $athlete->username);
    }

    /**
     * @test
     */
    public function can_not_use_wrong_object_as_activity_url(): void
    {
        $this->expectException(InvalidArgumentException::class);
        (new ListKudoers($this->client, new stdClass()))->execute();
    }
}
