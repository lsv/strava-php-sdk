<?php

namespace Lsv\StravaTest\Request\Gear;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedGear;
use Lsv\Strava\Model\SummaryGear;
use Lsv\Strava\Request\Gear\GetEquipment;
use Lsv\StravaTest\Request\BaseRequestTest;
use stdClass;

class GetEquipmentTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/get_equipment.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_request(): void
    {
        $request = (new GetEquipment($this->client, '333345456454562343455436452434'))->getRequest();

        $this->assertSame('/api/v3/gear/333345456454562343455436452434', $request->getUri()->getPath());
        $this->assertSame('GET', $request->getMethod());
    }

    /**
     * @test
     */
    public function can_make_request_from_summary_club(): void
    {
        $summary = new SummaryGear();
        $summary->id = 334;

        $request = (new GetEquipment($this->client, $summary))->getRequest();

        $this->assertSame('/api/v3/gear/334', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_from_detailed_club(): void
    {
        $detailed = new DetailedGear();
        $detailed->id = 1432;

        $request = (new GetEquipment($this->client, $detailed))->getRequest();

        $this->assertSame('/api/v3/gear/1432', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_parse_response(): void
    {
        $response = (new GetEquipment($this->client, 1))->execute();

        $this->assertSame('b1231', $response->id);
        $this->assertFalse($response->primary);
        $this->assertSame(3, $response->resourceState);
        $this->assertSame(388206, $response->distance);
        $this->assertSame('BMC', $response->brandName);
        $this->assertSame('Teammachine', $response->modelName);
        $this->assertSame(3, $response->frameType);
        $this->assertSame('My Bike.', $response->description);
    }

    /**
     * @test
     */
    public function can_not_use_wrong_object_as_activity_url(): void
    {
        $this->expectException(InvalidArgumentException::class);
        (new GetEquipment($this->client, new stdClass()))->execute();
    }
}
