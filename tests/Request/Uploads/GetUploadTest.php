<?php

namespace Lsv\StravaTest\Request\Uploads;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use Lsv\Strava\Client;
use Lsv\Strava\Model\Upload;
use Lsv\Strava\Request\Uploads\GetUpload;
use Lsv\StravaTest\Request\BaseRequestTest;
use stdClass;

class GetUploadTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/get_upload.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_correct_url(): void
    {
        $request = (new GetUpload($this->client, 123))->getRequest();

        $this->assertSame('/api/v3/uploads/123', $request->getUri()->getPath());
        $this->assertSame('GET', $request->getMethod());
    }

    /**
     * @test
     */
    public function can_make_correct_url_from_upload(): void
    {
        $upload = new Upload();
        $upload->id = '2354543512332455axsdfsdfa123123213';

        $request = (new GetUpload($this->client, $upload))->getRequest();

        $this->assertSame('/api/v3/uploads/2354543512332455axsdfsdfa123123213', $request->getUri()->getPath());
        $this->assertSame('GET', $request->getMethod());
    }

    /**
     * @test
     */
    public function can_parse_results(): void
    {
        $response = (new GetUpload($this->client, 1))->execute();

        $this->assertSame('6', $response->activityId);
        $this->assertSame('aeiou', $response->externalId);
        $this->assertSame('0', $response->id);
        $this->assertSame('aeiou', $response->error);
        $this->assertSame('aeiou', $response->status);
    }

    /**
     * @test
     */
    public function can_not_use_wrong_object_as_activity_url(): void
    {
        $this->expectException(InvalidArgumentException::class);
        (new GetUpload($this->client, new stdClass()))->execute();
    }
}
