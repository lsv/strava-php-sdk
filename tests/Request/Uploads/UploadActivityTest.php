<?php

namespace Lsv\StravaTest\Request\Uploads;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Lsv\Strava\Client;
use Lsv\Strava\Request\Uploads\UploadActivity;
use Lsv\Strava\Utils\Utils;
use Lsv\StravaTest\Request\BaseRequestTest;
use Psr\Http\Message\RequestInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;

class UploadActivityTest extends BaseRequestTest
{
    private Client $client;

    private File $file;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/upload_activity.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
        $this->file = new File(__DIR__.'/stub/uploadfile.gpx');
    }

    /**
     * @test
     */
    public function can_make_correct_url(): void
    {
        $request = (new UploadActivity($this->client, $this->file))->getRequest();

        $this->assertSame('/api/v3/uploads', $request->getUri()->getPath());
        $this->assertSame('POST', $request->getMethod());
        $this->assertStringContainsString('uploadfile.gpx', $request->getBody());
        $this->assertArrayHasKey('Content-Type', $request->getHeaders());
        $this->assertSame('multipart/form-data', $request->getHeader('Content-Type')[0]);
    }

    /**
     * @test
     */
    public function can_change_uploaded_filename(): void
    {
        $request = (new UploadActivity($this->client, $this->file))
            ->setName('newfilename')
            ->getRequest();

        $this->assertStringContainsString('name="data_type"', $request->getBody());
        $this->assertStringContainsString('gpx', $request->getBody());
        $this->assertStringContainsString('newfilename.gpx', $request->getBody());
        $this->assertStringNotContainsString('uploadfile.gpx', $request->getBody());
    }

    public function dataProvivder(): array
    {
        return [
            ['description'],
            ['commute'],
            ['external_id'],
            ['trainer'],
        ];
    }

    /**
     * @dataProvider dataProvivder
     *
     * @test
     *
     * @param string $data
     */
    public function can_make_correct_body($data): void
    {
        $setterName = Utils::camelCase($data);
        $setter = 'set'.$setterName;
        $length = strlen($data);

        /** @var RequestInterface $request */
        $request = (new UploadActivity($this->client, $this->file))
            ->$setter(
                $data
            )
            ->getRequest();

        $this->assertStringContainsString('Content-Disposition', $request->getBody());
        $this->assertStringContainsString("name=\"{$data}\"", $request->getBody());
        $this->assertStringContainsString("Content-Length: {$length}", $request->getBody());
        $this->assertStringContainsString($data, $request->getBody());
    }

    /**
     * @test
     */
    public function can_not_upload_not_allowed_file(): void
    {
        $allowed = array_map(
            static function ($allowed) {
                return sprintf('"%s"', $allowed);
            },
            UploadActivity::ALLOWED_FILE_EXTENSIONS
        );

        $this->expectException(InvalidOptionsException::class);
        $this->expectExceptionMessage(
            'The option "data_type" with value "txt" is invalid. Accepted values are: '.implode(', ', $allowed).'.'
        );

        $mock = $this->createMock(File::class);
        $mock
            ->method('getExtension')
            ->willReturn('txt');
        $mock
            ->method('getFilename')
            ->willReturn('dummy.txt');

        (new UploadActivity($this->client, $mock))->getRequest();
    }

    /**
     * @test
     */
    public function can_parse_results(): void
    {
        $response = (new UploadActivity($this->client, $this->file))->execute();

        $this->assertSame('6', $response->activityId);
        $this->assertSame('aeiou', $response->externalId);
        $this->assertSame('0', $response->id);
        $this->assertSame('aeiou', $response->error);
        $this->assertSame('aeiou', $response->status);
    }
}
