<?php

namespace Lsv\StravaTest\Request\RunningRaces;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use Lsv\Strava\Client;
use Lsv\Strava\Model\RunningRace;
use Lsv\Strava\Request\RunningRaces\GetRunningRace;
use Lsv\StravaTest\Request\BaseRequestTest;
use stdClass;

class GetRunningRaceTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/get_running_race.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_request(): void
    {
        $request = (new GetRunningRace($this->client, '234234324532453546131234'))->getRequest();

        $this->assertSame('/api/v3/running_races/234234324532453546131234', $request->getUri()->getPath());
        $this->assertSame('GET', $request->getMethod());
    }

    /**
     * @test
     */
    public function can_make_request_with_race(): void
    {
        $race = new RunningRace();
        $race->id = 44432;

        $request = (new GetRunningRace($this->client, $race))->getRequest();

        $this->assertSame('/api/v3/running_races/44432', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_parse_response(): void
    {
        $race = (new GetRunningRace($this->client, 1))->execute();

        $this->assertSame('aeiou', $race->country);
        $this->assertCount(1, $race->routeIds);
        $this->assertSame(5, $race->routeIds[0]);
        $this->assertSame(6, $race->runningRaceType);
        $this->assertSame(1.4658129, $race->distance);
        $this->assertSame('aeiou', $race->websiteUrl);
        $this->assertSame('aeiou', $race->city);
        $this->assertSame('23.01.2000', $race->startDateLocal->format('d.m.Y'));
        $this->assertSame('aeiou', $race->name);
        $this->assertSame('feet', $race->measurementPreference);
        $this->assertSame('0', $race->id);
        $this->assertSame('aeiou', $race->state);
        $this->assertSame('aeiou', $race->url);
    }

    /**
     * @test
     */
    public function can_not_use_wrong_object_as_activity_url(): void
    {
        $this->expectException(InvalidArgumentException::class);
        (new GetRunningRace($this->client, new stdClass()))->execute();
    }
}
