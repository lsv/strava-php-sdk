<?php

namespace Lsv\StravaTest\Request\SegmentEfforts;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use Lsv\Strava\Client;
use Lsv\Strava\Model\SummarySegment;
use Lsv\Strava\Request\SegmentEffort\ListSegmentEfforts;
use Lsv\StravaTest\Request\BaseRequestTest;
use stdClass;

class ListSegmentEffortsTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/list_segment_efforts.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_request(): void
    {
        $request = (new ListSegmentEfforts($this->client, '234234324532453546131234'))->getRequest();

        $this->assertSame('/api/v3/segments/234234324532453546131234/all_efforts', $request->getUri()->getPath());
        $this->assertSame('GET', $request->getMethod());
    }

    /**
     * @test
     */
    public function can_make_request_with_summary_effort(): void
    {
        $summary = new SummarySegment();
        $summary->id = 44432;

        $request = (new ListSegmentEfforts($this->client, $summary))->getRequest();

        $this->assertSame('/api/v3/segments/44432/all_efforts', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_with_pagination(): void
    {
        $request = (new ListSegmentEfforts($this->client, 1))
            ->setPageNumber(3)
            ->setPerPage(50)
            ->getRequest();

        $this->assertSame('page=3&per_page=50', $request->getUri()->getQuery());
    }

    /**
     * @test
     */
    public function can_parse_response(): void
    {
        $segments = (new ListSegmentEfforts($this->client, 1))->execute();

        $this->assertCount(1, $segments);
        $segment = $segments[0];

        $this->assertSame('123456789', $segment->id);
        $this->assertSame(2, $segment->resourceState);
        $this->assertSame('Alpe d\'Huez', $segment->name);
        $this->assertSame('788127', $segment->segment->id);
    }

    /**
     * @test
     */
    public function can_not_use_wrong_object_as_activity_url(): void
    {
        $this->expectException(InvalidArgumentException::class);
        (new ListSegmentEfforts($this->client, new stdClass()))->execute();
    }
}
