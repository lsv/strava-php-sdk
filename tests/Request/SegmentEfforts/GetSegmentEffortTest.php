<?php

namespace Lsv\StravaTest\Request\SegmentEfforts;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedSegmentEffort;
use Lsv\Strava\Model\SummarySegmentEffort;
use Lsv\Strava\Request\SegmentEffort\GetSegmentEffort;
use Lsv\StravaTest\Request\BaseRequestTest;
use stdClass;

class GetSegmentEffortTest extends BaseRequestTest
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mockHandler = new MockHandler(
            [
                new Response(200, [], file_get_contents(__DIR__.'/stub/get_segment_effort.json')),
            ]
        );
        $stack = HandlerStack::create($mockHandler);
        $this->client = $this->getClient($stack);
    }

    /**
     * @test
     */
    public function can_make_request(): void
    {
        $request = (new GetSegmentEffort($this->client, '234234324532453546131234'))->getRequest();

        $this->assertSame('/api/v3/segment_efforts/234234324532453546131234', $request->getUri()->getPath());
        $this->assertSame('GET', $request->getMethod());
    }

    /**
     * @test
     */
    public function can_make_request_with_summary_effort(): void
    {
        $summary = new SummarySegmentEffort();
        $summary->id = 44432;

        $request = (new GetSegmentEffort($this->client, $summary))->getRequest();

        $this->assertSame('/api/v3/segment_efforts/44432', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_make_request_with_detailed_effort(): void
    {
        $detailed = new DetailedSegmentEffort();
        $detailed->id = 44432;

        $request = (new GetSegmentEffort($this->client, $detailed))->getRequest();

        $this->assertSame('/api/v3/segment_efforts/44432', $request->getUri()->getPath());
    }

    /**
     * @test
     */
    public function can_parse_response(): void
    {
        $segment = (new GetSegmentEffort($this->client, 1))->execute();

        $this->assertSame('1234556789', $segment->id);
        $this->assertSame(3, $segment->resourceState);
        $this->assertSame('Alpe d\'Huez', $segment->name);
        $this->assertSame('63450', $segment->segment->id);
        $this->assertSame(212, $segment->athleteSegmentStats->prElapsedTime);
        $this->assertSame('12.02.2015', $segment->athleteSegmentStats->prDate->format('d.m.Y'));
    }

    /**
     * @test
     */
    public function can_not_use_wrong_object_as_activity_url(): void
    {
        $this->expectException(InvalidArgumentException::class);
        (new GetSegmentEffort($this->client, new stdClass()))->execute();
    }
}
