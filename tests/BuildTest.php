<?php

namespace Lsv\StravaTest;

use InvalidArgumentException;
use Lsv\Strava\Build;
use PHPUnit\Framework\TestCase;

class BuildTest extends TestCase
{
    /**
     * @test
     */
    public function will_throw_error_if_parameter_does_not_exists(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $class = new class() {
        };
        $className = get_class($class);
        Build::single(['not_exists'], $className);
    }
}
