<?php

namespace Lsv\StravaTest\Utils;

use Lsv\Strava\Utils\Utils;
use PHPUnit\Framework\TestCase;

class UtilsTest extends TestCase
{
    public function camelcaseDataProvider(): array
    {
        return [
            ['_hidden', 'hidden'],
            ['_hello_world', 'helloWorld'],
            ['world_is_big', 'worldIsBig'],
            ['this_is_something_long_', 'thisIsSomethingLong'],
            ['_and_with_a_beginning_and_end_', 'andWithABeginningAndEnd'],
        ];
    }

    /**
     * @dataProvider camelcaseDataProvider
     * @test
     */
    public function can_camelcase_strings(string $case, string $expected): void
    {
        $this->assertSame($expected, Utils::camelCase($case));
    }
}
