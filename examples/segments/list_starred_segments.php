<?php

use Lsv\Strava\Client;
use Lsv\Strava\Request\Segments\ListStarredSegments;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$generator = new ListStarredSegments($client);
$generator
    ->setPageNumber(1)
    ->setPerPage(15);

$segments = $generator->execute();

// $segments array<\Lsv\Strava\Model\DetailedSegment>;
