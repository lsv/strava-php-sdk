<?php

use Lsv\Strava\Client;
use Lsv\Strava\Request\Segments\ExploreSegments;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$southwestCornerLatitude = 37.8331119;
$southwestCornerLongitude = -122.4834356;
$northEastCornerLatitude = 37.8280722;
$northEastCornerLongitude = -122.4981393;

$minimum = 0;
$maximum = 5.7;

$generator = new ExploreSegments(
    $client,
    $southwestCornerLatitude,
    $southwestCornerLongitude,
    $northEastCornerLatitude,
    $northEastCornerLongitude
);
$generator
    // Optionals
    ->setActivityType('running')// Running or riding
    ->setMaximumClimbingCategory($maximum)
    ->setMinimumClimbingCategory($minimum);

$explore = $generator->execute();

// $explore instanceof \Lsv\Strava\Model\ExploreResponse;
