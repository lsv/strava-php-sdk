<?php

use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedClub;
use Lsv\Strava\Model\MetaClub;
use Lsv\Strava\Model\SummaryClub;
use Lsv\Strava\Request\Segments\GetSegmentLeaderboard;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$segment = '123'; // Segment by ID
//$segment = new SummarySegment();
//$segment = new DetailedSegment();

$generator = new GetSegmentLeaderboard($client, $segment);
$generator
    // Optionals
    ->setGender('F')// M or F
    ->setAgeGroup('65_69')
    // Any of 0_19, 20_24, 25_34, 35_44, 45_54, 55_64, 65_69, 70_74, 75_plus
    ->setAge(20)// Will calculate age group that should be used
    ->setWeightClass('125_149')
    // Any of 0_124, 125_149, 150_164, 165_179, 180_199, 200_224, 225_249, 250_plus, 0_54, 55_64, 65_74, 75_84, 85_94, 95_104, 105_114, 115_plus
    ->setWeight(75)// Will calculate weight class that should be used
    ->setFollowing()
    ->setClubId(123)
    ->setClub(new MetaClub())
    ->setClub(new SummaryClub())
    ->setClub(new DetailedClub())
    ->setDateRange('this_year')// Any of the following: this_year, this_month, this_week, today
    ->setPageNumber(1)
    ->setPerPage(15);

$leaderboard = $generator->execute();

// $leaderboard instanceof \Lsv\Strava\Model\SegmentLeaderboard;
