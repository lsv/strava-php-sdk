<?php

use Lsv\Strava\Client;
use Lsv\Strava\Request\Segments\GetSegment;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$segment = '123'; // Segment by ID
//$segment = new SummarySegment();
//$segment = new DetailedSegment();

$generator = new GetSegment($client, $segment);

$segment = $generator->execute();

// $segment instanceof \Lsv\Strava\Model\DetailedSegment;
