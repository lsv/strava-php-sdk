<?php

use Lsv\Strava\Client;
use Lsv\Strava\Request\Clubs\ListAthleteClubs;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);
$generator = new ListAthleteClubs($client);
$generator
    // Optionals
    ->setPerPage(10)
    ->setPageNumber(3);

$clubs = $generator->execute();

// $clubs array<\Lsv\Strava\Model\SummaryClub>;
