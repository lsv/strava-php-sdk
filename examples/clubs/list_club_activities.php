<?php

use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedClub;
use Lsv\Strava\Model\MetaClub;
use Lsv\Strava\Model\SummaryClub;
use Lsv\Strava\Request\Clubs\ListAthleteClubs;
use Lsv\Strava\Request\Clubs\ListClubActivities;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$club = 123; // Club ID
//$club = new MetaClub();
//$club = new SummaryClub();
//$club = new DetailedClub();

$generator = new ListClubActivities($client, $club);
$generator
    // Optionals
    ->setPerPage(10)
    ->setPageNumber(3);

$activities = $generator->execute();

// $activities array<\Lsv\Strava\Model\SummaryActivity>;
