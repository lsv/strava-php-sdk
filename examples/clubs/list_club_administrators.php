<?php

use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedClub;
use Lsv\Strava\Model\MetaClub;
use Lsv\Strava\Model\SummaryClub;
use Lsv\Strava\Request\Clubs\ListAthleteClubs;
use Lsv\Strava\Request\Clubs\ListClubActivities;
use Lsv\Strava\Request\Clubs\ListClubAdministrators;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$club = 123; // Club ID
//$club = new MetaClub();
//$club = new SummaryClub();
//$club = new DetailedClub();

$generator = new ListClubAdministrators($client, $club);
$generator
    // Optionals
    ->setPerPage(10)
    ->setPageNumber(3);

$athletes = $generator->execute();

// $athletes array<\Lsv\Strava\Model\SummaryAthlete>;
