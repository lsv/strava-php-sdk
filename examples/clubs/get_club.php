<?php

use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedClub;
use Lsv\Strava\Model\MetaClub;
use Lsv\Strava\Model\SummaryClub;
use Lsv\Strava\Request\Clubs\GetClub;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$club = 123; // Club ID
//$club = new MetaClub();
//$club = new SummaryClub();
//$club = new DetailedClub();

$generator = new GetClub($client, $club);

$club = $generator->execute();

// $club instanceof \Lsv\Strava\Model\DetailedClub;
