<?php

use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedActivity;
use Lsv\Strava\Model\MetaActivity;
use Lsv\Strava\Model\SummaryActivity;
use Lsv\Strava\Request\Activities\GetActivity;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$activity = new MetaActivity();
//$activity = new SummaryActivity();
//$activity = new DetailedActivity();
//$activity = 1; // String of activity ID

$generator = new GetActivity($client, $activity);
$generator
    // Optionals
    ->setIncludeAllEfforts();

$activity = $generator->execute();

// $activity instanceof \Lsv\Strava\Model\DetailedActivity;
