<?php

use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedActivity;
use Lsv\Strava\Model\MetaActivity;
use Lsv\Strava\Request\Activities\ListComments;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$activity = new MetaActivity();
//$activity = new SummaryActivity();
//$activity = new DetailedActivity();
//$activity = 1; // String of activity ID

$generator = new ListComments($client, $activity);
$generator
    // Optionals
    ->setPerPage(10)
    ->setPageNumber(3);

$comments = $generator->execute();

// $comments array<\Lsv\Strava\Model\Comment>;
