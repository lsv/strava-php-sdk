<?php

use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedActivity;
use Lsv\Strava\Model\MetaActivity;
use Lsv\Strava\Model\SummaryActivity;
use Lsv\Strava\Request\Activities\ListKudoers;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$activity = new MetaActivity();
//$activity = new SummaryActivity();
//$activity = new DetailedActivity();
//$activity = 1; // String of activity ID

$generator = new ListKudoers($client, $activity);
$generator
    // Optionals
    ->setPerPage(10)
    ->setPageNumber(3);

$kudoers = $generator->execute();

// $kudoers array<\Lsv\Strava\Model\SummaryAthlete>;
