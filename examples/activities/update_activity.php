<?php

use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedActivity;
use Lsv\Strava\Model\MetaActivity;
use Lsv\Strava\Model\SummaryActivity;
use Lsv\Strava\Request\Activities\UpdateActivity;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$activity = new MetaActivity();
//$activity = new SummaryActivity();
//$activity = new DetailedActivity();
//$activity = 1; // String of activity ID

$generator = new UpdateActivity($client, $activity);
$generator
    // Optionals
    ->setName('name')
    ->setType('type')
    ->setDescription('description')
    ->setGearId('gear_id')
    ->setTrainer(true)
    ->setCommute(false);

$updatedActivity = $generator->execute();

// $updatedActivity instanceof \Lsv\Strava\Model\DetailedActivity;
