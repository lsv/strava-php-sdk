<?php

use Lsv\Strava\Client;
use Lsv\Strava\Request\Activities\CreateActivity;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$name = 'name'; // The name of the activity.
$type = 'Ride'; // Type of activity. For example - Run, Ride etc.
$elapsedTime = 10; // Activity elapsed time in seconds.

$generator = new CreateActivity($client, $name, $type, $elapsedTime);
$generator
    // Optionals
    ->setStartDate(new \DateTime())
    ->setDescription('description')
    ->setDistance(11.4)
    ->setTrainer(true)
    ->setPhotoIds([1, 2, 3, 4])
    ->setCommute(false);

$createdActivity = $generator->execute();

// $createdActivity instanceof \Lsv\Strava\Model\DetailedActivity;
