<?php

use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedGear;
use Lsv\Strava\Model\SummaryGear;
use Lsv\Strava\Request\Gear\GetEquipment;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$gear = 123; // Gear ID
//$gear = new SummaryGear();
//$gear = new DetailedGear();

$generator = new GetEquipment($client, $gear);

$gear = $generator->execute();

// $gear instanceof \Lsv\Strava\Model\DetailedGear;
