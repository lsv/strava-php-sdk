<?php

use Lsv\Strava\Client;
use Lsv\Strava\Request\RunningRaces\ListRunningRaces;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);
$generator = new ListRunningRaces($client);

$generator
    // Optional
    ->setYear(2017);

$races = $generator->execute();

// $races array<\Lsv\Strava\Model\RunningRace>;
