<?php

use Lsv\Strava\Client;
use Lsv\Strava\Model\RunningRace;
use Lsv\Strava\Request\RunningRaces\GetRunningRace;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$race = 123; // Race ID
//$race = new RunningRace();

$generator = new GetRunningRace($client, $race);

$race = $generator->execute();

// $race instanceof \Lsv\Strava\Model\RunningRace;
