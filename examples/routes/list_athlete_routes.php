<?php

use Lsv\Strava\Client;
use Lsv\Strava\Request\Routes\ListAthleteRoutes;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$athlete = 1; // The ID of an athlete
//$athlete = new MetaAthlete();
//$athlete = new SummaryAthlete();
//$athlete = new DetailedAthlete();

$generator = new ListAthleteRoutes($client, $athlete);
$generator
    // Optionals
    ->setPerPage(10)
    ->setPageNumber(3);

$routes = $generator->execute();

// $routes array<\Lsv\Strava\Model\Route>;
