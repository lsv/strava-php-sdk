<?php

use Lsv\Strava\Client;
use Lsv\Strava\Model\Route;
use Lsv\Strava\Request\Routes\GetRoute;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$route = 123; // Route ID
//$route = new Route();

$generator = new GetRoute($client, $route);

$route = $generator->execute();

// $route instanceof \Lsv\Strava\Model\Route;
