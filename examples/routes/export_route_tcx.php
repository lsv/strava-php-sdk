<?php

use Lsv\Strava\Client;
use Lsv\Strava\Model\Route;
use Lsv\Strava\Request\Routes\ExportRouteTcx;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$route = 123; // Route ID
//$route = new Route();

$generator = new ExportRouteTcx($client, $route);

$fileContent = $generator->execute();

// $fileContent will now be the content of a tcx file, so you can write it with
// file_put_contents('your_filename.tcx', $fileContent);
