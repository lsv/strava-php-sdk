<?php

use Lsv\Strava\Client;
use Lsv\Strava\Request\SegmentEffort\ListSegmentEfforts;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$segment = 123; // Segment ID
//$segment = new SummarySegment();

$generator = new ListSegmentEfforts($client, $segment);

$segmentEfforts = $generator->execute();

// $segmentEffort array<\Lsv\Strava\Model\SummarySegmentEffort>;
