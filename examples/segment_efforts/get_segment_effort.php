<?php

use Lsv\Strava\Client;
use Lsv\Strava\Request\SegmentEffort\GetSegmentEffort;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$segmentEffort = 123; // SegmentEffort ID
//$segmentEffort = new SummarySegmentEffort();
//$segmentEffort = new DetailedSegmentEffort();

$generator = new GetSegmentEffort($client, $segmentEffort);

$segmentEffort = $generator->execute();

// $segmentEffort instanceof \Lsv\Strava\Model\DetailedSegmentEffort;
