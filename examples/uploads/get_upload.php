<?php

use Lsv\Strava\Client;
use Lsv\Strava\Request\Uploads\GetUpload;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$upload = 123; // Upload ID
// $upload = new Upload();

$generator = new GetUpload($client, $upload);

$upload = $generator->execute();

// $upload instanceof \Lsv\Strava\Model\Upload;
