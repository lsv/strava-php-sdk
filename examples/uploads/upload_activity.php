<?php

use Lsv\Strava\Client;
use Lsv\Strava\Request\Uploads\UploadActivity;
use Symfony\Component\HttpFoundation\File\File;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$path_to_file = '/path/to/your/file';
$file = new File($path_to_file);

$generator = new UploadActivity($client, $file);

$generator
    // Optional
    ->setName('new_file_name.gsx')// Rename the uploaded file (if file is already set, the extension is not needed)
    ->setTrainer('trainer')
    ->setExternalId('external_id')
    ->setCommute('commute')
    ->setDescription('description');

$upload = $generator->execute();

// $upload instanceof \Lsv\Strava\Model\Upload;
