<?php

use Lsv\Strava\Client;
use Lsv\Strava\Request\Streams\GetSegmentEffortStreams;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$segmentEffort = 123; // SegmentEffort ID
//$segmentEffort = new SummarySegmentEffort();
//$segmentEffort = new DetailedSegmentEffort();

$keys = ['key1', 'key2'];

$generator = new GetSegmentEffortStreams($client, $segmentEffort, $keys);

$strems = $generator->execute();

// $streams array<\Lsv\Strava\Model\StreamSet>;
