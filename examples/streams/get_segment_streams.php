<?php

use Lsv\Strava\Client;
use Lsv\Strava\Request\Streams\GetSegmentStreams;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$segment = 123; // Segment ID
//$segment = new SummarySegment();

$keys = ['key1', 'key2'];

$generator = new GetSegmentStreams($client, $segment, $keys);

$strems = $generator->execute();

// $streams array<\Lsv\Strava\Model\StreamSet>;
