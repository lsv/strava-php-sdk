<?php

use Lsv\Strava\Client;
use Lsv\Strava\Model\MetaActivity;
use Lsv\Strava\Request\Streams\GetActivityStreams;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$activity = new MetaActivity();
//$activity = new SummaryActivity();
//$activity = new DetailedActivity();
//$activity = 1; // String of activity ID

$keys = ['key1', 'key2'];

$generator = new GetActivityStreams($client, $activity, $keys);

$strems = $generator->execute();

// $streams array<\Lsv\Strava\Model\StreamSet>;
