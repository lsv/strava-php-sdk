<?php

use Lsv\Strava\Client;
use Lsv\Strava\Request\Athletes\GetActivities;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);
$generator = new GetActivities($client);
$generator
    // Optionals
    // A timestamp to use for filtering activities that have taken place before a certain time.
    ->setBefore(new \DateTime())
    // A timestamp to use for filtering activities that have taken place after a certain time.
    ->setAfter(new \DateTime())
    ->setPerPage(10)
    ->setPageNumber(3);

$activities = $generator->execute();

// $activities array<\Lsv\Strava\Model\SummaryActivity>;
