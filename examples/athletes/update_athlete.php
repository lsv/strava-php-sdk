<?php

use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedAthlete;
use Lsv\Strava\Model\MetaAthlete;
use Lsv\Strava\Model\SummaryAthlete;
use Lsv\Strava\Request\Athletes\GetAthletesStats;
use Lsv\Strava\Request\Athletes\UpdateAthlete;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$weight = 73.4; // Weight in kilogram

$generator = new UpdateAthlete($client, $weight);

$athlete = $generator->execute();

// $athlete instanceof \Lsv\Strava\Model\DetailedAthlete;
