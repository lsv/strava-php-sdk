<?php

use Lsv\Strava\Client;
use Lsv\Strava\Request\Athletes\GetAuthenticatedAthlete;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);
$generator = new GetAuthenticatedAthlete($client);

$athlete = $generator->execute();

// $athlete instanceof \Lsv\Strava\Model\DetailedAthlete;
