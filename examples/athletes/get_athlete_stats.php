<?php

use Lsv\Strava\Client;
use Lsv\Strava\Model\DetailedAthlete;
use Lsv\Strava\Model\MetaAthlete;
use Lsv\Strava\Model\SummaryAthlete;
use Lsv\Strava\Request\Athletes\GetAthletesStats;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);

$athlete = 1; // The ID of an athlete
//$athlete = new MetaAthlete();
//$athlete = new SummaryAthlete();
//$athlete = new DetailedAthlete();

$generator = new GetAthletesStats($client, $athlete);

$stats = $generator->execute();

// $stats instanceof \Lsv\Strava\Model\ActivityStats;
