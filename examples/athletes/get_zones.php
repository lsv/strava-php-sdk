<?php

use Lsv\Strava\Client;
use Lsv\Strava\Request\Athletes\GetAuthenticatedAthlete;
use Lsv\Strava\Request\Athletes\GetZones;

require __DIR__.'/../../vendor/autoload.php';

$bearer = new \Http\Message\Authentication\Bearer('YOUR_TOKEN');
$client = new Client($bearer);
$generator = new GetZones($client);

$zones = $generator->execute();

// $zones array<\Lsv\Strava\Model\Zones>;
