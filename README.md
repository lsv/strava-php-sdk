Strava PHP SDK
==============

[![pipeline status](https://gitlab.com/lsv/strava-php-sdk/badges/master/pipeline.svg)](https://gitlab.com/lsv/strava-php-sdk/commits/master) [![coverage report](https://gitlab.com/lsv/strava-php-sdk/badges/master/coverage.svg)](https://gitlab.com/lsv/strava-php-sdk/commits/master)

SDK for strava.com

## Examples

See [examples](examples)